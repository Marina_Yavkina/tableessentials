﻿using BastionGantt.Core.Abstractions.Repositories.Base;
using BastionGantt.Core.Models;
using BastionGantt.Core.Domain ;

namespace BastionGantt.Core.Abstractions.Repositories
{
    public interface IEntityRepository : IRepository<Entity,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору сущности 
        /// </summary>
        /// <param name="entityId">идентификатор сущности</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список сущностей</returns>
        Task<PagedList<Entity>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<Entity>> GetPagedAsync(string text, int pageNumber, int pageSize, bool noTracking = false);
        IQueryable<Entity> GetAllAsync(bool noTracking = false);
        

    }
}
