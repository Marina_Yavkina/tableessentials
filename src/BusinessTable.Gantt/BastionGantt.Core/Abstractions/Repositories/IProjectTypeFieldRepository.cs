﻿using BastionGantt.Core.Abstractions.Repositories.Base;
using BastionGantt.Core.Models;
using BastionGantt.Core.Domain ;

namespace BastionGantt.Core.Abstractions.Repositories
{
    public interface IProjectTypeFieldRepository : IRepository<ProjectTypeField,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору типа проекта 
        /// </summary>
        /// <param name="projectTypeId">идентификатор типа проекта</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список типов проектов</returns>
        Task<PagedList<ProjectTypeField>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<ProjectTypeField>> GetPagedByProjectTypeIdAsync(Guid projectTypeId, int pageNumber, int pageSize, bool noTracking = false);
       IQueryable<ProjectTypeField> GetAllAsync(bool noTracking = false);
    }
}
