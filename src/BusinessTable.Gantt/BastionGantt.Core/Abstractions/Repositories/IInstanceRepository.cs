﻿using BastionGantt.Core.Abstractions.Repositories.Base;
using BastionGantt.Core.Models;
using BastionGantt.Core.Domain ;

namespace BastionGantt.Core.Abstractions.Repositories
{
    public interface IInstanceRepository : IRepository<Instance,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору инстанса
        /// </summary>
        /// <param name="instanceId">идентификатор инстанса</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список инстансов</returns>
        Task<PagedList<Instance>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<Instance>> GetPagedByEntityIdAsync(Guid entityId, int pageNumber, int pageSize, bool noTracking = false);
        Task<Instance> GetById(Guid id, bool noTracking = false);
        IQueryable<Instance> GetAllAsync(bool noTracking = false);
        
    }
}
