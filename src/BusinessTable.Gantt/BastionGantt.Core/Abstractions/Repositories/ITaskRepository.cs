﻿using BastionGantt.Core.Abstractions.Repositories.Base;
using BastionGantt.Core.Models;
using BastionGantt.Core.Domain ;
using A=BastionGantt.Core.Domain ;

namespace BastionGantt.Core.Abstractions.Repositories
{
    public interface ITaskRepository : IRepository<A.Task,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору поля 
        /// </summary>
        /// <param name="taskId">идентификатор задачи</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список полей</returns>
        Task<PagedList<A.Task>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<A.Task> GetById(Guid id, bool noTracking = false);
        Task<PagedList<A.Task>> GetTasksByPrpjectTypePagedAsync(Guid projectTypeId, int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<A.Task>> GetOnlyProjectsAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<A.Task>> GetTaskByProjectIdPagedAsync(Guid taskId, int pageNumber, int pageSize, bool noTracking = false);
    }
}
