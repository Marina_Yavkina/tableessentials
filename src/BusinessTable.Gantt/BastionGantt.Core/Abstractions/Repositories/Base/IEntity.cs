﻿namespace BastionGantt.Core.Abstractions.Repositories.Base
{
    /// <summary>
    /// Интерфейс сущности с идентификатором
    /// </summary>
    /// <typeparam name="TId">Тип идентификатора</typeparam>
    public interface IEntity<TId>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        TId Id { get; set; }

        /// <summary>
        /// Возвращает значение true, если элемент был удален
        /// </summary>
        bool IsDeleted { get; set; }
        /// <summary>
        /// Дата создания
        /// </summary>
        DateTime CreateDate  { get; set; }
        /// <summary>
        /// Дата изменения
        /// </summary>
        DateTime? UpdateDate  { get; set; }
        /// <summary>
        /// Дата удаления
        /// </summary>
        DateTime? DeleteDate { get; set; }
    }
}
