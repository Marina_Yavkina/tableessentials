﻿using BastionGantt.Core.Abstractions.Repositories.Base;
using BastionGantt.Core.Models;
using BastionGantt.Core.Domain ;

namespace BastionGantt.Core.Abstractions.Repositories
{
    public interface IFieldRepository : IRepository<Field,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору поля 
        /// </summary>
        /// <param name="fieldId">идентификатор поля</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список полей</returns>
        Task<PagedList<Field>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<Field>> GetPagedAsync(string text, int pageNumber, int pageSize, bool noTracking = false);
        IQueryable<Field> GetAllAsync( bool noTracking = false);
    }
}

