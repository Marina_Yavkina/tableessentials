﻿using BastionGantt.Core.Abstractions.Repositories.Base;
using BastionGantt.Core.Models;
using BastionGantt.Core.Domain ;

namespace BastionGantt.Core.Abstractions.Repositories
{
    public interface IValueRepository : IRepository<Value,Guid>
    {
        /// <summary>
        /// Получить постраничный список по значению
        /// </summary>
        /// <param name="valueId">идентификатор значения</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список значений</returns>
        Task<PagedList<Value>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<Value>> GetPagedByInstanceIdAsync(Guid instanceId, int pageNumber, int pageSize, bool noTracking = false);
        Task<List<Value>> GetByInstanceIdAsync(Guid instanceId, bool noTracking = false);
        IQueryable<Value> GetAllAsync(bool noTracking = false);
    }
}
