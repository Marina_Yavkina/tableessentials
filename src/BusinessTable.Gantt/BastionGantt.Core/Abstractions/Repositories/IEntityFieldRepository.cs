﻿using BastionGantt.Core.Abstractions.Repositories.Base;
using BastionGantt.Core.Models;
using BastionGantt.Core.Domain ;

namespace BastionGantt.Core.Abstractions.Repositories
{
    public interface IEntityFieldRepository : IRepository<EntityField,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору сущность-поля
        /// </summary>
        /// <param name="entityFieldId">идентификатор сущность-поля</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список сущность-полей</returns>
        Task<PagedList<EntityField>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<List<EntityField>> GetPagedByEntityIdAsync(Guid entityId, bool noTracking = false);
        IQueryable<EntityField> GetAllAsync(bool noTracking = false);
       
    }
}
