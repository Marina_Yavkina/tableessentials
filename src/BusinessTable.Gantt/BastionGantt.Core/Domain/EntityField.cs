using System;
using System.ComponentModel.DataAnnotations.Schema;
using BastionGantt.Core.Domain.Base;

namespace BastionGantt.Core.Domain 
{
    public class EntityField: BaseEntity
    {
        public Guid EntityId {get;set;}
        public virtual  Entity  Entity { get; set; }
        public Guid FieldId {get;set;}
        public virtual  Field  Field { get; set; }

    }
}