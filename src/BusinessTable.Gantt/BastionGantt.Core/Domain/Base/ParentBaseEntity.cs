using BastionGantt.Core.Abstractions.Repositories.Base;

namespace BastionGantt.Core.Domain.Base
{
    public class ParentBaseEntity : IParentEntity<Guid,Guid?>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }
        public Guid? ParentId {get;set;}

        /// <summary>
        /// Удаленная запись доступна только для чтения
        /// </summary>
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime? UpdateDate { get; set; }
        /// <summary>
        /// Дата удаления
        /// </summary>
        public DateTime? DeleteDate { get; set; }
    }
}
