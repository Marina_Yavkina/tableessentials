using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using BastionGantt.Core.Domain.Base;

namespace BastionGantt.Core.Domain 
{
    public class ProjectTypeField : BaseEntity
    {
        public Guid ProjectTypeId {get;set;}
        public virtual  ProjectType  ProjectType { get; set; }
        
        public Guid FieldId {get;set;}
        public virtual  Field  Field { get; set; }

    }
}