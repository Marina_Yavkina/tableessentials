using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using BastionGantt.Core.Domain.Base;

namespace BastionGantt.Core.Domain 
{
    public class TaskValue: BaseEntity
    {
        public Guid FieldId {get;set;}
        public virtual  Field  Field { get; set; }
        public Guid TaskId {get;set;}
        public virtual Task Task { get; set; }
        public Guid? InstanceId {get;set;}
        public virtual Instance? Instance { get; set; }
        public Guid? ValueId {get;set;}
        public virtual Value? Value { get; set; }
            
        public string? TextData {get;set;}
        public double? NumericData {get;set;}
        public bool? BoolData {get;set;}
       
    }
}