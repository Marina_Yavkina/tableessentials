using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using BastionGantt.Core.Domain.Base;

namespace BastionGantt.Core.Domain 
{
    public class ProjectType : BaseEntity
    {
        public string Name { get; set; }
        public DateTime CreateDate { get; set; }
        public string Decsription { get; set; }
        public virtual List<Task> Tasks { get; set; }
    }
}