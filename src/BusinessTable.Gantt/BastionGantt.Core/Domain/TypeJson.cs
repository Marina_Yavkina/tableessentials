using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using BastionGantt.Core.Domain.Base;

namespace BastionGantt.Core.Domain 
{
    public class TypeJson
    {
        public string SimpleType { get; set; }
        public string DirectoryId { get; set; }
        public string InstanceDirectoryId { get; set; }
        // TODO: delete
        public string TaskId { get; set; }
    }
}