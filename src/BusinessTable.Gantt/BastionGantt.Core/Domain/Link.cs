using System;
using BastionGantt.Core.Domain.Base;

namespace BastionGantt.Core.Domain 
{
    public class Link: BaseEntity
    {
        public string Type { get; set; }
        public Guid SourceTaskId { get; set; }
        public Guid TargetTaskId { get; set; }
        public virtual Task SourceTask {get;set;}
        public virtual Task TargetTask {get;set;}
        
    }
}