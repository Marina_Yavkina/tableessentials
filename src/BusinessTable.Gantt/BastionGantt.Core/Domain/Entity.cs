using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using BastionGantt.Core.Domain.Base;

namespace BastionGantt.Core.Domain 
{
    public class Entity: BaseEntity
    {
       
        public string Name { get; set; }
        public string Decsription { get; set; }
        public virtual List<EntityField> EntityFields { get; set; }
    }
}