﻿namespace BastionGantt.Core.Models
{
    public class PagedList<T> 
    {
        public List<T> Entities { get; set; }
        public Pagination Pagination { get; set; }
    }
}
