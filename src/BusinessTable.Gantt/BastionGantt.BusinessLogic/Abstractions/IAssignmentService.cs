﻿using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.BusinessLogic.Abstractions
{
    public interface IAssignmentService : IService
    {
        /// <summary>
        /// Получить постраничный список зон
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>список ДТО зон</returns>
        Task<PagedList<AssignmentDto>> GetPage(int pageNumber, int pageSize);
        Task<List<AssignmentDto>> GetList();

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        Task<AssignmentDto> Get(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        Task<Guid> Create(AssignmentCreateDto dto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        System.Threading.Tasks.Task Update(AssignmentDto dto);
        System.Threading.Tasks.Task Remove(Guid id);


    }
}
