﻿using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.BusinessLogic.Abstractions
{
    public interface ITaskService : IService
    {
        /// <summary>
        /// Получить постраничный список зон
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>список ДТО зон</returns>
        Task<PagedList<TaskViewModel>> GetPage(int pageNumber, int pageSize);
        Task<PagedList<TaskViewModel>> GetOnlyProjectsPage(int pageNumber, int pageSize);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        Task<TaskDto> Get(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        Task<Guid> Create(TaskCreateDto dto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        Task<Guid> Update(TaskDto taskdto);


        /// <summary>
        /// Поместить в архив
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="employeeId">идентификатор сотрудника клуба</param>
       // Task AddToArchive(Guid id, Guid employeeId);

        Task<PagedList<TaskViewModel>> GetTasksByProjectId(Guid id, int pageNumber, int pageSize);
        System.Threading.Tasks.Task Remove(Guid id);
    }
}
