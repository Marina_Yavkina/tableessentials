
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;
using BastionGantt.BusinessLogic.Models;
using BastionGantt.BusinessLogic.Contracts;
using SharedModel;

namespace BastionGantt.BusinessLogic.Abstractions
{
    public interface IValueService : IService
    {   /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        Task<ValueDto> Get(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        Task<Guid> Create(ValueCreateDto dto);
        System.Threading.Tasks.Task CreateValueBatch(ValueCreateDto[] dto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        System.Threading.Tasks.Task Update(ValueDto dto);
        System.Threading.Tasks.Task<bool> DeleteBatchValue(Guid id);
        Task<List<ValueDto>> GetValueByInstance(Guid id);

        System.Threading.Tasks.Task Remove(Guid id);
        Task<List<ValueDto>> GetList();
        Task<List<DictContract>> GetDictionaries();
    }
}
