﻿using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.BusinessLogic.Abstractions
{
    public interface IEntityService : IService
    {
        /// <summary>
        /// Получить постраничный список зон
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>список ДТО зон</returns>
        Task<PagedList<EntityDto>> GetPage(int pageNumber, int pageSize);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        Task<EntityDto> Get(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        Task<Guid> Create(EntityCreateDto valuedto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        System.Threading.Tasks.Task Update(EntityDto fielddto);
        Task<List<EntityDto>> GetList();
        System.Threading.Tasks.Task Remove(Guid id);

    }
}
