﻿using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.BusinessLogic.Abstractions
{
    public interface IFieldService : IService
    {
        /// <summary>
        /// Получить постраничный список зон
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>список ДТО зон</returns>
        Task<PagedList<FieldDto>> GetPage(int pageNumber, int pageSize);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        Task<FieldDto> Get(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        Task<Guid> Create(FieldCreateDto valuedto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        System.Threading.Tasks.Task Update(FieldDto fielddto);


        /// <summary>
        /// Поместить в архив
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="employeeId">идентификатор сотрудника клуба</param>
       // Task AddToArchive(Guid id, Guid employeeId);

        Task<List<FieldDto>> GetFieldsByEntityId(Guid id);
        Task<List<FieldDto>> SearchFieldsByEntityId(Guid id,string text);
        Task<List<FieldDto>> GetFieldsByProjectTypeId(Guid id);
        Task<List<FieldDto>> GetFieldsByProjectId(Guid id);

        Task<List<FieldDto>> GetList();
        System.Threading.Tasks.Task Remove(Guid id);
    }
}
