using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;

namespace BastionGantt.BusinessLogic.Abstractions
{
    public interface ITaskValueService : IService
    {
        /// <summary>
        /// Получить постраничный список зон
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>список ДТО зон</returns>
        Task<PagedList<TaskValueDto>> GetPage(int pageNumber, int pageSize);

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">id задачи</param>
        /// <returns>ДТО зоны</returns>
        Task<TaskValueDto> Get(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        Task<Guid> Create(TaskValueCreateDto dto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        Task Update(TaskValueDto dto);
        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="valueDto">ДТО TaskValue</param>
        Task CreateValueBach(TaskValueCreateDto[] valuedDto);

        /// <summary>
        /// Поместить в архив
        /// </summary>
        /// <param name="id">идентификатор</param>
        Task AddToArchive(Guid id);
        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="taskId">идентификатор задачи</para
        /// <returns>список значений задач</returns>
        Task<List<TaskValueDto>> GetTaskValueByTask(Guid taskId);
        /// <summary>
        /// Поместить в архив
        /// </summary>
        /// <param name="id">id задачи</param>
        System.Threading.Tasks.Task RemoveBatch(Guid id);
         /// <summary>
        /// Поместить в архив
        /// </summary>
        /// <param name="id">id TaskValue</param>
        Task DeleteValue(Guid id);
        System.Threading.Tasks.Task Remove(Guid id);
        Task<List<TaskValueDto>> GetList();
    }
}
