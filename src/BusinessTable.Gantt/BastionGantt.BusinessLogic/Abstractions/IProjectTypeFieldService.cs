
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.BusinessLogic.Abstractions
{
    public interface IProjectTypeFieldService : IService
    {   /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
         Task<ProjectTypeFieldDto> Get(Guid id);

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        Task<Guid> Create(ProjectTypeFieldCreateDto dto);
        System.Threading.Tasks.Task CreateValueBatch(ProjectTypeFieldCreateDto[] dto);

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        System.Threading.Tasks.Task Update(ProjectTypeFieldDto dto);
        System.Threading.Tasks.Task<bool> DeleteBatchValue(ProjectTypeFieldDto[] dto);
        Task<List<ProjectTypeFieldDto>> GetList();
        System.Threading.Tasks.Task Remove(Guid id);

    }
}
