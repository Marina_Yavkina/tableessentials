﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using A=BastionGantt.BusinessLogic.Contracts;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;
using System.Collections.Generic;
using SharedModel;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class ValueService : IValueService
    {
        private readonly IMapper _mapper;
        private readonly IValueRepository _valueRepository;
        private readonly IEntityFieldRepository _entityFieldRepository;
        private readonly IFieldRepository _fieldRepository;
        private readonly IEntityRepository _entityRepository;
        private readonly  IInstanceRepository _instanceRepository;


        public ValueService(IMapper mapper,  IValueRepository valueRepository,IEntityFieldRepository entityFieldRepository,IFieldRepository fieldRepository,
        IEntityRepository entityRepository,IInstanceRepository instanceRepository)
        {
            _mapper = mapper;
            _valueRepository = valueRepository;
            _entityFieldRepository = entityFieldRepository;
            _fieldRepository = fieldRepository;
            _entityRepository = entityRepository;
            _instanceRepository = instanceRepository;

        }
        public async Task<PagedList<ValueDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _valueRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<ValueDto>
            {
                Entities = _mapper.Map<List<Value>, List<ValueDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };
        }
        public async System.Threading.Tasks.Task CreateValueBatch(ValueCreateDto[] dto)
        {
            var values = _mapper.Map<Value[]>(dto);
            await _valueRepository.AddRange(values); 
            await _valueRepository.SaveChangesAsync();
        }
       
        public async System.Threading.Tasks.Task<bool> DeleteBatchValue(Guid id)
        {
            var items = _valueRepository.GetAllAsync().Where(c => (c.InstanceId  ==  id)).ToList();
            await _valueRepository.RemoveRange(items);
            await _valueRepository.SaveChangesAsync();
            return (true);
        }

        public async Task<ValueDto> Get(Guid id)
        {
            var instance = await _valueRepository.GetAsync(id);
            return _mapper.Map<ValueDto>(instance);
        }

        public async Task<Guid> Create(ValueCreateDto dto)
        {
            var value = _mapper.Map<Value>(dto);
            var result = await _valueRepository.AddAsync(value);
            await _valueRepository.SaveChangesAsync();
            return result.Id;

        }

        public async System.Threading.Tasks.Task Update(ValueDto dto)
        {
            var value = await _valueRepository.GetAsync(dto.id!);

            if (value == null)
                throw new NotExistException("Value", dto.id);
            _mapper.Map(dto, value);
            await _valueRepository.SaveChangesAsync();
        }

        public async Task<List<ValueDto>> GetValueByInstance(Guid id)
        {
            var items =  _valueRepository.GetAllAsync().Where(c => (c.InstanceId  ==  id)).ToList();
            var t = _mapper.Map<List<ValueDto>>(items);
            return t;
        }
        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _valueRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("Value", id);
           
            _valueRepository.Delete(id);
            await _valueRepository.SaveChangesAsync();
        }
        public async Task<List<ValueDto>> GetList()
        {
            var list =  _valueRepository.GetAllAsync().ToList();
            return _mapper.Map<List<Value>, List<ValueDto>>(list);
        }
         public async Task<List<DictContract>> GetDictionaries()
         {
            var ef = _entityFieldRepository.GetAllAsync().ToList();
                List<DictContract> values =new List<DictContract>();
                if (ef.Count != 0)
                {
                    var fieldList = _fieldRepository.GetAllAsync().AsEnumerable().Join(ef, c => c.Id, ct => ct.FieldId, (c,ct) => new {
                    id = ct.Id,
                    field_id = ct.FieldId,
                    field = c.Name,
                    entity_id = ct.EntityId,
                    type =  new A.TypeJsonDto { simple_type = c.Type.SimpleType, directory_id = c.Type.DirectoryId, instance_directory_id = c.Type.InstanceDirectoryId}
                    }).ToList(); 
                    var efList = _entityRepository.GetAllAsync().AsEnumerable().Join(fieldList , c => c.Id, ct => ct.entity_id, (c,ct) => new {
                    id = ct.id,
                    field_id = ct.field_id,
                    field = ct.field,
                    entity_id = ct.entity_id,
                    entity = c.Name,
                    type =  ct.type
                    }).ToList(); 
                    var inst = _instanceRepository.GetAllAsync().AsEnumerable().Join(efList , c => c.EntityId, ct => ct.entity_id, (c,ct) => new {
                    id = ct.id,
                    field_id = ct.field_id,
                    field = ct.field,
                    entity_id = ct.entity_id,
                    entity = ct.entity,
                    type =  ct.type,
                    instance = c.Name,
                    instance_id = c.Id//,
                    //values =  _valueRepository.GetAllAsync().Where(m => (m.InstanceId  ==  c.Id)).ToList()
                    }).ToList(); 
                    var val = _valueRepository.GetAllAsync().AsEnumerable().Join(inst , c => (new {ins_id = c.InstanceId, field_id = c.FieldId}) , ct => ( new {ins_id = ct.instance_id, field_id = ct.field_id}), (c,ct) => 
                    new ValueContract() {
                    field = ct.field,
                    entity = ct.entity,
                    type =  new SharedModel.TypeJsonDto{ simple_type = ct.type.simple_type,directory_id = ct.type.directory_id,
                    instance_directory_id = ct.type.instance_directory_id,  task_id = ct.type.task_id },
                    instance = ct.instance,
                    text_data =c.TextData ,
                    num_data = (int?)c.NumericData ,
                    bool_data = c.BoolData
                    }).ToList(); 

                    var res1 =  val.GroupBy(c => new{ c.entity,c.field,c.type,c.instance })
                    .Select( cl => new InstanceContract()
                    {
                        entity = cl.Key.entity,
                        field = cl.Key.field,
                        type = cl.Key.type,
                        instance = cl.Key.instance,
                        values = cl.ToList()//.SelectMany(g => new ValueContract{bool_data = g.bool_data,entity = g.entity, instance = g.instance, field = g.field, text_data = g.text_data, num_data =(int)g.num_data})
                    }).ToList();
                    values =  res1.GroupBy(c => new{ c.entity,c.field,c.type })
                    .Select( cl => new DictContract
                    {
                        entity = cl.Key.entity,
                        field = cl.Key.field,
                        type = cl.Key.type,
                        //  () => 
                        //     {
                        //         if (cl.Key.type.SimpleType == "integer")
                        //             return (int)1;
                        //         if (cl.Key.type.SimpleType == "string")
                        //             return (int)4;
                        //         else return (int)-1;
                        //     } ,
                        Instance = (List<InstanceContract>)cl.ToList()
                    }).ToList();


                }  
                return values; 
         }
    }
}
