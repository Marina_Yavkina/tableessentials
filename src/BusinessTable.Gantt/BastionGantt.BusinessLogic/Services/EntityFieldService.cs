﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
     [Inject(ServiceLifetime.Scoped)]
    public class EntityFieldService : IEntityFieldService
    {
        private readonly IMapper _mapper;
        private readonly IEntityFieldRepository _entityFieldRepository;

        public EntityFieldService(IMapper mapper, IEntityFieldRepository entityFieldRepository)
        {
            _mapper = mapper;
            _entityFieldRepository = entityFieldRepository;
        }
        public async Task<PagedList<EntityFieldDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _entityFieldRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<EntityFieldDto>
            {
                Entities = _mapper.Map<List<EntityField>, List<EntityFieldDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };

        }

        public async Task<List<EntityFieldDto>> GetList()
        {
            var list =  _entityFieldRepository.GetAllAsync().ToList();
            if (list == null)
                throw new NotFoundException("List of EntityFields","");
            return  _mapper.Map<List<EntityField>, List<EntityFieldDto>>(list);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        public async Task<EntityFieldDto> Get(Guid id)
        {
            var entityField = await _entityFieldRepository.GetAsync(id);
            return _mapper.Map<EntityField,EntityFieldDto>(entityField);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">>ДТО entityField</param>
        /// <returns>идентификатор</returns>
        public async Task<Guid> Create(EntityFieldCreateDto dto)
        {
            var entityField = _mapper.Map<EntityField>(dto);
            var result = await _entityFieldRepository.AddAsync(entityField);
            await _entityFieldRepository.SaveChangesAsync();
            return result.Id;

        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="dto">ДТО entityField</param>
        public async System.Threading.Tasks.Task Update(EntityFieldDto dto)
        {
            var field = await _entityFieldRepository.GetAsync(dto.id);

            if (field == null)
                throw new NotExistException("EntityField", dto.id);
            _mapper.Map(dto, field);
            await _entityFieldRepository.SaveChangesAsync();
        }

       public async System.Threading.Tasks.Task CreateEntityFieldBatch(EntityFieldCreateDto[] entityFieldDto)
       {
         var entityField = _mapper.Map<EntityField[]>(entityFieldDto);

                await _entityFieldRepository.AddRange(entityField);
                await _entityFieldRepository.SaveChangesAsync();
       }

        public async System.Threading.Tasks.Task DeleteEntityFields(EntityFieldDto[] entityFieldDto)
        {
            var entityField = _mapper.Map<EntityField[]>(entityFieldDto);
            await _entityFieldRepository.RemoveRange(entityField);
            await _entityFieldRepository.SaveChangesAsync();
        }

    }
}
