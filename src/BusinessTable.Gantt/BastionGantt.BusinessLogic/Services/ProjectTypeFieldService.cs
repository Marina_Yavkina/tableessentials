﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class ProjectTypeFieldService : IProjectTypeFieldService
    {
        private readonly IMapper _mapper;
        private readonly IProjectTypeFieldRepository _projectTypeFieldRepository;

        public ProjectTypeFieldService(IMapper mapper,IProjectTypeFieldRepository projectTypeFieldRepository)
        {
            _mapper = mapper;
            _projectTypeFieldRepository = projectTypeFieldRepository;
        }
       
        public async Task<ProjectTypeFieldDto> Get(Guid id)
        {
            var projectType = await _projectTypeFieldRepository.GetAsync(id);
            return _mapper.Map<ProjectTypeField,ProjectTypeFieldDto>(projectType);
        }

        public async Task<Guid> Create(ProjectTypeFieldCreateDto dto)
        {
            var projectTypeField = _mapper.Map<ProjectTypeField>(dto);
            var result = await _projectTypeFieldRepository.AddAsync(projectTypeField);
            await _projectTypeFieldRepository.SaveChangesAsync();
            return result.Id;

        }
        public async System.Threading.Tasks.Task Update(ProjectTypeFieldDto dto)
        {
            var projectType= await _projectTypeFieldRepository.GetAsync(dto.id);

            if (projectType == null)
                throw new NotExistException("ProjectTypeField", dto.id);
            _mapper.Map(dto, projectType);
            await _projectTypeFieldRepository.SaveChangesAsync();
        }

        public async Task<List<ProjectTypeFieldDto>> GetList()
        {
            var list =  _projectTypeFieldRepository.GetAllAsync().ToList();
            return _mapper.Map<List<ProjectTypeField>, List<ProjectTypeFieldDto>>(list);
        }
        public async System.Threading.Tasks.Task CreateValueBatch(ProjectTypeFieldCreateDto[] dto)
        {
            var projectTypeField = _mapper.Map<ProjectTypeField[]>(dto);
            await _projectTypeFieldRepository.AddRange(projectTypeField); 
            await _projectTypeFieldRepository.SaveChangesAsync();
        }
       
        public async System.Threading.Tasks.Task<bool> DeleteBatchValue(ProjectTypeFieldDto[] dto)
        {
            var items = _mapper.Map<List<ProjectTypeField>>(dto);
            await _projectTypeFieldRepository.RemoveRange(items);
            await _projectTypeFieldRepository.SaveChangesAsync();
            return (true);
        }

        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _projectTypeFieldRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("ProjectType", id);
           
            _projectTypeFieldRepository.Delete(id);
            await _projectTypeFieldRepository.SaveChangesAsync();
        }

    }
}
