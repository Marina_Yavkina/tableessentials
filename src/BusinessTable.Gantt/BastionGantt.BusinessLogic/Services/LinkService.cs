﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class LinkService : ILinkService
    {
        private readonly IMapper _mapper;
        private readonly ILinkRepository _linkRepository;

        public LinkService(IMapper mapper, ILinkRepository linkRepository)
        {
            _mapper = mapper;
            _linkRepository = linkRepository;
        }
        public async Task<List<LinkDto>> GetPage(Guid[] taskIds,bool noTracking = false)
        {
            var list = await _linkRepository.GetPagedLinksAsync(taskIds);
            return _mapper.Map<List<LinkDto>>(list.ToList());

        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        public async Task<LinkDto> Get(Guid id)
        {
            var link = await _linkRepository.GetAsync(id);
            return _mapper.Map<Link,LinkDto>(link);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">>ДТО entityField</param>
        /// <returns>идентификатор</returns>
        public async Task<Guid> Create(LinkCreateDto dto)
        {
            var link = _mapper.Map<Link>(dto);
            var result = await _linkRepository.AddAsync(link);
            await _linkRepository.SaveChangesAsync();
            return result.Id;

        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="dto">ДТО entityField</param>
        public async System.Threading.Tasks.Task Update(LinkDto dto)
        {
            var field = await _linkRepository.GetAsync(dto.id);

            if (field == null)
                throw new NotExistException("Link", dto.id);
            _mapper.Map(dto, field);
            await _linkRepository.SaveChangesAsync();
        }

       public async System.Threading.Tasks.Task CreateEntityFieldBatch(LinkDto[] linkDto)
       {
         var links = _mapper.Map<Link[]>(linkDto);

                await _linkRepository.AddRange(links);
                await _linkRepository.SaveChangesAsync();
       }

        public async System.Threading.Tasks.Task DeleteEntityFields(LinkDto[] linkDto)
        {
            var links = _mapper.Map<Link[]>(linkDto);
            await _linkRepository.RemoveRange(links);
            await _linkRepository.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _linkRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("Link", id);
                //return BadRequest();
           
            _linkRepository.Delete(id);
            await _linkRepository.SaveChangesAsync();
        }

    }
}
