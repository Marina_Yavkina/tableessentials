﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class TaskService : ITaskService 
    {
        private readonly IMapper _mapper;
        private readonly ITaskRepository _taskRepository;
        private readonly ITaskValueRepository _taskValueRepository;
        private readonly IFieldRepository _fieldRepository;
        private readonly IInstanceRepository _instanceRepository;
        private readonly IProjectTypeRepository _projectTypeRepository;
        private readonly IAssignmentRepository _assignmentRepository;
        

        public TaskService(IMapper mapper, ITaskRepository repository,ITaskValueRepository taskValueRepository,IFieldRepository fieldRepository,
        IProjectTypeRepository projectTypeRepository, IInstanceRepository instanceRepository, IAssignmentRepository assignmentRepository)
        {
            _mapper = mapper;
            _taskRepository = repository;
            _taskValueRepository = taskValueRepository;
            _fieldRepository = fieldRepository;
            _instanceRepository = instanceRepository;
            _projectTypeRepository = projectTypeRepository;
            _assignmentRepository = assignmentRepository;
        }

    public async Task<PagedList<TaskViewModel>> GetTasksByProjectId(Guid id, int pageNumber, int pageSize)
    {
        var pagedList =  await _taskRepository.GetTaskByProjectIdPagedAsync(id,pageNumber, pageSize);
        var rootTask = await _taskRepository.GetById(id,false);
        var rootTaskVm =_mapper.Map<BastionGantt.Core.Domain.Task, TaskViewModel>(rootTask);
        var taskvm = _mapper.Map<List<BastionGantt.Core.Domain.Task>, List<TaskViewModel>>(pagedList.Entities);
        var res2 = new List<TaskViewModel>();
        res2.Add(rootTaskVm);
        foreach(TaskViewModel tv in taskvm)
        {
                //  находим значения полей для текущей задачи в values
                var takValues = _taskValueRepository.GetAllAsync().Where(x => x.TaskId == new Guid(tv.id));
                List<TaskValueFieldDto> values =new List<TaskValueFieldDto>();
                if (takValues.ToList().Count != 0)
                {
                    values = _fieldRepository.GetAllAsync().Join(takValues, c => c.Id, ct => ct.FieldId, (c,ct) => new TaskValueFieldDto{
                    id = ct.Id,
                    field_id = ct.FieldId,
                    task_id = ct.TaskId,
                    text_data = ct.TextData,
                    numeric_data = ct.NumericData,
                    bool_data = ct.BoolData,
                    instance_id = ct.InstanceId,
                    field = c.Name,
                    value_id = ct.ValueId,
                    type =  new TypeJsonDto { simple_type = c.Type.SimpleType, directory_id = c.Type.DirectoryId, instance_directory_id = c.Type.InstanceDirectoryId}
                    }).ToList(); 
                }   
                List<TaskValueView> tvList = new List<TaskValueView>();
                Instance instance = new Instance();
                if (values != null)  
                    foreach( var item in values )
                    {
                        string value = "";
                        if (item.text_data != null && item.text_data != "")
                            value = item.text_data;
                        else if (item.numeric_data != null )
                            value = item.numeric_data.ToString()!;
                        else if (item.bool_data != null)
                        {
                            if (item.bool_data == true)
                                value = "true";
                            else
                                value = "false"; 
                        }
                        else if (item.type.directory_id != null || item.type.directory_id != "")
                        {
                            if (item.instance_id != null)
                            {
                                Guid inst_id = (Guid)item.instance_id;
                                instance = await _instanceRepository.GetById(inst_id);
                                value = item.instance_id.ToString(); 
                            }
                        }
                        TaskValueView taskView = new TaskValueView{ field = item.field,value = value, type = item.type ,field_id = item.field_id.ToString()} ;
                        tvList.Add(taskView);   
                    }
                tv.values = tvList;
                res2.Add(tv);
            }              
                
        return new PagedList<TaskViewModel>
        {
            Entities = res2,
            Pagination = pagedList.Pagination
        };
    }
    public async Task<PagedList<TaskViewModel>> GetOnlyProjectsPage(int pageNumber, int pageSize)
    {
        ProjectType projectType = await _projectTypeRepository.GetByName("project");
        var tasks = await _taskRepository.GetOnlyProjectsAsync(pageNumber, pageSize );
        if (tasks.Entities != null)
        {
            var task = _mapper.Map<IEnumerable<TaskViewModel>>(tasks.Entities);
            return new PagedList<TaskViewModel>
            {
                Entities = task.ToList() ,
                Pagination = tasks.Pagination
            };
        }
        else
        {
            throw new NotFoundException("projects", "" );
        }
    }
    
    public async Task<TaskDto> Get(Guid id)
    {
        var task = await _taskRepository.GetAsync(id, true);

        if (task == null)
            throw new NotFoundException("projects", id);
        return _mapper.Map<TaskDto>(task);
    }

    public async Task<Guid> Create(TaskCreateDto taskdto)
    {
        try {
        var task = _mapper.Map<BastionGantt.Core.Domain.Task>(taskdto);
                if (taskdto.parent != null)
                {
                    var subtask = await _taskRepository.GetById((Guid)taskdto.parent);
                    task.ParentTask = subtask;
                }
                if (taskdto.project_type_id != null)
                {
                    var projectType = await _projectTypeRepository.GetById(new Guid(taskdto.project_type_id));
                    task.ProjectType = projectType;
                }
            
                task.Id = Guid.NewGuid();
                var new_task =  await _taskRepository.AddAsync(task);
                await  _taskRepository.SaveChangesAsync();
                if  (!(taskdto.user == null))
                {
                    var users = _mapper.Map<List<Assignment>>(taskdto.user);
                    foreach(Assignment user in  users )
                    {
                        user.TaskId =  new_task.Id;
                        await _assignmentRepository.AddAsync(user);
                    }
                }
                await _assignmentRepository.SaveChangesAsync();
                var item = await _taskRepository.GetById(task.Id);

                if (item == null)
                    throw new NotFoundException("задача", task.Id);
                return item.Id;
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new Guid();
        }
    }

    public async Task<Guid> Update(TaskDto taskdto)
    {
        var item = await _taskRepository.GetById(taskdto.id,true);
        if (item == null)
            throw new NotExistException("задача", taskdto.id);
        var task = _mapper.Map<BastionGantt.Core.Domain.Task>(taskdto);
        if (taskdto.parent != null)
        {
            var subtask = await _taskRepository.GetById(new Guid(taskdto.parent.ToString()),true);
            task.ParentTask = subtask;
        }
        if (taskdto.project_type_id != null)
        {
            Guid pr_id = new Guid(taskdto.project_type_id!);
            var projectType = await _projectTypeRepository.GetById(pr_id);
            task.ProjectType = projectType;
            
        }
        else if  (taskdto.type == "project")
        {
            task.ProjectTypeId = item.ProjectTypeId;
            
        }
        _taskRepository.Update(task);
        await _taskRepository.SaveChangesAsync();
        //  adding new assignments
        if  (!(taskdto.user== null))
        {
            var users = _mapper.Map<List<Assignment>>(taskdto.user);
            foreach(Assignment user in  users )
            { 
                var assign = await _assignmentRepository.GetById(user.Id,true);
                if (assign == null)
                {
                    user.Id =  Guid.NewGuid();
                    user.TaskId =  task.Id;
                    await _assignmentRepository.AddAsync(user);
                }
            }
            //updating existing assignments or deleting old
            var assigns =  await _assignmentRepository.GetByTaskIdAsync(task.Id);
            if (assigns != null){
                foreach(Assignment assign in  assigns )
                {
                    var assgn = users.Where(c => c.Id == assign.Id).FirstOrDefault();
                    if (assgn != null)
                    {
                        _assignmentRepository.Update(assgn);
                    }
                    else
                    {
                        _assignmentRepository.Delete(assign.Id);
                    }
                }
            }
            await _assignmentRepository.SaveChangesAsync();
        }    
        return taskdto.id;       
    }

    public async System.Threading.Tasks.Task Remove(Guid id)
    {
        var item = await _taskRepository.GetById(id);
        if (item == null)
            Console.WriteLine("Exc");
            //return BadRequest();
        var assigns =  await _assignmentRepository.GetByTaskIdAsync(id);
        if (assigns != null)
        {
            await _assignmentRepository.RemoveRange(assigns);
            await _assignmentRepository.SaveChangesAsync();
        }
        _taskRepository.Delete(id);
        await _taskRepository.SaveChangesAsync();
    }

        public async Task<PagedList<TaskViewModel>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _taskRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<TaskViewModel>
            {
                Entities = _mapper.Map<List<Core.Domain.Task>, List<TaskViewModel>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };
        }
    }
}
