﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class InstanceService : IInstanceService
    {
        private readonly IMapper _mapper;
        private readonly IInstanceRepository _instanceRepository;

        public InstanceService(IMapper mapper,  IInstanceRepository instanceRepository)
        {
            _mapper = mapper;
             _instanceRepository = instanceRepository;
        }
        public async Task<PagedList<InstanceDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _instanceRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<InstanceDto>
            {
                Entities = _mapper.Map<List<Instance>, List<InstanceDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };

        }

        public async Task<List<InstanceDto>> GetAllInstancesByEntityId (Guid id)
        {
            var list  =  _instanceRepository.GetAllAsync().Where(c => c.EntityId == id).ToList();
            return _mapper.Map<List<InstanceDto>>(list);
        }
            

        public async Task<InstanceDto> Get(Guid id)
        {
            var instance = await _instanceRepository.GetAsync(id);
            return _mapper.Map<InstanceDto>(instance);
        }

        public async Task<Guid> Create(InstanceCreateDto dto)
        {
            var instance = _mapper.Map<Instance>(dto);
            var result = await _instanceRepository.AddAsync(instance);
            await _instanceRepository.SaveChangesAsync();
            return result.Id;

        }
        public async System.Threading.Tasks.Task Update(InstanceDto dto)
        {
            var instance = await _instanceRepository.GetAsync(dto.id);

            if (instance == null)
                throw new NotExistException("Instance", dto.id);
            _mapper.Map(dto, instance);
            await _instanceRepository.SaveChangesAsync();
        }

        public async Task<List<InstanceDto>> GetList()
        {
            var list =  _instanceRepository.GetAllAsync().ToList();
            return _mapper.Map<List<Instance>, List<InstanceDto>>(list);
        }

        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _instanceRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("Instance", id);
           
            _instanceRepository.Delete(id);
            await _instanceRepository.SaveChangesAsync();
        }
    }
}
