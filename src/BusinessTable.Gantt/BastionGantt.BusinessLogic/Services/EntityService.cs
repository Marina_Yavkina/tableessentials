﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class EntityService : IEntityService
    {
        private readonly IMapper _mapper;
        private readonly IEntityRepository _entityRepository;

        public EntityService(IMapper mapper, IEntityRepository entityRepository)
        {
            _mapper = mapper;
            _entityRepository = entityRepository;
        }
        public async Task<PagedList<EntityDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _entityRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<EntityDto>
            {
                Entities = _mapper.Map<List<Entity>, List<EntityDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };

        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        public async Task<EntityDto> Get(Guid id)
        {
            var entity = await _entityRepository.GetAsync(id);
            return _mapper.Map<Entity,EntityDto>(entity);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="entitydto">ДТО</para
        /// <returns>идентификатор</returns>
        public async Task<Guid> Create(EntityCreateDto entitydto)
        {
             var entity = _mapper.Map<Entity>(entitydto);
            var result = await _entityRepository.AddAsync(entity);
            await _entityRepository.SaveChangesAsync();
            return result.Id;

        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="ientityd">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        public async System.Threading.Tasks.Task Update(EntityDto entitydto)
        {
            var field = await _entityRepository.GetAsync(entitydto.id);

            if (field == null)
                throw new NotExistException("Entity", entitydto.id);
            _mapper.Map(entitydto, field);
            await _entityRepository.SaveChangesAsync();
        }

        public async Task<List<EntityDto>> GetList()
        {
            var list =  _entityRepository.GetAllAsync().ToList();
            return _mapper.Map<List<Entity>, List<EntityDto>>(list);
        }

        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _entityRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("ProjectType", id);
           
            _entityRepository.Delete(id);
            await _entityRepository.SaveChangesAsync();
        }

    }
}
