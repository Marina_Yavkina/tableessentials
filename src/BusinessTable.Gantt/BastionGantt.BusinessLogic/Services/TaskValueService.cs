using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class TaskValueService : ITaskValueService 
    {
        private readonly IMapper _mapper;
        private readonly ITaskValueRepository _taskValueRepository;
        

        public TaskValueService(IMapper mapper, ITaskValueRepository taskValueRepository)
        {
            _mapper = mapper;
            _taskValueRepository = taskValueRepository;
        }

        public async Task<PagedList<TaskValueDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _taskValueRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<TaskValueDto>
            {
                Entities = _mapper.Map<List<TaskValue>, List<TaskValueDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };
        }
        public async Task<TaskValueDto> Get(Guid id)
        {
            var tv = await _taskValueRepository.GetAsync(id);   
            return _mapper.Map<TaskValue,TaskValueDto>(tv);
        }
        public async Task<Guid> Create(TaskValueCreateDto valuedto)
        {
            var value = _mapper.Map<TaskValue>(valuedto);
            var result = await _taskValueRepository.AddAsync(value);
            await _taskValueRepository.SaveChangesAsync();
            return result.Id;
        }
        public async System.Threading.Tasks.Task Update(TaskValueDto dto)
        {
            var taskvalue = await _taskValueRepository.GetAsync((Guid)dto.id);

            if (taskvalue == null)
                throw new NotExistException("TaskValue", (Guid)dto.id);
           _mapper.Map(dto, taskvalue);
            await _taskValueRepository.SaveChangesAsync();
        
        }
        public async System.Threading.Tasks.Task CreateValueBach(TaskValueCreateDto[] valuedDto)
        {
            var values = _mapper.Map<List<TaskValue>>(valuedDto);
            await _taskValueRepository.AddRange(values);
            await _taskValueRepository.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task AddToArchive(Guid id)
        {
             var taskValue = await _taskValueRepository.GetAsync(id);

            if (taskValue == null)
                throw new NotExistException("TaskValue", id);
            taskValue.IsDeleted = true;
            await _taskValueRepository.SaveChangesAsync();
        }
        public async Task<List<TaskValueDto>> GetTaskValueByTask(Guid taskId)
        {
            var items = await _taskValueRepository.GetByTaskIdAsync(taskId);
            return _mapper.Map<List<TaskValueDto>>(items);

        }
        public async System.Threading.Tasks.Task RemoveBatch(Guid id)
        {
            var item = await _taskValueRepository.GetByTaskIdAsync(id);
            if (item == null)
                 throw new NotExistException("TaskValues of taskId", id);
            await _taskValueRepository.RemoveRange(item);
            await _taskValueRepository.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task DeleteValue(Guid id)
        {
            _taskValueRepository.Delete(id);
            await _taskValueRepository.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _taskValueRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("Value", id);
           
            _taskValueRepository.Delete(id);
            await _taskValueRepository.SaveChangesAsync();
        }
        public async Task<List<TaskValueDto>> GetList()
        {
            var list =  _taskValueRepository.GetAllAsync().ToList();
            return _mapper.Map<List<TaskValue>, List<TaskValueDto>>(list);
        }
    }
    
}
