﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class ProjectTypeService : IProjectTypeService
    {
        private readonly IMapper _mapper;
        private readonly IProjectTypeRepository _projectTypeRepository;

        public ProjectTypeService(IMapper mapper,IProjectTypeRepository projectTypeRepository)
        {
            _mapper = mapper;
            _projectTypeRepository = projectTypeRepository;
        }
        public async Task<PagedList<ProjectTypeDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _projectTypeRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<ProjectTypeDto>
            {
                Entities = _mapper.Map<List<ProjectType>, List<ProjectTypeDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };

        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        public async Task<ProjectTypeDto> Get(Guid id)
        {
            var projectType = await _projectTypeRepository.GetAsync(id);
            return _mapper.Map<ProjectType,ProjectTypeDto>(projectType);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        public async Task<Guid> Create(ProjectTypeCreateDto dto)
        {
            var projectType = _mapper.Map<ProjectType>(dto);
            var result = await _projectTypeRepository.AddAsync(projectType);
            await _projectTypeRepository.SaveChangesAsync();
            return result.Id;

        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="dto">ДТО зоны</param>
        public async System.Threading.Tasks.Task Update(ProjectTypeDto dto)
        {
            var projectType= await _projectTypeRepository.GetAsync(dto.id);

            if (projectType == null)
                throw new NotExistException("ProjectType", dto.id);
            _mapper.Map(dto, projectType);
            await _projectTypeRepository.SaveChangesAsync();
        }

        public async Task<List<ProjectTypeDto>> GetList()
        {
            var list =  _projectTypeRepository.GetAllAsync().ToList();
            return _mapper.Map<List<ProjectType>, List<ProjectTypeDto>>(list);
        }

        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _projectTypeRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("ProjectType", id);
           
            _projectTypeRepository.Delete(id);
            await _projectTypeRepository.SaveChangesAsync();
        }

    }
}
