﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class FieldService : IFieldService
    {
        private readonly IMapper _mapper;
        private readonly IFieldRepository _fieldRepository;
        private readonly IProjectTypeFieldRepository _projectTypeFieldRepository;
        private readonly IEntityFieldRepository _entityFieldRepository;
        private readonly ITaskRepository _taskRepository;
        public FieldService(IMapper mapper, IFieldRepository fieldRepository,IEntityFieldRepository entityFieldRepository,
        ITaskRepository taskRepository,IProjectTypeFieldRepository projectTypeFieldRepository)
        {
            _mapper = mapper;
            _fieldRepository = fieldRepository;
            _entityFieldRepository = entityFieldRepository;
            _projectTypeFieldRepository = projectTypeFieldRepository;
            _taskRepository = taskRepository;
        }
        public async Task<PagedList<FieldDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _fieldRepository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<FieldDto>
            {
                Entities = _mapper.Map<List<Field>, List<FieldDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };

        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        public async Task<FieldDto> Get(Guid id)
        {
            var field = await _fieldRepository.GetAsync(id);
            return _mapper.Map<Field,FieldDto>(field);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        public async Task<Guid> Create(FieldCreateDto valuedto)
        {
             var value = _mapper.Map<Field>(valuedto);
            var result = await _fieldRepository.AddAsync(value);
            await _fieldRepository.SaveChangesAsync();
            return result.Id;

        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        public async System.Threading.Tasks.Task Update(FieldDto fielddto)
        {
            var field = await _fieldRepository.GetAsync(fielddto.id);

            if (field == null)
                throw new NotExistException("Field", fielddto.id);
            _mapper.Map(fielddto, field);
            await _fieldRepository.SaveChangesAsync();
        }


        /// <summary>
        /// Поместить в архив
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <param name="employeeId">идентификатор сотрудника клуба</param>
       // Task AddToArchive(Guid id, Guid employeeId);

        public async Task<List<FieldDto>> GetFieldsByEntityId(Guid id)
        {
            var fields = from e in _entityFieldRepository.GetAllAsync().Where(c => c.EntityId == id)
            join f in _fieldRepository.GetAllAsync() on e.FieldId equals f.Id
            select new Field { Id = f.Id, Decsription = f.Decsription, Name = f.Name, CreateDate = f.CreateDate, Type = f.Type };
            return _mapper.Map<List<FieldDto>>(fields.ToList());
        }
        public async Task<List<FieldDto>> SearchFieldsByEntityId(Guid id,string text)
        {
            var fields = from e in _entityFieldRepository.GetAllAsync().Where(c => c.EntityId == id)
            join f in _fieldRepository.GetAllAsync().Where(c => c.Name.Contains(text)) on e.FieldId equals f.Id
            select new Field { Id = f.Id, Decsription = f.Decsription, Name = f.Name, CreateDate = f.CreateDate, Type = f.Type };
            return _mapper.Map<List<FieldDto>>(fields.ToList());
        }
        public async Task<List<FieldDto>> GetFieldsByProjectTypeId(Guid id)
        {
            var pr = _projectTypeFieldRepository.GetAllAsync().Where(p => p.ProjectTypeId == id);
            var fields = from e in pr
            join f in _fieldRepository.GetAllAsync() on e.FieldId equals f.Id
            select new Field { Id = f.Id, Decsription = f.Decsription, Name = f.Name, CreateDate = f.CreateDate, Type = f.Type };
            return _mapper.Map<List<FieldDto>>(fields.ToList());

        }
        public async Task<List<FieldDto>> GetFieldsByProjectId(Guid id)
        {
            var project= await _taskRepository.GetById(id);
            var pr = _projectTypeFieldRepository.GetAllAsync().Where(p => p.ProjectTypeId == project.ProjectTypeId);
            var fields = from e in pr
            join f in _fieldRepository.GetAllAsync() on e.FieldId equals f.Id
            select new Field { Id = f.Id, Decsription = f.Decsription, Name = f.Name, CreateDate = f.CreateDate, Type = f.Type };
            return _mapper.Map<List<FieldDto>>(fields.ToList());

        }

        public async Task<List<FieldDto>> GetList()
        {
            var list =  _fieldRepository.GetAllAsync().ToList();
            return _mapper.Map<List<Field>, List<FieldDto>>(list);
        }

        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _fieldRepository.GetAsync(id);
            if (item == null)
                 throw new NotExistException("Field", id);
           
            _fieldRepository.Delete(id);
            await _fieldRepository.SaveChangesAsync();
        }
    }
}
