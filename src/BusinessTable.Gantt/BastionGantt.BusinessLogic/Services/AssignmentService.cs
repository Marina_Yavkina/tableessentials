﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using BastionGantt.BusinessLogic.Models;
//using GMS.Core.BusinessLogic.Exceptions;
using BastionGantt.Core.Abstractions.Repositories;
using BastionGantt.Core.Domain;
using BastionGantt.Core.Models;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Exceptions;
using BastionGantt.BusinessLogic.Attributes;

namespace  BastionGantt.BusinessLogic.Services
{
    [Inject(ServiceLifetime.Scoped)]
    public class AssignmentService : IAssignmentService
    {
        private readonly IMapper _mapper;
        private readonly IAssignmentRepository _assignmentRpository;

        public AssignmentService(IMapper mapper,IAssignmentRepository assignmentRpository)
        {
            _mapper = mapper;
            _assignmentRpository = assignmentRpository;
        }
        public async Task<PagedList<AssignmentDto>> GetPage(int pageNumber, int pageSize)
        {
            var pagedList = await _assignmentRpository.GetPagedAsync(pageNumber,pageSize);
            return new PagedList<AssignmentDto>
            {
                Entities = _mapper.Map<List<Assignment>, List<AssignmentDto>>(pagedList.Entities),
                Pagination = pagedList.Pagination
            };

        }

        public async Task<List<AssignmentDto>> GetList()
        {
            var list = await _assignmentRpository.GetListAsync();
            return _mapper.Map<List<AssignmentDto>>(list);
        }

        /// <summary>
        /// Получить
        /// </summary>
        /// <param name="id">идентификатор</param>
        /// <returns>ДТО зоны</returns>
        public async Task<AssignmentDto> Get(Guid id)
        {
            var entity = await _assignmentRpository.GetAsync(id);
            return _mapper.Map<Assignment,AssignmentDto>(entity);
        }

        /// <summary>
        /// Создать
        /// </summary>
        /// <param name="dto">ДТО</para
        /// <returns>идентификатор</returns>
        public async Task<Guid> Create(AssignmentCreateDto dto)
        {
             var assignment = _mapper.Map<Assignment>(dto);
            var result = await _assignmentRpository.AddAsync(assignment);
            await _assignmentRpository.SaveChangesAsync();
            return result.Id;

        }

        /// <summary>
        /// Изменить
        /// </summary>
        /// <param name="ientityd">идентификатор</param>
        /// <param name="dto">ДТО зоны</param>
        public async System.Threading.Tasks.Task Update(AssignmentDto dto)
        {
            var field = await _assignmentRpository.GetAsync(new Guid(dto.id));

            if (field == null)
                throw new NotExistException("Assignment", dto.id);
            _mapper.Map(dto, field);
            await _assignmentRpository.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Remove(Guid id)
        {
            var item = await _assignmentRpository.GetById(id);
            if (item == null)
            {
                throw new NotExistException("Assignment", id);
            }
            _assignmentRpository.Delete(id);
             await _assignmentRpository.SaveChangesAsync();
        }
    }
}

