﻿using Microsoft.Extensions.DependencyInjection;

namespace BastionGantt.BusinessLogic.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectAttribute : Attribute
    {
        public InjectAttribute(ServiceLifetime lifetime) 
        { 
            LifeTime = lifetime;
        }
        public ServiceLifetime LifeTime { get; }
    }
}
