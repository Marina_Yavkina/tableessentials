using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{

    public class TypeJsonDto
    {
        public string simple_type { get; set; }
        public string directory_id { get; set; }
        public string instance_directory_id { get; set; }
        public string task_id { get;set; }
    }
}