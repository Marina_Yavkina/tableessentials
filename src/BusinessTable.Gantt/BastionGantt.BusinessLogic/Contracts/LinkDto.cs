using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class LinkDto
    {
        public Guid id { get; set; }
        public string type { get; set; }
        public Guid? sourceTaskId { get; set; }
        public Guid? targetTaskId { get; set; }
    }
}