using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class LinkCreateDto
    {
        public string type { get; set; }
        public Guid? sourceTaskId { get; set; }
        public Guid? targetTaskId { get; set; }
    }
}