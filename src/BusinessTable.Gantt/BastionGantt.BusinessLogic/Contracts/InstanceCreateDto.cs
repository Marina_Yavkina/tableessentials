using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class InstanceCreateDto
    {
        public string name { get; set; }
        public string decsription { get; set; }
        public Guid entity_id {get;set;}
    }
}
