using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class FieldCreateDto
    {
        public string name { get; set; }
        public string decsription { get; set; }
        public TypeJsonDto type { get; set; }
    }
}
