namespace BastionGantt.BusinessLogic.Contracts
{
     public class TaskValueCreateDto
    {
        public Guid field_id {get;set;}
        public Guid? task_id {get;set;}
        public string? text_data {get;set;}
        public double? numeric_data {get;set;}
        public bool? bool_data {get;set;}
        public Guid? instance_id {get;set;}
        public Guid? value_id {get;set;}
       
    }
    // public class TaskValueView
    // {
    //     public string field { get; set; }
    //     public string field_id { get; set; }
    //     public string value { get; set; }
    //     public TypeJsonDto type {get;set;}
    // }
}