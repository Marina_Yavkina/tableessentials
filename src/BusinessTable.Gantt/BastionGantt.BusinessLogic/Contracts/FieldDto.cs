using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class FieldDto
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string create_date { get; set; }
        public string decsription { get; set; }
        public TypeJsonDto type { get; set; }
    }
}
