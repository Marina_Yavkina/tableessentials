using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class ProjectTypeFieldCreateDto
    {
        public Guid project_type_id {get;set;}
        public Guid field_id {get;set;}
    }
}
