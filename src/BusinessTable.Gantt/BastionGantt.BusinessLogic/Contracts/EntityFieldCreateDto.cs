using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class EntityFieldCreateDto
    {
        public Guid entity_id {get;set;}
        public Guid field_id {get;set;}
    }
}
