using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class ProjectTypeCreateDto
    {
        public string name { get; set; }
        public string? decsription { get; set; }
    }
}