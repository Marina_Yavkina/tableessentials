using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class EntityFieldDto
    {
        public Guid id { get; set; }
        public Guid entity_id {get;set;}
        public Guid field_id {get;set;}
    }
}
