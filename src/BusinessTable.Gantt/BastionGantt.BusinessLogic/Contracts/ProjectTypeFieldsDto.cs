using System;
using System.Collections.Generic;

namespace BastionGantt.BusinessLogic.Contracts
{
    public class ProjectTypeFieldDto
    {
        public Guid id { get; set; }
        public Guid project_type_id {get;set;}
        public Guid field_id {get;set;}
    }
}
