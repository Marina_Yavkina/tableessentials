﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BastionGantt.WebHost.Migrations
{
    public partial class instanceConfigAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Instance_Entities_EntityId",
                table: "Instance");

            migrationBuilder.DropForeignKey(
                name: "FK_Instance_Instance_ParentInstanceId",
                table: "Instance");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskValues_Instance_InstanceId",
                table: "TaskValues");

            migrationBuilder.DropForeignKey(
                name: "FK_Values_Instance_InstanceId",
                table: "Values");

            migrationBuilder.DropForeignKey(
                name: "FK_Values_Instance_ValueInstanceId",
                table: "Values");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Instance",
                table: "Instance");

            migrationBuilder.DropIndex(
                name: "IX_Instance_ParentInstanceId",
                table: "Instance");

            migrationBuilder.DropColumn(
                name: "ParentInstanceId",
                table: "Instance");

            migrationBuilder.RenameTable(
                name: "Instance",
                newName: "Instances");

            migrationBuilder.RenameIndex(
                name: "IX_Instance_EntityId",
                table: "Instances",
                newName: "IX_Instances_EntityId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartDate",
                table: "Assignments",
                type: "timestamp",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndDate",
                table: "Assignments",
                type: "timestamp",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Instances",
                type: "character varying(256)",
                unicode: false,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Instances",
                type: "boolean",
                nullable: false,
                defaultValue: false,
                oldClrType: typeof(bool),
                oldType: "boolean");

            migrationBuilder.AlterColumn<string>(
                name: "Decsription",
                table: "Instances",
                type: "character varying(1000)",
                unicode: false,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "Instances",
                type: "timestamp",
                nullable: false,
                defaultValueSql: "now()",
                oldClrType: typeof(DateTime),
                oldType: "timestamp with time zone");

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "Instances",
                type: "uuid",
                nullable: false,
                defaultValueSql: "uuid_generate_v4()",
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Instances",
                table: "Instances",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Instances_Entities_EntityId",
                table: "Instances",
                column: "EntityId",
                principalTable: "Entities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskValues_Instances_InstanceId",
                table: "TaskValues",
                column: "InstanceId",
                principalTable: "Instances",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Values_Instances_InstanceId",
                table: "Values",
                column: "InstanceId",
                principalTable: "Instances",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Values_Instances_ValueInstanceId",
                table: "Values",
                column: "ValueInstanceId",
                principalTable: "Instances",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Instances_Entities_EntityId",
                table: "Instances");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskValues_Instances_InstanceId",
                table: "TaskValues");

            migrationBuilder.DropForeignKey(
                name: "FK_Values_Instances_InstanceId",
                table: "Values");

            migrationBuilder.DropForeignKey(
                name: "FK_Values_Instances_ValueInstanceId",
                table: "Values");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Instances",
                table: "Instances");

            migrationBuilder.RenameTable(
                name: "Instances",
                newName: "Instance");

            migrationBuilder.RenameIndex(
                name: "IX_Instances_EntityId",
                table: "Instance",
                newName: "IX_Instance_EntityId");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartDate",
                table: "Assignments",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndDate",
                table: "Assignments",
                type: "timestamp with time zone",
                nullable: true,
                oldClrType: typeof(DateTime),
                oldType: "timestamp",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Instance",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(256)",
                oldUnicode: false);

            migrationBuilder.AlterColumn<bool>(
                name: "IsDeleted",
                table: "Instance",
                type: "boolean",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "boolean",
                oldDefaultValue: false);

            migrationBuilder.AlterColumn<string>(
                name: "Decsription",
                table: "Instance",
                type: "text",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(1000)",
                oldUnicode: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreateDate",
                table: "Instance",
                type: "timestamp with time zone",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "timestamp",
                oldDefaultValueSql: "now()");

            migrationBuilder.AlterColumn<Guid>(
                name: "Id",
                table: "Instance",
                type: "uuid",
                nullable: false,
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldDefaultValueSql: "uuid_generate_v4()");

            migrationBuilder.AddColumn<Guid>(
                name: "ParentInstanceId",
                table: "Instance",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Instance",
                table: "Instance",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_Instance_ParentInstanceId",
                table: "Instance",
                column: "ParentInstanceId");

            migrationBuilder.AddForeignKey(
                name: "FK_Instance_Entities_EntityId",
                table: "Instance",
                column: "EntityId",
                principalTable: "Entities",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Instance_Instance_ParentInstanceId",
                table: "Instance",
                column: "ParentInstanceId",
                principalTable: "Instance",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskValues_Instance_InstanceId",
                table: "TaskValues",
                column: "InstanceId",
                principalTable: "Instance",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Values_Instance_InstanceId",
                table: "Values",
                column: "InstanceId",
                principalTable: "Instance",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Values_Instance_ValueInstanceId",
                table: "Values",
                column: "ValueInstanceId",
                principalTable: "Instance",
                principalColumn: "Id");
        }
    }
}
