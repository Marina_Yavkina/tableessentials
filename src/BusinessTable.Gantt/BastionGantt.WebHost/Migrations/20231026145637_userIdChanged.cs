﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BastionGantt.WebHost.Migrations
{
    public partial class userIdChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Assignments",
                type: "character varying(40)",
                unicode: false,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(25)",
                oldUnicode: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "Assignments",
                type: "character varying(25)",
                unicode: false,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character varying(40)",
                oldUnicode: false);
        }
    }
}
