using BastionGantt.DataAccess.Context;
using BastionGantt.WebHost.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

public class DataBaseContextFactory : IDesignTimeDbContextFactory<DatabaseContext>
{      

    DatabaseContext IDesignTimeDbContextFactory<DatabaseContext>.CreateDbContext(string[] args)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        var builder = new DbContextOptionsBuilder<DatabaseContext>();
        var connectionString = configuration.GetConnectionString("BastionCore");
        builder.UseNpgsql(connectionString,b => b.MigrationsAssembly("BastionGantt.WebHost"));

        return new DatabaseContext(builder.Options);
    }
}