﻿namespace BastionGantt.WebHost.HttpClients.Abstractions
{
    public interface IBaseHttpClient
    {
        string? Token { set; }
    }
}
