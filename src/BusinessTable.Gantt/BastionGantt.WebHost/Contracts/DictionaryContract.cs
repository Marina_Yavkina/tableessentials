using System.Collections.Generic;
namespace BastionGantt.WebHost.Contracts
{
    public class DictionaryContract
    {
        public string EntityName { get; set; }
        public string FieldName { get; set; }
        public int FieldType { get; set; }
        public List<InstancContr> Instance {get;set;}
    }
    public class InstancContr
    {
        public string InstanceName { get; set; }
        public List<ValueContr> Values {get;set;}
    }
    public class ValueContr
    {
        public bool? bool_value { get; set; }
        public string? text_value { get; set; }
        public double? numeric_value { get; set; }
    }
}
