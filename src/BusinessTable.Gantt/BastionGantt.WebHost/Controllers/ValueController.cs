﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class ValueController : BaseController<IValueService>
    {
        public ValueController(IValueService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка сущностей
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<InstanceDto></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAll()
        {
            var list = await _service.GetList();
             //var list1 = await _service.GetDictionaries();
            return Ok(list);
        }

       
        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="request">информация о сущности</param>
        /// <returns>идентификатор сущности</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(ValueCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию о сущности
        /// </summary>
        /// <param name="id">идендификатор сущности</param>
        /// <param name="request">информация о сущности</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTask(ValueDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

        [HttpGet("[action]/GetValueByInstanceId{id}")]
        public async Task<IActionResult> GetValueByInstanceId(Guid id)
        {
            var items =  await _service.GetValueByInstance(id);
            return Ok(items);
        }       

        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteValue(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
         [HttpPost("batch")]
        public async Task<IActionResult> CreateValueBatch(ValueCreateDto[] valueDto)
        {
            await _service.CreateValueBatch(valueDto);
            return Ok();
        }

         /// <summary>
        /// Удалить значения инстанса
        /// </summary>
        /// <param name="id">идендификатор инстанса</param>
        /// <returns></returns>

        [HttpDelete("batch/{id}")]
        public async Task<ActionResult> DeleteValues(Guid id)
        {
            await _service.DeleteBatchValue(id);
            return Ok();
        }
    }
}
