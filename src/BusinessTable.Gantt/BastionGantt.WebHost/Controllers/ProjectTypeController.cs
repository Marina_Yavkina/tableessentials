﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class ProjectTypeController : BaseController<IProjectTypeService>
    {
        public ProjectTypeController(IProjectTypeService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка типов прокта 
        /// </summary>
        /// <returns>List<AssignViewModel></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetProjectTypes()
        {
            var list = await _service.GetList();
            return Ok(list);
        }

         /// <summary>
        /// Получение списка типов проекта
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>AssignViewModel</returns>
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetProjectType(Guid id){
            var link = await _service.Get(id);
            return Ok(link);
        }

       
        /// <summary>
        /// Добавить тип проекта
        /// </summary>
        /// <param name="request">информация о типе проекта</param>
        /// <returns>идентификатор типа проекта</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(ProjectTypeCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию типа проекта
        /// </summary>
        /// <param name="id">идендификатор типа проекта</param>
        /// <param name="request">информация о типе проекта</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateProjectType( ProjectTypeDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

        /// <summary>
        /// Удалить типа проекта
        /// </summary>
        /// <param name="id">идентификатор типа проекта</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteProjectType(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
    }
}
