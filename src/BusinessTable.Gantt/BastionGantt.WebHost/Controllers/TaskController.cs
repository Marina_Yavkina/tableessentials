﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class TaskController : BaseController<ITaskService>
    {
        public TaskController(ITaskService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка задач
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>
        [AllowAnonymous]
        [HttpGet("[action]/{pageNumber}:{pageSize}")]
        public async Task<IActionResult> GetPage(int pageNumber = 1, int pageSize = 6)
        {
            var pagedList = await _service.GetPage(pageNumber, pageSize);

            return Ok(new ResponseWrapper<List<TaskViewModel>>(pagedList.Entities, pagedList.Pagination));
        }

         /// <summary>
        /// Получение списка проектов
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>
        [HttpGet("[action]/projects/{pageNumber}:{pageSize}")]

        public async Task<IActionResult> GetProjects(int pageNumber = 1, int pageSize = 6){
            var pagedList = await _service.GetOnlyProjectsPage(pageNumber, pageSize);
            return Ok(new ResponseWrapper<List<TaskViewModel>>(pagedList.Entities, pagedList.Pagination));
        }

         /// <summary>
        /// Получение списка задач по проекту
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>

        [AllowAnonymous]
        [HttpGet("[action]/projects/{pageNumber}:{pageSize}:{id}")]
        public async Task<IActionResult> GetTasksByProject(string id,int pageNumber = 1, int pageSize = 6)
        {
            var pagedList = await _service.GetTasksByProjectId(new Guid(id), pageNumber, pageSize);

            return Ok(new ResponseWrapper<List<TaskViewModel>>(pagedList.Entities, pagedList.Pagination));
        }

        /// <summary>
        /// Получение информации о задаче
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>TaskDto</returns>
        [AllowAnonymous]
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> Get(Guid id)
        {
            var taskDto = await _service.Get(id);
            return Ok(taskDto);
        }

        /// <summary>
        /// Добавить фитнес клуб
        /// </summary>
        /// <param name="request">информация о клубе</param>
        /// <returns>идентификатор клуба</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(TaskCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию фитнес клуба
        /// </summary>
        /// <param name="id">идендификатор клуба</param>
        /// <param name="request">информация о клубе</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<TaskViewModel>> UpdateTask(TaskDto request)
        {
            var id = await _service.Update(request);
            return Ok(id.ToString());            
        }

        /// <summary>
        /// Поместить клуб в архив
        /// </summary>
        /// <param name="id">идентификатор клуба</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteTask(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
    }
}
