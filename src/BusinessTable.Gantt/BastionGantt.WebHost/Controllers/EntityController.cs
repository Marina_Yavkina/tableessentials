﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
  
    public class EntityController : BaseController<IEntityService>
    {
        public EntityController(IEntityService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка сущностей
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>PagedList<EntityDto></returns>
         
        [HttpGet("[action]/{pageNumber}:{pageSize}")]
        public async Task<IActionResult> GetPage(int pageNumber = 1, int pageSize = 6)
        {
            var pagedList = await _service.GetPage(pageNumber, pageSize);

            return Ok(new ResponseWrapper<List<EntityDto>>(pagedList.Entities, pagedList.Pagination));
        }

        /// <summary>
        /// Получение списка сущностей
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<EntityDto></returns>
       
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAll()
        {
            var list = await _service.GetList();

            return Ok(list);
        }

       
        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="request">информация о сущности</param>
        /// <returns>идентификатор сущности</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(EntityCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию о сущности
        /// </summary>
        /// <param name="id">идендификатор сущности</param>
        /// <param name="request">информация о сущности</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTask(EntityDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

        /// <summary>
        /// удалить сущность
        /// </summary>
        /// <param name="id">идентификатор сущности</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteTask(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
    }
}
