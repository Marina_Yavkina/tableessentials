﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class LinkController : BaseController<ILinkService>
    {
        public LinkController(ILinkService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка задач
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetLinks([FromQuery]Guid[] taskIds)
        {
            var list = await _service.GetPage(taskIds);
            try {
            var result = _mapper.Map<List<LinkViewModel>>(list);
            return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

         /// <summary>
        /// Получение списка проектов
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetLinks(Guid id){
            var link = await _service.Get(id);
            var result = _mapper.Map<LinkViewModel>(link);
            return Ok(result);
        }

       
        /// <summary>
        /// Добавить фитнес клуб
        /// </summary>
        /// <param name="request">информация о клубе</param>
        /// <returns>идентификатор клуба</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(LinkCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию фитнес клуба
        /// </summary>
        /// <param name="id">идендификатор клуба</param>
        /// <param name="request">информация о клубе</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTask(LinkDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

        /// <summary>
        /// Поместить клуб в архив
        /// </summary>
        /// <param name="id">идентификатор клуба</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteTask(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
    }
}
