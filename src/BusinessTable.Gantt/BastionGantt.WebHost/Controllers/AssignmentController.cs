﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class AssignmentController : BaseController<IAssignmentService>
    {
        public AssignmentController(IAssignmentService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка назначений
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<AssignViewModel></returns>
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAssignment(int pageNumber = 1, int pageSize = 6)
        {
            try {
            var list = await _service.GetPage(pageNumber,pageSize);
            var result = _mapper.Map<List<AssignViewModel>>(list.Entities);
            return Ok(result);
            }
            catch (Exception ex) {
                var s = ex.Message;
                return BadRequest(s);
            }
        }

         /// <summary>
        /// Получение списка назначений
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>AssignViewModel</returns>
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetProjects(Guid id){
            var link = await _service.Get(id);
            var result = _mapper.Map<AssignViewModel>(link);
            return Ok(result);
        }

       
        /// <summary>
        /// Добавить назначение
        /// </summary>
        /// <param name="request">информация о клубе</param>
        /// <returns>идентификатор клуба</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(AssignmentCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию назначения
        /// </summary>
        /// <param name="id">идендификатор назначения</param>
        /// <param name="request">информация о назначении</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTask(AssignmentDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

        /// <summary>
        /// Удалить назначение
        /// </summary>
        /// <param name="id">идентификатор назначения</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteTask(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
    }
}
