﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class FieldController : BaseController<IFieldService>
    {
        public FieldController(IFieldService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка полей
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>PagedList<FieldDto></returns>
         [AllowAnonymous]
        [HttpGet("[action]/{pageNumber}:{pageSize}")]
        public async Task<IActionResult> GetPage(int pageNumber = 1, int pageSize = 6)
        {
            var pagedList = await _service.GetPage(pageNumber, pageSize);

            return Ok(new ResponseWrapper<List<FieldDto>>(pagedList.Entities, pagedList.Pagination));
        }

        /// <summary>
        /// Получение списка сущностей
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<EntityDto></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAll()
        {
            var list = await _service.GetList();
            return Ok(list);
        }

       
        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="request">информация о сущности</param>
        /// <returns>идентификатор сущности</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(FieldCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию о сущности
        /// </summary>
        /// <param name="id">идендификатор сущности</param>
        /// <param name="request">информация о сущности</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTask(FieldDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

         [HttpGet("[action]/GetByEntityId{id}")]
        public async Task<IActionResult> GetFieldByEntityId(Guid id)
        {
            var items =  await _service.GetFieldsByEntityId(id);
            return Ok(items);
        }

        [HttpGet("[action]/GetByProjectTypeId{id}")]
        public async Task<IActionResult> GetFieldByProjectTypeId(Guid id)
        {
            var items =  await _service.GetFieldsByProjectTypeId(id);
            return Ok(items);
        }

        [HttpGet("[action]/GetByProjectId{id}")]
        public async Task<IActionResult> GetFieldByProjectId(Guid id)
        {
            var items =  await _service.GetFieldsByProjectId(id);
            return Ok(items);
           
        }

        [HttpGet("[action]/GetByProjectId{id}/{text}")]
        public async Task<ActionResult<IEnumerable<FieldDto>>> SearchFieldsByText(Guid id, string text)
        {
           var items =  await _service.SearchFieldsByEntityId(id,text);
            return Ok(items);
        }

        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteProjectTypeField(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
    }
}
