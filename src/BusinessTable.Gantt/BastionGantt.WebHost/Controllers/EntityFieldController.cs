﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class EntityFieldController : BaseController<IEntityFieldService>
    {
        public EntityFieldController(IEntityFieldService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка типов и полей 
        /// </summary>
        /// <returns>List<AssignViewModel></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetEntityFields()
        {
            var list = await _service.GetList();
            return Ok(list);
        }

         /// <summary>
        /// Получение списка типов и полей
        /// </summary>
        /// <returns>AssignViewModel</returns>
        [HttpGet("[action]/{id}")]
        public async Task<IActionResult> GetEntityField(Guid id){
            var link = await _service.Get(id);
            return Ok(link);
        }

       
        /// <summary>
        /// Добавить тип проекта
        /// </summary>
        /// <param name="request">информация о типе проекта</param>
        /// <returns>идентификатор типа проекта</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(EntityFieldCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию типа проекта
        /// </summary>
        /// <param name="id">идендификатор типа проекта</param>
        /// <param name="request">информация о типе проекта</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateEntityField( EntityFieldDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

        /// <summary>
        /// Удалить EntityFields
        /// </summary>
        /// <param name="ids">идентификатор типа проекта</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteEntityFields(EntityFieldDto[] ids)
        {
            await _service.DeleteEntityFields(ids);
            return Ok();
        }

        [HttpPost("batch")]
        public async Task<IActionResult> CreateEntityFieldBatch(EntityFieldCreateDto[] entityFieldDto)
        {
            await _service.CreateEntityFieldBatch(entityFieldDto);
            return Ok();
        }

        [HttpPut("batchDelete")]
        public async Task<ActionResult> Delete(EntityFieldDto[] entityFieldDto)
        {
            await _service.DeleteEntityFields(entityFieldDto);
            return Ok();
        }
    }
}
