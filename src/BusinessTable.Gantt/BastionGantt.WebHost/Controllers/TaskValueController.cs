﻿using AutoMapper;
using BastionGantt.BusinessLogic.Abstractions;
using BastionGantt.BusinessLogic.Contracts;
using GMS.Core.WebHost.Controllers.Base;
using BastionGantt.WebHost.Models;
using BastionGantt.WebHost.Models.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json;
using BastionGantt.BusinessLogic.Models;

namespace BastionGantt.WebHost.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("_myAllowSpecificOrigins")]
    public class TaskValueController : BaseController<ITaskValueService>
    {
        public TaskValueController(ITaskValueService service, IMapper mapper) : base(service, mapper) { }

        /// <summary>
        /// Получение списка сущностей
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<InstanceDto></returns>
        [AllowAnonymous]
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAll()
        {
            var list = await _service.GetList();
            return Ok(list);
        }

       
        /// <summary>
        /// Добавить сущность
        /// </summary>
        /// <param name="request">информация о сущности</param>
        /// <returns>идентификатор сущности</returns>
       [HttpPost("[action]")]
        public async Task<IActionResult> Add(TaskValueCreateDto request)
        {
            var id = await _service.Create(request);
            return Ok(id.ToString());
        }
        /// <summary>
        /// Отредактировать информацию о сущности
        /// </summary>
        /// <param name="id">идендификатор сущности</param>
        /// <param name="request">информация о сущности</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<IActionResult> UpdateTask(TaskValueDto request)
        {
            await _service.Update(request);
            return Ok();            
        }

        [HttpGet("[action]/byTask/{id}")]
        public async Task<IActionResult> GetValueByTaskId(Guid id)
        {
            var items =  await _service.GetTaskValueByTask(id);
            return Ok(items);
        }       

        [HttpDelete("[action]/{id}")] 
        public async Task<IActionResult> DeleteValue(string id)
        {
            await _service.Remove(new Guid(id));
            return Ok();
        }
         [HttpPost("batch")]
        public async Task<IActionResult> CreateValueBatch(TaskValueCreateDto[] valueDto)
        {
            await _service.CreateValueBach(valueDto);
            return Ok();
        }

         /// <summary>
        /// Удалить значения инстанса
        /// </summary>
        /// <param name="id">идендификатор task</param>
        /// <returns></returns>

        [HttpDelete("batch/{id}")]
        public async Task<ActionResult> DeleteValues(Guid id)
        {
            await _service.RemoveBatch(id) ;
            return Ok();
        }
    }
}
