using MassTransit;
using SharedModel;
using System.Net;
using BastionGantt.BusinessLogic.Abstractions;
using Microsoft.Extensions.DependencyInjection;
using BastionGantt.BusinessLogic.Services;

namespace BastionGantt.WebHost.Job;
public class RabbitMqBackgroundTask : BackgroundService
{
    private readonly TimeSpan _period = TimeSpan.FromSeconds(5);
    private readonly ILogger<RabbitMqBackgroundTask> _logger;
    private readonly IServiceScopeFactory _factory;
    public RabbitMqBackgroundTask(ILogger<RabbitMqBackgroundTask> logger,IServiceScopeFactory factory)
    {
        _logger = logger;
        _factory = factory;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using PeriodicTimer timer = new PeriodicTimer(_period);

        while (!stoppingToken.IsCancellationRequested &&
               await timer.WaitForNextTickAsync(stoppingToken))
        {
            await using AsyncServiceScope asyncScope = _factory.CreateAsyncScope();
            IValueService valueService = asyncScope.ServiceProvider.GetRequiredService<IValueService>();
            List<DictContract> ls = await valueService.GetDictionaries();

            DictListContract contractModel = new DictListContract();
            contractModel.dictionary = ls;
            try{
            IPublishEndpoint publishEndpoint = asyncScope.ServiceProvider.GetRequiredService<IPublishEndpoint>();
            await publishEndpoint.Publish<DictListContract>(contractModel);
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
            }
            _logger.LogInformation("Executing RabbitMqBackgroundTask");
        }
    }
}