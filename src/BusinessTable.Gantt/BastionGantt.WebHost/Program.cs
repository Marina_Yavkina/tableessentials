using BastionGantt.Common.Extensions;
using BastionGantt.DataAccess.Context;
using BastionGantt.WebHost.Configurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using BastionGantt.WebHost.MiddleWareSettings;
using Microsoft.OpenApi.Models;
using BastionGantt.WebHost.Settings;
using MassTransit;
using SharedModel;
using BastionGantt.WebHost.Job;

try
{
    var builder = WebApplication.CreateBuilder(args);
    builder.Environment.ApplicationName = typeof(Program).Assembly.FullName;
// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("BastionCore");
builder.Services.AddDbContext<DatabaseContext>(options => options.UseLazyLoadingProxies()
                                                              .UseNpgsql(connectionString,b => b.MigrationsAssembly("BastionGantt.WebHost")),
                                                              ServiceLifetime.Scoped)
        
                                                             
        .AddRepositories()
        .AddOptions(builder.Configuration)
        .AddAutoMapper()
        .AddServices()
        .AddLogger(builder.Configuration)
        .AddEndpointsApiExplorer()
        .AddCustomJWTAuthentification()
        .AddAuthorizationGantt()
       // .AddSwagger()
        .AddControllers();
builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1", new OpenApiInfo { Title = "Demo API", Version = "v1" });
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
});
    builder.Services.AddMassTransit(x =>
    {
        x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(c =>
        {
            var rabbitMQSettings = builder.Configuration.GetSection(nameof(RabbitMQSettings)).Get<RabbitMQSettings>();
            c.Host(rabbitMQSettings.Host);
            c.ConfigureEndpoints(context);
        }));

        x.AddRequestClient<DictListContract>();
    });

    builder.Services.AddHostedService<RabbitMqBackgroundTask>();  
    var CorsAllowedOrigins = "_myAllowSpecificOrigins";

    builder.Services.AddCors(options =>
    {
        options.AddPolicy(CorsAllowedOrigins,
            builder =>
            {
                builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });
    });
   var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json","Gantt Application"));
    app.UseDeveloperExceptionPage();
    
}
    
    app.UseCors(CorsAllowedOrigins);
    //var app = WebApplicationConfiguration.Configure(builder);
    app.UseAuthentication();
    app.UseJwtTimeValidator();
    app.UseHttpsRedirection();
    app.UseAuthorization();
    app.MapControllers();

    Log.Logger.Information($"The {app.Environment.ApplicationName} started...");
    app.Run();

    return 0;
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly!");
    return 1;
}
finally
{
    Log.CloseAndFlush();
}