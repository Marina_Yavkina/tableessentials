﻿using AutoMapper;
using BastionGantt.Mappers;

namespace BastionGantt.WebHost.Configurations
{
    public static class AutoMapperConfiguration
    {
        public static IServiceCollection AddAutoMapper(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IMapper>(new Mapper(Configuration()));
            return serviceCollection;
        }

        private static MapperConfiguration Configuration()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            });

            configuration.AssertConfigurationIsValid();
            return configuration;
        }
    }
}
