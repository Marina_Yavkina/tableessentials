﻿using BastionGantt.DataAccess.Repositories;
using BastionGantt.Core.Abstractions.Repositories;

namespace BastionGantt.WebHost.Configurations
{
    public static class RepositoriesConfiguration
    {
        public static IServiceCollection AddRepositories(this IServiceCollection serviceCollection)
        {
            serviceCollection
                .AddTransient<ITaskRepository, TaskRepository>()
                .AddTransient<ILinkRepository, LinkRepository>()
                .AddTransient<IAssignmentRepository, AssignmentRepository>()
                .AddTransient<IEntityRepository, EntityRepository>()
                .AddTransient<IFieldRepository, FieldRepository>()
                .AddTransient<IEntityFieldRepository, EntityFieldRepository>()
                .AddTransient<IProjectTypeRepository, ProjectTypeRepository>()
                .AddTransient<IProjectTypeFieldRepository, ProjectTypeFieldRepository>()
                .AddTransient<IInstanceRepository, InstanceRepository>()
                .AddTransient<ITaskValueRepository, TaskValueRepository>()
                .AddTransient<IValueRepository, ValueRepository>();
            return serviceCollection;
        }
    }
}
