﻿using BastionGantt.WebHost.Middlewares;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace BastionGantt.WebHost.Configurations
{
    public static class WebApplicationConfiguration
    {
        public static WebApplication Configure(WebApplicationBuilder builder)
        {
            var app = builder.Build();
            app.UseMiddleware<LoggerMiddleware>();
            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseRouting();
            app.UseCors("_myAllowSpecificOrigins");
            app.UseAuthentication();
            app.UseAuthorization();
            //if (app.Environment.IsDevelopment())
            //{
                app.UseSwagger();
                app.UseSwaggerUI(options =>
                {
                  
                });
            //}
            //app.MapControllers();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            return app;
        }
    }
}
