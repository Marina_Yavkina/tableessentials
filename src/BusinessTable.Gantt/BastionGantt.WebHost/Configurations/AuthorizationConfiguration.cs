﻿using BastionGantt.Common;
using System.Security.Claims;

namespace BastionGantt.WebHost.Configurations
{
    public static class AuthorizationConfiguration
    {
        public static IServiceCollection AddAuthorizationGantt(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddAuthorization(options =>
            {
                options.AddPolicy("Administrator", builder =>
                {
                    builder.RequireAssertion(x => x.User.HasClaim(ClaimTypes.Role, nameof(Priviliges.System))
                                               || x.User.HasClaim(ClaimTypes.Role, nameof(Priviliges.Administrator)));
                });
             
                options.AddPolicy("User", builder =>
                {
                    builder.RequireAssertion(x => x.User.HasClaim(ClaimTypes.Role, nameof(Priviliges.User)));
                });
            });

            return serviceCollection;
        }
    }
}
