﻿using BastionGantt.WebHost.Configurations.Options;

namespace BastionGantt.WebHost.Configurations
{
    public static class OptionsConfiguration
    {
        public static IServiceCollection AddOptions(this IServiceCollection serviceCollection, ConfigurationManager configuration)
        {
            serviceCollection.AddOptions<HttpClientOptions>()
                .Bind(configuration.GetSection(HttpClientOptions.Position));

            return serviceCollection;
        }
    }
}
