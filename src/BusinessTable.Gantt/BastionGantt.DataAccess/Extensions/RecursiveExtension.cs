using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using BastionGantt.Core.Domain.Base;
using BastionGantt.Core.Abstractions.Repositories.Base;

namespace BastionGantt.DataAccess.Extensions
{
    public static class SelectRecursiveExtension
    {
        public static IEnumerable<T> SelectManyRecursive<T>(this IEnumerable<T> source, 
        Func<T, IEnumerable<T>> selector)
        {
            var result = source.SelectMany(selector);
            if (!result.Any())
            {
                    return result;
            }
            return result.Union(result.SelectManyRecursive(selector));
        }
           

         public static IEnumerable<T> GetRecursive<T>(this IEnumerable<T> source, Guid id ) where T : IParentEntity<Guid,Guid?>
        {
            return source.Where(x => x.ParentId == id || x.Id== id)
            .Union((source.Where(x => x.ParentId == id)
            .SelectMany(y => source.GetRecursive(y.Id))));  
                      
        }
        
    }
}


