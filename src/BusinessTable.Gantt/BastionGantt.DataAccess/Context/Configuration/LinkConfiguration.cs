﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BastionGantt.Core.Domain;

namespace BastionGantt.DataAccess.Context.Configurations
{
    public class LinkConfiguration : IEntityTypeConfiguration<Link>
    {
        public void Configure(EntityTypeBuilder<Link> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
            builder.Property(a => a.UpdateDate)
                     .IsRequired(false)
                     .HasColumnType("timestamp");
               builder.Property(a => a.CreateDate)
                     .IsRequired()
                     .HasColumnType("timestamp")
                     .HasDefaultValueSql("now()");
              builder.HasOne(a => a.SourceTask)
                    .WithMany()
                    .HasForeignKey(t => t.SourceTaskId)
                    .IsRequired();
              builder.HasOne(a => a.TargetTask)
                    .WithMany()
                    .HasForeignKey(t => t.TargetTaskId)
                    .IsRequired();
        }
    }
}
