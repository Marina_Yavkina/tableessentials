﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BastionGantt.Core.Domain;

namespace BastionGantt.DataAccess.Context.Configurations
{
    public class AssignmentConfiguration : IEntityTypeConfiguration<Assignment>
    {
        public void Configure(EntityTypeBuilder<Assignment> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.UserId)
                     .IsRequired()
                     .HasColumnType("character varying(40)")
                     .IsUnicode(false);
              builder.Property(a => a.Value)
                     .IsRequired()
                     .HasColumnType("character varying(200)")
                     .IsUnicode(false);
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.Property(a => a.StartDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.EndDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              
        }
    }
}
