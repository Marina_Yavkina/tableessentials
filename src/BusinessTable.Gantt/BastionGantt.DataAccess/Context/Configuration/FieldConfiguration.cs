﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BastionGantt.Core.Domain;

namespace BastionGantt.DataAccess.Context.Configurations
{
    public class FieldConfiguration : IEntityTypeConfiguration<Field>
    {
        public void Configure(EntityTypeBuilder<Field> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.Name)
                     .IsRequired()
                     .HasColumnType("character varying(256)")
                     .IsUnicode(false);
              builder.Property(a => a.Decsription)
                     .HasColumnType("character varying(1000)")
                     .IsUnicode(false);
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
               builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.Property(a => a.Type)
                     .HasColumnType("jsonb");
              builder.HasMany(f => f.EntityFields)
                     .WithOne(p => p.Field)
                     .HasForeignKey(a => a.FieldId)
                     .IsRequired();
              
        }
    }
}
