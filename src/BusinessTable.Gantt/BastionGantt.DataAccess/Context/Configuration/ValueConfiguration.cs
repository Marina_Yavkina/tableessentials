﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BastionGantt.Core.Domain;

namespace BastionGantt.DataAccess.Context.Configurations
{
    public class ValueConfiguration : IEntityTypeConfiguration<Value>
    {
        public void Configure(EntityTypeBuilder<Value> builder)
        {
            builder.Property(a => a.Id)
                  .HasColumnType("uuid")
                  .HasDefaultValueSql("uuid_generate_v4()")
                  .IsRequired();
            builder.Property(a => a.TextData)
                  .HasColumnType("character varying(1024)")
                  .IsUnicode(false);
            builder.Property(a => a.NumericData)
                  .HasColumnType("numeric(30, 6)")
                  .IsUnicode(false);
            builder.Property(a => a.IsDeleted)
                  .HasDefaultValue(false)
                  .IsRequired();
            builder.Property(a => a.CreateDate)
                  .HasColumnType("timestamp")
                  .IsRequired()
                  .HasDefaultValueSql("now()");
            builder.Property(a => a.UpdateDate)
                  .IsRequired(false)
                  .HasColumnType("timestamp");
            builder.HasOne(a => a.Field)
                  .WithMany()
                  .HasForeignKey(t => t.FieldId)
                  .IsRequired();
            builder.HasOne(a => a.Instance)
                  .WithMany()
                  .HasForeignKey(t => t.InstanceId)
                  .IsRequired();
            builder.HasOne(a => a.ValueInstance)
                  .WithMany()
                  .HasForeignKey(t => t.ValueInstanceId)
                  .IsRequired(false);
        }
    }
}
