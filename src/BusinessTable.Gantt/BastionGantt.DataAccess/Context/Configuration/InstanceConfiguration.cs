﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BastionGantt.Core.Domain;

namespace BastionGantt.DataAccess.Context.Configurations
{
    public class InstanceConfiguration : IEntityTypeConfiguration<Instance>
    {
        public void Configure(EntityTypeBuilder<Instance> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.Name)
                     .IsRequired()
                     .HasColumnType("character varying(256)")
                     .IsUnicode(false);
              builder.Property(a => a.Decsription)
                     .HasColumnType("character varying(1000)")
                     .IsUnicode(false);
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .IsRequired(false)
                     .HasColumnType("timestamp");
              builder.Property(a => a.CreateDate)
                     .IsRequired()
                     .HasColumnType("timestamp")
                     .HasDefaultValueSql("now()");
              builder.HasOne(a => a.Entity)
                    .WithMany()
                    .HasForeignKey(t => t.EntityId)
                    .IsRequired();
        }
    }
}
