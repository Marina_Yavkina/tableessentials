using A=BastionGantt.Core.Domain;
using BastionGantt.Core.Domain;
using BastionGantt.DataAccess.Context.Configurations;
using Microsoft.EntityFrameworkCore;

namespace BastionGantt.DataAccess.Context
{
    public class DatabaseContext : DbContext
    {
         public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
            //Database.EnsureCreated();
        }

        public DbSet<A.Task> Tasks { get; set; }
        public DbSet<Assignment> Assignments { get; set; }
        public DbSet<ProjectType> ProjectTypes { get; set; }
        public DbSet<ProjectTypeField> ProjectTypeFields { get; set; }
        public DbSet<Field> Fields { get; set; }
        public DbSet<Entity> Entities { get; set; }
        public DbSet<EntityField> EntityFields { get; set; }
        public DbSet<Link> Links { get; set; }
        public DbSet<Value> Values { get; set; }
        public DbSet<TaskValue> TaskValues { get; set; }
        public DbSet<Instance> Instances { get; set; }

       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasPostgresExtension("uuid-ossp");

            modelBuilder.ApplyConfiguration(new TaskConfiguration());
            modelBuilder.ApplyConfiguration(new AssignmentConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTypeConfiguration());
            modelBuilder.ApplyConfiguration(new EntityConfiguration());
            modelBuilder.ApplyConfiguration(new FieldConfiguration());
            modelBuilder.ApplyConfiguration(new EntityFieldConfiguration());
            modelBuilder.ApplyConfiguration(new ProjectTypeFieldConfiguration());
            modelBuilder.ApplyConfiguration(new LinkConfiguration());
            modelBuilder.ApplyConfiguration(new ValueConfiguration());
            modelBuilder.ApplyConfiguration(new TaskValueConfiguration());
            modelBuilder.ApplyConfiguration(new InstanceConfiguration());
        }
    }
}
