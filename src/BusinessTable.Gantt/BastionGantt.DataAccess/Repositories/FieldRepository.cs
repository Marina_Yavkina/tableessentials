﻿using BastionGantt.Core.Abstractions.Repositories;
using  BastionGantt.DataAccess.Repositories.Base;
using BastionGantt.Core.Domain;
using A=BastionGantt.Core.Domain;
using Microsoft.EntityFrameworkCore;
using BastionGantt.DataAccess.Context;
using BastionGantt.Core.Models;
using BastionGantt.DataAccess.Extensions;

namespace  BastionGantt.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class FieldRepository : Repository<Field, Guid>, IFieldRepository
    {
        public FieldRepository(DatabaseContext context) : base(context) { }

        public async Task<PagedList<Field>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Field>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        public IQueryable<Field> GetAllAsync( bool noTracking = false)
        {
            return GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
        }
        public async Task<PagedList<Field>> GetPagedAsync(string text, int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.Name.Contains(text) && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Field>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        
        

        
    }
}
