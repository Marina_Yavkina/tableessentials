using BastionGantt.Core.Abstractions.Repositories;
using  BastionGantt.DataAccess.Repositories.Base;
using BastionGantt.Core.Domain;
using A=BastionGantt.Core.Domain;
using Microsoft.EntityFrameworkCore;
using BastionGantt.DataAccess.Context;
using BastionGantt.Core.Models;
using BastionGantt.DataAccess.Extensions;

namespace BastionGantt.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class TaskValueRepository : Repository<TaskValue, Guid>, ITaskValueRepository
    {
        public TaskValueRepository(DatabaseContext context) : base(context) { }


        public async Task<PagedList<TaskValue>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<TaskValue>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        public IQueryable<TaskValue> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);
        }

        public async Task<PagedList<TaskValue>> GetPagedByTaskIdAsync(Guid taskId, int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.TaskId == taskId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<TaskValue>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        public async Task<List<TaskValue>> GetByTaskIdAsync(Guid taskId, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.TaskId == taskId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return await query.ToListAsync();
        }
//  public async Task<List<TaskValueFieldDto>?> GetValuesByTask(Guid id)
//         {
//              var values = await _context.TaskValues.Where(x => x.TaskId == id).ToListAsync();
//              var fieldsRes = await _context.Fields.ToListAsync();

//              if (values.Count != 0)
//              {
//                 List<TaskValueFieldDto> fields = fieldsRes.Join(values, c => c.Id, ct => ct.FieldId, (c,ct) => new TaskValueFieldDto{
//                 id = ct.Id,
//                 field_id = ct.FieldId,
//                 task_id = ct.TaskId,
//                 text_data = ct.TextData,
//                 numeric_data = ct.NumericData,
//                 bool_data = ct.BoolData,
//                 instance_id = ct.InstanceId,
//                 field = c.Name,
//                 value_id = ct.ValueId,
//                 type =  new TypeJsonDto { simple_type = c.Type.SimpleType, directory_id = c.Type.DirectoryId, instance_directory_id = c.Type.InstanceDirectoryId}
//                 }).ToList();
//                 return fields; 
//              }    
//             else    
//                 return null;    
//         }
//     }
    }
}
