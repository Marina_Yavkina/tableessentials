using BastionGantt.Core.Abstractions.Repositories;
using  BastionGantt.DataAccess.Repositories.Base;
using BastionGantt.Core.Domain;
using A=BastionGantt.Core.Domain;
using Microsoft.EntityFrameworkCore;
using BastionGantt.DataAccess.Context;
using BastionGantt.Core.Models;
using BastionGantt.DataAccess.Extensions;

namespace BastionGantt.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class AssignmentRepository : Repository<Assignment, Guid>, IAssignmentRepository
    {
        public AssignmentRepository(DatabaseContext context) : base(context) { }

        public async Task<PagedList<Assignment>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Assignment>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }

        public async Task<List<Assignment>> GetListAsync( bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
            var entities = await query.ToListAsync();
            return entities;
        }

        public async Task<List<Assignment>> GetPagedByTaskIdsAsync(Guid[] taskIds, bool noTracking = false){
             var query = GetAll(noTracking)
                .Where(p =>  taskIds.Contains(p.TaskId) && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

           return await query.ToListAsync();

        }
        public async Task<List<Assignment>> GetByTaskIdAsync(Guid taskId, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p =>  p.TaskId == taskId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

           return await query.ToListAsync();
        }
        public async Task<Assignment> GetById(Guid id, bool noTracking = false)
        {
            return await GetAll(noTracking)
                .Where(p => p.Id== id )
                .FirstAsync();
        }
    }
}
