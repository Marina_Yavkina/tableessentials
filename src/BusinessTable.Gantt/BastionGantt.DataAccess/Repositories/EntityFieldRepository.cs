using BastionGantt.Core.Abstractions.Repositories;
using  BastionGantt.DataAccess.Repositories.Base;
using BastionGantt.Core.Domain;
using A=BastionGantt.Core.Domain;
using Microsoft.EntityFrameworkCore;
using BastionGantt.DataAccess.Context;
using BastionGantt.Core.Models;
using BastionGantt.DataAccess.Extensions;

namespace  BastionGantt.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class EntityFieldRepository : Repository<EntityField, Guid>, IEntityFieldRepository
    {
        public EntityFieldRepository(DatabaseContext context) : base(context) { }

        public async Task<PagedList<EntityField>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<EntityField>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        
        public async Task<List<EntityField>> GetPagedByEntityIdAsync(Guid entityId, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p =>  p.EntityId == entityId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

           return await query.ToListAsync();
        }
        public IQueryable<EntityField> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);

        }
    }
}
