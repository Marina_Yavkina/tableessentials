using BastionGantt.Core.Abstractions.Repositories;
using  BastionGantt.DataAccess.Repositories.Base;
using BastionGantt.Core.Domain;
using A=BastionGantt.Core.Domain;
using Microsoft.EntityFrameworkCore;
using BastionGantt.DataAccess.Context;
using BastionGantt.Core.Models;
using BastionGantt.DataAccess.Extensions;

namespace BastionGantt.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class InstanceRepository : Repository<Instance, Guid>, IInstanceRepository
    {
        public InstanceRepository(DatabaseContext context) : base(context) { }

        public async Task<PagedList<Instance>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Instance>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        
        public async Task<PagedList<Instance>> GetPagedByEntityIdAsync(Guid entityId,int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p =>  p.EntityId == entityId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

           return new PagedList<Instance>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };

        }

        public async Task<Instance> GetById(Guid id, bool noTracking = false)
        {
            var query = await GetAll(noTracking)
                .Where(p =>  p.Id == id && p.IsDeleted == false)
                .FirstOrDefaultAsync();
            return query;
        }
        public IQueryable<Instance> GetAllAsync( bool noTracking = false)
        {
            return GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
        }
        
    }
}
