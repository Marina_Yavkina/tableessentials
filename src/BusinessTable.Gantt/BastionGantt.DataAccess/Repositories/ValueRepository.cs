using BastionGantt.Core.Abstractions.Repositories;
using  BastionGantt.DataAccess.Repositories.Base;
using BastionGantt.Core.Domain;
using A=BastionGantt.Core.Domain;
using Microsoft.EntityFrameworkCore;
using BastionGantt.DataAccess.Context;
using BastionGantt.Core.Models;
using BastionGantt.DataAccess.Extensions;

namespace  BastionGantt.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class ValueRepository : Repository<Value, Guid>, IValueRepository
    {
        public ValueRepository(DatabaseContext context) : base(context) { }

        public async Task<PagedList<Value>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Value>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        public async Task<PagedList<Value>> GetPagedByInstanceIdAsync(Guid instanceId, int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.InstanceId == instanceId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Value>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        public async Task<List<Value>> GetByInstanceIdAsync(Guid instanceId, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.InstanceId == instanceId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
            return await query.ToListAsync();
        }
         public IQueryable<Value> GetAllAsync( bool noTracking = false)
        {
            return GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
        }

    }
}
