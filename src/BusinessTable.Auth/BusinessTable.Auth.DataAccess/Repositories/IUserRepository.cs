using BastionGantt.Auth.Core.Domain;
using System;

namespace BastionGantt.Auth.DataAccess.Repositories
{
    public interface IUserRepository: IGenericRepository<ApplicationUser>
    {       

    }
}