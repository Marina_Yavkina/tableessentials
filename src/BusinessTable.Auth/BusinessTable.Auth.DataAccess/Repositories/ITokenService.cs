using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;


namespace  BastionGantt.Auth.DataAccess.Repositories
{
    public interface ITokenService
    {
        JwtSecurityToken GetToken(List<Claim> authClaims);
        string GenerateRefreshToken();
        ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    }
}