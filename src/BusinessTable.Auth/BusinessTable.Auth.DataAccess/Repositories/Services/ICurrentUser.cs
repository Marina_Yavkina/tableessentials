using  BastionGantt.Auth.Core.Model;

namespace BastionGantt.Auth.DataAccess.Repositories
{
    public interface ICurrentUser
    {
       UserModel? GetCurrentUserInfo();
    }
}