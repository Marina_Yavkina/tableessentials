namespace BastionGantt.Auth.DataAccess.Repositories.UOF
{
public interface IUnitOfWork : IDisposable
{
    IUserRepository Users { get; }
    Task CompleteAsync();
}
}