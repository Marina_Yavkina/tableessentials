using Microsoft.EntityFrameworkCore;
using BastionGantt.Auth.Core.Domain;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace BastionGantt.Auth.DataAccess.Data
{
 public class DataContext : IdentityDbContext<ApplicationUser,  ApplicationRole ,Guid>
    {
        public DataContext(DbContextOptions<DataContext> options)
           : base(options)
        {
            
        }
        public DbSet<ApplicationUser> ApplicationUsers { get; set;}
        public DbSet<ApplicationRole> ApplicationRoles { get; set;}
 
    }
    }