using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BastionGantt.Auth.Core.Model;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Web;
using BastionGantt.Auth.DataAccess.Repositories;

namespace BastionGantt.Auth.WebHost.Services
{
    public class CurrentUserData: ICurrentUser
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        public  CurrentUserData(IHttpContextAccessor httpContextAccessor)
        {
           
            _httpContextAccessor = httpContextAccessor;

        }
        public UserModel? GetCurrentUserInfo(){
            var Identity = _httpContextAccessor?.HttpContext?.User.Identity as ClaimsIdentity; 
            if (Identity != null)
            {
                var userClaims = Identity.Claims;
                return new UserModel{
                    Name = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Name)?.Value,
                    Email = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Email)?.Value,
                    Role = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Role)?.Value
                };
            }
            else
            {
               return null;     
            }
        }

         
    }
}