using Microsoft.AspNetCore.Builder;
 
namespace BastionGantt.Auth.WebHost.MiddleWareSettings
{
    public static class CustomMiddlewareExtensions
    {
        public static IApplicationBuilder UseJwtTimeValidator(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JwtTimeValidator>();
        }
    }
}