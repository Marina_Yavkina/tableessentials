using System;
using System.Collections.Generic;

namespace BastionGantt.Auth.WebHost.ViewModels
{
    public class UserViewModel
    {
        public Guid id { get; set; }
        public string text { get; set; }
        public string? parent { get; set; }
        public string? unit { get; set; }

        public string email { get; set; }

        public string role { get; set; }
    }
}