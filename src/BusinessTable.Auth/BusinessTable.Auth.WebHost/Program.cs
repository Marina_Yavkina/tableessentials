using BastionGantt.Auth.DataAccess.Data;
using Microsoft.EntityFrameworkCore;
using BastionGantt.Auth.Core.Domain;
using BastionGantt.Auth.DataAccess.Repositories.UOF;
using BastionGantt.Auth.DataAccess.Repositories;
using Microsoft.AspNetCore.Identity;
using BastionGantt.Auth.WebHost.Implementations.UOF;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using BastionGantt.Auth.WebHost.Implementations.UOF;
using BastionGantt.Auth.WebHost.Implementations;
using Microsoft.OpenApi.Models;
using BastionGantt.Auth.WebHost.MiddleWareSettings;
using BastionGantt.Auth.WebHost.Services;
using BastionGantt.Common.Extensions;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
builder.Services.AddDbContext<DataContext>(options => options.UseLazyLoadingProxies()
                                                              .UseNpgsql(connectionString,b => b.MigrationsAssembly("BastionGantt.Auth.WebHost")),
                                                              ServiceLifetime.Scoped);


builder.Services.AddIdentity<ApplicationUser, ApplicationRole>()
            .AddEntityFrameworkStores<DataContext>()
            .AddDefaultUI()
            .AddDefaultTokenProviders();
var s = builder.Configuration["Jwt:Secret"];

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

// builder.Services.AddAuthentication(options =>
// {
//     options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
//     options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
//     options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
// })

// // Adding Jwt Bearer
// .AddJwtBearer(options =>
// {
//     options.SaveToken = true;
//     options.RequireHttpsMetadata = false;
//     options.TokenValidationParameters = new TokenValidationParameters()
//     {
//         ValidateIssuer = true,
//         ValidateAudience = true,
//         ValidAudience = builder.Configuration["Jwt:Audience"],
//         ValidIssuer = builder.Configuration["Jwt:Issuer"],
//         IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["Jwt:Secret"]))
//     };
// });


builder.Services.AddControllers();
builder.Services.AddCors(options => {
    options.AddPolicy(name : "AllowOrigin",
    builder => {builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();});
});
builder.Services.AddAutoMapper(typeof(Program).Assembly);

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

#region Repositories
builder.Services.AddScoped(typeof(IGenericRepository<>), typeof(GenericRepository<>));
builder.Services. AddTransient<ICurrentUser,CurrentUserData>();
builder.Services.AddSingleton<ITokenService, TokenService>();
builder.Services.AddScoped<IUserRepository, UserRepository>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddCustomJWTAuthentification();

builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1", new OpenApiInfo { Title = "Demo API", Version = "v1" });
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
});
#endregion

var app = builder.Build();

app.UseCors("AllowOrigin");
// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json","Gantt Application"));
    app.UseDeveloperExceptionPage();
    
}
app.UseAuthentication();
app.UseJwtTimeValidator();
app.UseHttpsRedirection();
app.UseAuthorization();
app.MapControllers();
app.Run();
