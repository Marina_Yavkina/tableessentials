using AutoMapper;
using BastionGantt.Auth.WebHost.Dto;
using BastionGantt.Auth.Core.Domain;
using BastionGantt.Auth.WebHost.ViewModels;
using System.Globalization;

namespace gantt_backend.Mapping
{

public class AutoMapperProfile : Profile  
{  
   public AutoMapperProfile()  
   {  
      CultureInfo currentCulture = CultureInfo.CurrentCulture;
      

      CreateMap<ApplicationUser, UserViewModel>()
      .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id.ToString()))
      .ForMember(dest => dest.parent, opt => opt.MapFrom(src => src.ParentId.ToString()))
      .ForMember(dest => dest.text, opt => opt.MapFrom(src => src.UserName)) 
      .ForMember(dest => dest.email, opt => opt.MapFrom(src => src.Email)) 
      //.ForMember(dest => dest.role, opt => opt.MapFrom(src => src.Role)) 
      .ForMember(dest => dest.unit, opt => opt.Ignore());  

      CreateMap<UserViewModel, ApplicationUser>()
      .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
      .ForMember(dest => dest.ParentId, opt => opt.MapFrom(src => src.parent))
      .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.text));        
 
      CreateMap<UserForRegistrationDto, ApplicationUser>()
      .ForMember(u => u.FullName, opt => opt.MapFrom(x => x.LastName+" "+x.FirstName ))
      .ForMember(u => u.UserName, opt => opt.MapFrom(x => x.Email ))
      .ForMember(u => u.ParentId, opt => opt.MapFrom(x => x.ParentId ));
     
   }  
}
}