using BastionGantt.Auth.DataAccess.Repositories.UOF;
using BastionGantt.Auth.DataAccess.Repositories;
using BastionGantt.Auth.DataAccess.Data;
using AutoMapper;

namespace BastionGantt.Auth.WebHost.Implementations.UOF
{
public class UnitOfWork : IUnitOfWork
{
    private readonly DataContext _context;
    private readonly IMapper _mapper;
    public UnitOfWork (DataContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
        Users = new UserRepository(_context);

    }
    public IUserRepository Users { get; private set; }
   
     public async Task CompleteAsync()
        {
            await _context.SaveChangesAsync();
        }
    public void Dispose()
    {
        _context.Dispose();
    }
}
}