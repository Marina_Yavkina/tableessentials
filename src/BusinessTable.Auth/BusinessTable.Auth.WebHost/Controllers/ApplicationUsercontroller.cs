using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using BastionGantt.Auth.Core.Domain;
using BastionGantt.Auth.DataAccess.Repositories.UOF;
using BastionGantt.Auth.DataAccess.Repositories;
using BastionGantt.Auth.WebHost.Implementations.UOF;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using BastionGantt.Auth.WebHost.Implementations.UOF;
using BastionGantt.Auth.WebHost.Implementations;
using BastionGantt.Auth.WebHost.ViewModels;
using BastionGantt.Auth.WebHost.Dto;
using BastionGantt.Auth.WebHost.Model;

namespace BastionGantt.Auth.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [AllowAnonymous]
    public class ApplicationUserController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;
        private IMapper _mapper;
        private ITokenService _tokenService;
        private  RoleManager<ApplicationRole> _roleManager;

        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        public ApplicationUserController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,IUnitOfWork unitOfWork,
        IMapper mapper,RoleManager<ApplicationRole> roleManager,ITokenService tokenService)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _tokenService = tokenService;
            _roleManager = roleManager;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserViewModel>>> Get()
        {
            try {
            var users = await _unitOfWork.Users.GetAll();         
                if (users != null)
                {
                    var user = _mapper.Map<IEnumerable<UserViewModel>>(users);
                    return Ok(user);
                }
                }
                catch(Exception ex)
                {
                Console.Write(ex.Message);
                }    
            return NotFound();
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModelDto model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            var res = await _signInManager.PasswordSignInAsync(user, model.Password, false, lockoutOnFailure: false);
            var isVal = await _userManager.CheckPasswordAsync(user, model.Password);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var userRoles = await _userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, user.UserName),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }
                var token = _tokenService.GetToken(authClaims);
                var refreshToken = _tokenService.GenerateRefreshToken();
                user.RefreshToken = refreshToken;
                user.RefreshTokenExpiryTime = DateTime.Now.AddDays(365);
                _userManager.UpdateAsync(user);
                return Ok(new AuthenticatedResponse
                {
                    Token = new JwtSecurityTokenHandler().WriteToken(token),
                    RefreshToken = refreshToken
                });
            }
            return Unauthorized();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> RegisterUser( UserForRegistrationDto userForRegistration) 
        {
            if (userForRegistration == null || !ModelState.IsValid) 
                return BadRequest(); 
                
            var user = _mapper.Map<ApplicationUser>(userForRegistration);
            var result = await _userManager.CreateAsync(user, userForRegistration.Password); 
            if (!result.Succeeded) 
            { 
                var errors = result.Errors.Select(e => e.Description); 
                    
                return BadRequest(new RegistrationResponseDto { Errors = errors }); 
            }
                
            return StatusCode(200); 
        }
       
        [AllowAnonymous]
        [HttpPost("register_role")]
        public async Task<IActionResult> RegisterRole([FromBody] UserForRegistrationDto model)
        {
            var userExists = await _userManager.FindByEmailAsync(model.Email);
            if (userExists != null)
                return StatusCode(StatusCodes.Status500InternalServerError, new RegistrationResponseDto { IsSuccessfulRegistration = false, Errors = new List<string>().Append("User already exists!") });

            ApplicationUser user = new()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                FullName = model.LastName + " " + model.FirstName,
                UserName = model.LastName
            };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError,  new RegistrationResponseDto { IsSuccessfulRegistration = false, Errors = new List<string>().Append( "User creation failed! Please check user details and try again.") });

            try
            {
                foreach( var role in model.Role)
                {
                    if (!await _roleManager.RoleExistsAsync(role))
                    {
                        ApplicationRole applicationRole = new ApplicationRole  
                    {                     
                                        Name = role,
                                        Description = "Description"
                    };  
                        await _roleManager.CreateAsync(applicationRole);
                    }
                    if (await _roleManager.RoleExistsAsync(role))
                    {
                        await _userManager.AddToRoleAsync(user, role);
                    }            
                }
                return Ok(new RegistrationResponseDto { IsSuccessfulRegistration = true});
            }
            catch(Exception ex)
            {
                Console.Write(ex.Message);
                 return Ok(new RegistrationResponseDto { IsSuccessfulRegistration = true});
            }
        }
    }
}