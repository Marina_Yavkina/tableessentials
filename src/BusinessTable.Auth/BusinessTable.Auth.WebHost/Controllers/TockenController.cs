using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using BastionGantt.Auth.Core.Domain;
using BastionGantt.Auth.DataAccess.Repositories.UOF;
using BastionGantt.Auth.DataAccess.Repositories;
using BastionGantt.Auth.WebHost.Implementations.UOF;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authorization;
using BastionGantt.Auth.WebHost.Implementations.UOF;
using BastionGantt.Auth.WebHost.Implementations;
using BastionGantt.Auth.WebHost.ViewModels;
using BastionGantt.Auth.WebHost.Dto;
using BastionGantt.Auth.WebHost.Model;
using BastionGantt.Auth.Core.Model;

namespace BastionGantt.Auth.Controllers
{
    [ApiController]
    [Route("[controller]")]
   
    public class TokenController : ControllerBase
    {
        private ITokenService _tokenService;
        private ICurrentUser _currentUser;
        private  RoleManager<ApplicationRole> _roleManager;

        private UserManager<ApplicationUser> _userManager;
        private SignInManager<ApplicationUser> _signInManager;
        public TokenController(UserManager<ApplicationUser> userManager, ITokenService tokenService,ICurrentUser currentUser)
        {
            _userManager = userManager;
            _currentUser = currentUser;
            this._tokenService = tokenService ?? throw new ArgumentNullException(nameof(tokenService));
        }
        [HttpPost]
        [Route("refresh")]
        public async Task<IActionResult> Refresh(TokenApiModel tokenApiModel)
        {
            if (tokenApiModel is null)
                return BadRequest("Invalid client request");
            string accessToken = tokenApiModel.AccessToken;
            string refreshToken = tokenApiModel.RefreshToken;
            var principal = _tokenService.GetPrincipalFromExpiredToken(accessToken);
            var claims =  principal.Identities.First().Claims.ToList();
             var userEmail = claims?.FirstOrDefault(x => x.Type.Equals( ClaimValueTypes.Email, StringComparison.OrdinalIgnoreCase))?.Value;
            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user is null || user.RefreshToken != refreshToken || user.RefreshTokenExpiryTime <= DateTime.Now)
                return BadRequest("Invalid client request");
            var newAccessToken =  _tokenService.GetToken(principal.Claims.ToList());
            var newRefreshToken = _tokenService.GenerateRefreshToken();
            user.RefreshToken = newRefreshToken;
            _userManager.UpdateAsync(user);
            return Ok(new AuthenticatedResponse()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(newAccessToken),
                RefreshToken = newRefreshToken
            });
        }
        [HttpPost, Authorize]
        [Route("revoke")]
        public async Task<IActionResult> Revoke()
        { 
            var userEmail =_currentUser.GetCurrentUserInfo()!.Email;
            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user == null) return BadRequest();
            user.RefreshToken = null;
            _userManager.UpdateAsync(user);
            return NoContent();
        }
    }
}