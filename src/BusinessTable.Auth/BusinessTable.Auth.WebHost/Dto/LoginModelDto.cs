namespace BastionGantt.Auth.WebHost.Dto
{
  public class LoginModelDto
    {
        public string? Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

    }
}