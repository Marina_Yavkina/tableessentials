using System;

namespace BastionGantt.Auth.Core.Model{

    public class UserModel{
        public string? Name { get; set;}
        public string? Email { get; set;}
        public string? Role {get; set;}

    }
}