using Microsoft.AspNetCore.Identity;

namespace BastionGantt.Auth.Core.Domain
{
    public class ApplicationRole : IdentityRole<Guid>
    {
        public string Description { get; set; }
    }
}