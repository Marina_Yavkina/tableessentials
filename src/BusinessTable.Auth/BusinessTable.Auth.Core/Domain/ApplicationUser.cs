using Microsoft.AspNetCore.Identity;

namespace BastionGantt.Auth.Core.Domain
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FullName { get; set; }
        public virtual Guid? ParentId {get; set;}
        public string? RefreshToken { get; set; }
        public DateTime RefreshTokenExpiryTime { get; set; }
    }
}
