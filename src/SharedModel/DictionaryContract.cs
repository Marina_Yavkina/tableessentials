﻿using System.Collections.Generic;

namespace SharedModel
{
    public class DictListContract
    {
        public List<DictContract> dictionary {get;set;}
    }
    public class DictContract
    {
       public string entity { get; set; }
       public string field { get; set; }
       public TypeJsonDto? type { get; set; }
       public List<InstanceContract>? Instance { get; set; }
    }
    public class InstanceContract
    {
       public string entity { get; set; }
        public string instance { get; set; }
        public string field { get; set; }
       public TypeJsonDto? type { get; set; }
       public List<ValueContract>? values { get; set; }
    }
    public class ValueContract
    {
        public bool? bool_data { get; set; }
        public string entity { get; set; }
        public string instance { get; set; }
        public string field { get; set; }
        public int? num_data { get; set; }
        public string? text_data { get; set; }
        public TypeJsonDto? type { get; set; }
    }

    public class TypeJsonDto
    {
        public string simple_type { get; set; }
        public string directory_id { get; set; }
        public string instance_directory_id { get; set; }
        public string task_id { get;set; }
    }
}
