using BastionGantt.Common.Extensions;
using MMLib.SwaggerForOcelot.DependencyInjection;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddJsonFile("ocelot.json", false, false);


builder.Services.AddOcelot(builder.Configuration);
builder.Services.AddCustomJWTAuthentification();
builder.Services.AddEndpointsApiExplorer();



var app = builder.Build();

await app.UseOcelot();


app.UseAuthentication();
app.UseAuthorization();

app.Run();