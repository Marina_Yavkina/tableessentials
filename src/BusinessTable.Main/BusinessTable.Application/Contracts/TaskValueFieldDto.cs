using System;
using System.Collections.Generic;

namespace BusinessTable.Application.Contracts;
   public class TaskValueFieldDto
    {
       public Guid field_id {get;set;}
        public Guid? task_id {get;set;}
        public string? text_data {get;set;}
        public double? numeric_data {get;set;}
        public bool? bool_data {get;set;}
        public Guid? item_id {get;set;}
        public Guid? value_id {get;set;}
        public Guid? unit_field_id {get;set;}
        public string field {get;set;}
        public Guid? lookupEntityId { get; set; }
        public Guid? lookupEntityFieldId { get; set; }
        public bool? lookupMultipleSelection {get;set;}
       
    }