﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Models;

namespace BusinessTable.Application.Repositories
{
    public interface IProjectTypeRepository : IRepository<ProjectType,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору типа проекта 
        /// </summary>
        /// <param name="projectTypeId">идентификатор типа проекта</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список типов проектов</returns>
        Task<PagedList<ProjectType>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false);
        Task<ProjectType> GetById(Guid id, bool noTracking = false);
        Task<ProjectType> GetByName(string name, bool noTracking = false);
        IQueryable<ProjectType> GetAllAsync( bool noTracking = false);
    }
}
