﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Repositories
{
    public interface ISelectedDataDictionaryRepository : IRepository<SelectedDictionaryItem,Guid>
    {
        /// <summary>
        /// Получить список по идентификатору сущности 
        /// </summary>
        /// <param name="id">идентификатор сущности</param>
        /// <returns>список сущностей</returns>
        Task<List<SelectedDictionaryItem>> GetSelectedItemsByValueIdAsync(Guid valueId, bool noTracking = false);
        IQueryable<SelectedDictionaryItem> GetAllAsync(bool noTracking = false);
        

    }
}
