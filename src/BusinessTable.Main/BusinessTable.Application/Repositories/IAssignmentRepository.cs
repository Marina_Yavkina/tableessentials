﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Models;

namespace BusinessTable.Application.Repositories
{
    public interface IAssignmentRepository : IRepository<Assignment,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору поля 
        /// </summary>
        /// <param name="fieldId">идентификатор поля</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список полей</returns>
        Task<PagedList<Assignment>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<List<Assignment>> GetPagedByTaskIdsAsync(Guid[] taskIds, bool noTracking = false);
        Task<List<Assignment>> GetByTaskIdAsync(Guid taskId, bool noTracking = false);
        Task<Assignment> GetById(Guid id, bool noTracking = false);
        Task<List<Assignment>> GetListAsync(bool noTracking = true);
    }
}
