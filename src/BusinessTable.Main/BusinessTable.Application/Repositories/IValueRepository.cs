﻿using System.Linq.Expressions;
using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Repositories
{
    public interface IValueRepository : IRepository<Value,Guid>
    {
        /// <summary>
        /// Получить список по идентификатору сущности 
        /// </summary>
        /// <param name="id">идентификатор сущности</param>
        /// <returns>список сущностей</returns>
        Task<List<Value>> GetValuesByEntityfieldAndItemAsync(Guid entityFieldId, Guid itemId ,bool noTracking = false,params Expression<Func<Value, object>>[] includeProperties);
        IQueryable<Value> GetAllAsync(bool noTracking = false);
        

    }
}
