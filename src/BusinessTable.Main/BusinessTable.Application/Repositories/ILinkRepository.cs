﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Models;

namespace BusinessTable.Application.Repositories
{
    public interface ILinkRepository : IRepository<Link,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору поля 
        /// </summary>
        /// <param name="fieldId">идентификатор поля</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список полей</returns>
        Task<PagedList<Link>> GetPagedAsync(Guid[] taskIds,  bool noTracking = false);
        Task<List<Link>> GetPagedLinksAsync(Guid[] taskIds,  bool noTracking = false);
        Task<PagedList<Link>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
    }
}
