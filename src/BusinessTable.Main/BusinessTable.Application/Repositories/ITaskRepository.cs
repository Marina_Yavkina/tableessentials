﻿using BusinessTable.Application.Repositories.Base;
using A=BusinessTable.Domain.Entities;
using BusinessTable.Domain.Models;

namespace BusinessTable.Application.Repositories
{
    public interface ITaskRepository : IRepository<A.Task,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору поля 
        /// </summary>
        /// <param name="taskId">идентификатор задачи</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список полей</returns>
        Task<PagedList<A.Task>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<A.Task> GetById(Guid id, bool noTracking = false);
        Task<PagedList<A.Task>> GetTasksByPrpjectTypePagedAsync(Guid projectTypeId, int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<A.Task>> GetOnlyProjectsAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<A.Task>> GetTaskByProjectIdPagedAsync(Guid taskId, int pageNumber, int pageSize, bool noTracking = false);
        Task<List<A.Task>?> GetTaskByProjectIdAsync(Guid taskId,bool noTracking = false);
    }
}
