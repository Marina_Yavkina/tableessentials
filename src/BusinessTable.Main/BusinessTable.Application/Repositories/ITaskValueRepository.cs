﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Models;

namespace BusinessTable.Application.Repositories
{
    public interface ITaskValueRepository : IRepository<TaskValue,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору поля 
        /// </summary>
        /// <param name="taskValueId">идентификатор значений задач</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список значений задач</returns>
        IQueryable<TaskValue> GetAllAsync(bool noTracking = false);
        Task<PagedList<TaskValue>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<PagedList<TaskValue>> GetPagedByTaskIdAsync(Guid taskId, int pageNumber, int pageSize, bool noTracking = false);
        Task<List<TaskValue>> GetByTaskIdAsync(Guid taskId, bool noTracking = false);
    }
}
