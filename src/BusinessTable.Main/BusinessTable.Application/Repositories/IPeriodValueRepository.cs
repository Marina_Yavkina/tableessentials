﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Repositories
{
    public interface IPeriodValueRepository : IRepository<PeriodValue,Guid>
    {
        /// <summary>
        /// Получить список по идентификатору сущности 
        /// </summary>
        /// <param name="id">идентификатор сущности</param>
        /// <returns>список сущностей</returns>
        Task<List<PeriodValue>> GetPeriodValueByValueIdAsync(Guid valueId, bool noTracking = false);
        IQueryable<PeriodValue> GetAllAsync(bool noTracking = false);
        

    }
}
