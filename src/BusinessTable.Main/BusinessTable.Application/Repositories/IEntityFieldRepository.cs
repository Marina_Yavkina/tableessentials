﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Repositories
{
    public interface IUnitFieldRepository : IRepository<EntityField,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору сущность-поля
        /// </summary>
        /// <param name="entityFieldId">идентификатор сущность-поля</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список сущность-полей</returns>
       // Task<PagedList<EntityField>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false);
        Task<List<EntityField>> GetFieldsByEntityIdAsync(Guid entityId, bool noTracking = false);
       // IQueryable<EntityField> GetAllAsync(bool noTracking = false);
       IQueryable<EntityField> GetAllAsync( bool noTracking = false);
       
    }
}
