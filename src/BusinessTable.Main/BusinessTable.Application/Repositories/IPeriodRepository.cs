﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Repositories
{
    public interface IPeriodRepository : IRepository<Period,Guid>
    {
        /// <summary>
        /// Получить список по идентификатору сущности 
        /// </summary>
        /// <param name="id">идентификатор сущности</param>
        /// <returns>список сущностей</returns>
        Task<List<Period>> GetPeriodsAsync(bool noTracking = false);
        IQueryable<Period> GetAllAsync(bool noTracking = false);
        

    }
}
