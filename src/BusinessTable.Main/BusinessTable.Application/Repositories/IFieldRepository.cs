﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Repositories
{
    public interface IFieldRepository : IRepository<Field,Guid>
    {
        /// <summary>
        /// Получить постраничный список по идентификатору сущности 
        /// </summary>
        /// <param name="entityId">идентификатор сущности</param>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">объем страницы</param>
        /// <returns>список сущностей</returns>
        //Task<PagedList<Entity>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false);
        Task<List<Field>> GetPagedByTextAsync(string text, bool noTracking = false);
        IQueryable<Field> GetAllAsync( bool noTracking = false);
        

    }
}
