﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Repositories
{
    public interface IItemRepository : IRepository<Item,Guid>
    {
        /// <summary>
        /// Получить список по идентификатору сущности 
        /// </summary>
        /// <param name="id">идентификатор сущности</param>
        /// <returns>список сущностей</returns>
        Task<List<Item>> GetItemsByEntityIdAsync(Guid id ,bool noTracking = false);
        Task<List<Item>> GetItemsWithChildsByIdAsync(Guid id ,bool noTracking = false);
        IQueryable<Item> GetAllAsync(bool noTracking = false);
        

    }
}
