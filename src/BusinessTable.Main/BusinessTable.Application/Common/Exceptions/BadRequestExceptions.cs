namespace BusinessTable.Application.Common.Exceptions;
public class BadRequestException : Exception
{
    public BadRequestException(string errorMsg)
        : base(errorMsg) { }
}
