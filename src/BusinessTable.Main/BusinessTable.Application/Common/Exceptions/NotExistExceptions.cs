namespace BusinessTable.Application.Common.Exceptions;
public class NotExistException : Exception
{
    public NotExistException(string errorMsg)
        : base(errorMsg) { }
}
