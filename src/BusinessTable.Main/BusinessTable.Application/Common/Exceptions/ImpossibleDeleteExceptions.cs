namespace BusinessTable.Application.Common.Exceptions;
public class ImpossibleDeleteExceptions : Exception
{
    public ImpossibleDeleteExceptions(string errorMsg)
        : base(errorMsg) { }
}
