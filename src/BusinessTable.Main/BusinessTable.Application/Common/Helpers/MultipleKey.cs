using System;
using System.Collections.Generic;

namespace BusinessTable.Application.Common.Helpers;
public class MultipleKey
{ 
    public Guid? val_id {get;set;} 
    public Guid? id { get; set; }
};

public class TaskValueEF
{ 
   public Guid entity_field_id{get;set;} 
   public Guid item_id {get;set;}
   public Guid task_id {get;set;} 
   public Guid value_id {get;set;} 
   public Guid? field_id {get;set;} 
   public string text_data {get;set;} 
   public float? numeric_data {get;set;}
   public bool? bool_data{get; set;}
};