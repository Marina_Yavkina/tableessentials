using AutoMapper;
using BusinessTable.Domain.Entities;
using System.Globalization;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed class UpdatePeriodValueMapper : Profile
{
    CultureInfo currentCulture = CultureInfo.CurrentCulture;
    public UpdatePeriodValueMapper()
    {
        CreateMap<PeriodValueUpdateRequest,PeriodValue>()
        .ForMember(dest => dest.Value_dt, opt => opt.MapFrom(src => Convert.ToDateTime(src.value_dt,System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)))
        .ForMember(dest => dest.ValueId, opt => opt.MapFrom(src => src.value_id))
        .ForMember(dest => dest.PeriodId, opt => opt.MapFrom(src => src.period_id)); 
    }
}