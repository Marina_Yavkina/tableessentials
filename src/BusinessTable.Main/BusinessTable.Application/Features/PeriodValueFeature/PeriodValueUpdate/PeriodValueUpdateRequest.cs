using MediatR;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record  BatchUpdatePeriodValueRequest(List<PeriodValueUpdateRequest> batchUpdatePeriodValue) : IRequest<BatchUpdatePeriodValueResponse>;

public sealed record PeriodValueUpdateRequest( string id , string value_id,string period_id ,string value_dt ) : IRequest<PeriodValueUpdateResponse>;