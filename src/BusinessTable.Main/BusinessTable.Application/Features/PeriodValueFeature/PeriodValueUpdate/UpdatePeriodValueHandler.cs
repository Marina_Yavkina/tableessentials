using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed class PeriodValueUpdateHandler : IRequestHandler<PeriodValueUpdateRequest, PeriodValueUpdateResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IMapper _mapper;

    public PeriodValueUpdateHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<PeriodValueUpdateResponse> Handle(PeriodValueUpdateRequest request, CancellationToken cancellationToken)
    {
        try{
            var pv = _mapper.Map<PeriodValue>(request);
            _periodValueRepository.Update(pv);
            await _unitOfWork.Save(cancellationToken);
            return new PeriodValueUpdateResponse{isSuccessful = true};
        }
        catch
        {
            return new PeriodValueUpdateResponse{isSuccessful = false};
        }
    }
}