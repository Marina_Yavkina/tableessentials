using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed class BatchUpdatePeriodValueHandler : IRequestHandler<BatchUpdatePeriodValueRequest, BatchUpdatePeriodValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IMapper _mapper;

    public BatchUpdatePeriodValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchUpdatePeriodValueResponse> Handle(BatchUpdatePeriodValueRequest request, CancellationToken cancellationToken)
    {
        try {
        var pv = _mapper.Map<List<PeriodValue>>(request.batchUpdatePeriodValue);
        await _periodValueRepository.AddRange(pv);
        await _unitOfWork.Save(cancellationToken);
        }
        catch (Exception ex)
        {
         return new BatchUpdatePeriodValueResponse{ isSuccessful =false} ;
        }
        return new BatchUpdatePeriodValueResponse{ isSuccessful = true} ;
    }
}