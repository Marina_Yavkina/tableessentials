namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record BatchUpdatePeriodValueResponse
{
    public bool isSuccessful {get;set;}
}