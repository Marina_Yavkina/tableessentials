namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record PeriodValueUpdateResponse
{
    public bool isSuccessful {get;set;} 
}