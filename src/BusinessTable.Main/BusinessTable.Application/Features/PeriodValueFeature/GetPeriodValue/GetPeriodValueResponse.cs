namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record GetPeriodValueResponse
{
   public string periodId {get;set;}
   public string periodName {get;set;}
   public string value_dt {get;set;} 
   public string? value{get;set;}  
}