using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed class CreatePeriodValueHandler : IRequestHandler<PeriodValueCreateRequest, CreatePeriodValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IMapper _mapper;

    public CreatePeriodValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<CreatePeriodValueResponse> Handle(PeriodValueCreateRequest request, CancellationToken cancellationToken)
    {
        var pv = _mapper.Map<PeriodValue>(request);
        PeriodValue result = await _periodValueRepository.AddAsync(pv);
        await _unitOfWork.Save(cancellationToken);

        return _mapper.Map<CreatePeriodValueResponse>(result);
    }
}