using MediatR;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record  BatchCreatePeriodValueRequest(List<PeriodValueCreateRequest> batchCreatePeriodValue) : IRequest<BatchCreatePeriodValueResponse>;

public sealed record PeriodValueCreateRequest( string value_id,string period_id ,string value_dt ) : IRequest<CreatePeriodValueResponse>;

