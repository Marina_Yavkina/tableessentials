namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record CreatePeriodValueResponse
{
    public Guid id {get;set;} 
    public string value_id {get;set;}
    public string period_id {get;set;}
    public string value_dt {get;set;}
}