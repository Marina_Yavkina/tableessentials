using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed class BatchCreatePeriodValueHandler : IRequestHandler<BatchCreatePeriodValueRequest, BatchCreatePeriodValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IMapper _mapper;

    public BatchCreatePeriodValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchCreatePeriodValueResponse> Handle(BatchCreatePeriodValueRequest request, CancellationToken cancellationToken)
    {
        try {
        var pv = _mapper.Map<List<PeriodValue>>(request.batchCreatePeriodValue);
        await _periodValueRepository.AddRange(pv);
        await _unitOfWork.Save(cancellationToken);
        }
        catch (Exception ex)
        {
         throw new BadRequestException(ex.Message);
        }
        return new BatchCreatePeriodValueResponse{ isSuccessful = true} ;
    }
}