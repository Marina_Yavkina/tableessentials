namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record BatchCreatePeriodValueResponse
{
    public bool isSuccessful {get;set;}
}