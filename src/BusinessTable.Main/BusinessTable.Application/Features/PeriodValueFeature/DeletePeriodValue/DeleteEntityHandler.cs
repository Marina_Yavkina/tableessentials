using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed class DeletePeriodValueHandler : IRequestHandler<DeletePeriodValueRequest, DeletePeriodValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IMapper _mapper;

    public DeletePeriodValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<DeletePeriodValueResponse> Handle(DeletePeriodValueRequest request, CancellationToken cancellationToken)
    {
        try
        {
            _periodValueRepository.Delete(request.id);
            await _unitOfWork.Save(cancellationToken);
            return new DeletePeriodValueResponse{isSuccessful = true};
        }
         catch(Exception ex)
        {
           throw new BadRequestException(ex.Message);
        }

    }
}