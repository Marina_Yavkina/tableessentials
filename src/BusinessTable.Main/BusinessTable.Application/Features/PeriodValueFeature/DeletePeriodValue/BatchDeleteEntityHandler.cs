using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed class BatchDeletedPeriodValueHandler : IRequestHandler<BatchDeletePeriodValueRequest, DeletePeriodValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IMapper _mapper;

    public BatchDeletedPeriodValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<DeletePeriodValueResponse> Handle(BatchDeletePeriodValueRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entities = await _periodValueRepository.GetListWithInclude(p => request.ids.Contains(p.Id));
            await _periodValueRepository.RemoveRange(entities);
            await _unitOfWork.Save(cancellationToken);
            return new DeletePeriodValueResponse{ isSuccessful = true} ;
        }
        catch (Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }

        
    }
}