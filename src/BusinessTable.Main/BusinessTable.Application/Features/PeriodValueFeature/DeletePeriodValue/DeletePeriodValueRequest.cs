using MediatR;

namespace BusinessTable.Application.Features.PeriodValueFeature;

public sealed record  BatchDeletePeriodValueRequest(List<Guid> ids) : IRequest<DeletePeriodValueResponse>;

public sealed record DeletePeriodValueRequest( Guid id) : IRequest<DeletePeriodValueResponse>;