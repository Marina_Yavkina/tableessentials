namespace BusinessTable.Application.Features.UnitFeature;

public sealed record BatchCreateUnitResponse
{
    public bool isSuccessful {get;set;}
}