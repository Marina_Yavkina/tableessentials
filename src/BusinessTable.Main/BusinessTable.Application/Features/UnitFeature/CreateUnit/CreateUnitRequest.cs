using MediatR;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed record  BatchCreateUnitRequest(List<CreateUnitRequest> batchCreateUnit) : IRequest<BatchCreateUnitResponse>;

public sealed record CreateUnitRequest( string displayName, string definiton,bool isTaskConnnected) : IRequest<CreateUnitResponse>;