using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class CreateUnitMapper : Profile
{
    public CreateUnitMapper()
    {
        CreateMap<Unit, CreateUnitResponse>()
        .ForMember(dest => dest.displayName, opt => opt.MapFrom(src => src.DisplayName))
        .ForMember(dest => dest.definiton, opt => opt.MapFrom(src => src.Definiton))
        .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
        .ForMember(dest => dest.isTaskConnnected, opt => opt.MapFrom(src => src.isTaskConnnected)); 
        CreateMap<CreateUnitRequest,Unit>()
        .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.displayName))
        .ForMember(dest => dest.Definiton, opt => opt.MapFrom(src => src.definiton))
        .ForMember(dest => dest.isTaskConnnected, opt => opt.MapFrom(src => src.isTaskConnnected))
        .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
        .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
        .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
        .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
        .ForMember(dest => dest.EntityFields, opt => opt.Ignore())
        .ForMember(dest => dest.Items, opt => opt.Ignore())
        .ForMember(dest => dest.Views, opt => opt.Ignore());
    }
}