using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using U=BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class BatchCreateUnitHandler : IRequestHandler<BatchCreateUnitRequest, BatchCreateUnitResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitRepository _unitRepository;
    private readonly IMapper _mapper;

    public BatchCreateUnitHandler(IUnitOfWork unitOfWork, IUnitRepository entityRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitRepository = entityRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchCreateUnitResponse> Handle(BatchCreateUnitRequest request, CancellationToken cancellationToken)
    {
        try {
        var entity = _mapper.Map<List<U.Unit>>(request.batchCreateUnit);
        await _unitRepository.AddRange(entity);
        await _unitOfWork.Save(cancellationToken);
        }
        catch (Exception ex)
        {
         throw new BadRequestException(ex.Message);
        }

        return new BatchCreateUnitResponse{ isSuccessful = true} ;
    }
}