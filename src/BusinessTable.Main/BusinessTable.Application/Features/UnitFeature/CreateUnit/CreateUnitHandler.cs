using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using U=BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class CreateEntityHandler : IRequestHandler<CreateUnitRequest, CreateUnitResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitRepository _unitRepository;
    private readonly IMapper _mapper;

    public CreateEntityHandler(IUnitOfWork unitOfWork, IUnitRepository unitRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitRepository = unitRepository;
        _mapper = mapper;
    }
    
    public async Task<CreateUnitResponse> Handle(CreateUnitRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var entity = _mapper.Map<U.Unit>(request);
            await _unitRepository.AddAsync(entity);
            await _unitOfWork.Save(cancellationToken);
            return _mapper.Map<CreateUnitResponse>(entity);
        }
        catch( Exception ex )
        {
            throw new BadRequestException(ex.Message);
        }

        
    }
}