using MediatR;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed record  BatchDeleteUnitRequest(List<Guid> ids) : IRequest<DeleteUnitResponse>;

public sealed record DeleteUnitRequest( Guid id) : IRequest<DeleteUnitResponse>;