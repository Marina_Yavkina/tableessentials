using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class BatchDeleteUnitHandler : IRequestHandler<BatchDeleteUnitRequest, DeleteUnitResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitRepository _unitRepository;
    private readonly IMapper _mapper;

    public BatchDeleteUnitHandler(IUnitOfWork unitOfWork, IUnitRepository unitRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitRepository = unitRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteUnitResponse> Handle(BatchDeleteUnitRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entities = await _unitRepository.GetListWithInclude(p => request.ids.Contains(p.Id));
            await _unitRepository.RemoveRange(entities);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteUnitResponse{ isSuccessful = true} ;
        }
        catch (Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }

        
    }
}