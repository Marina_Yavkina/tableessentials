using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class DeleteEntityHandler : IRequestHandler<DeleteUnitRequest, DeleteUnitResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitRepository _unitRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public DeleteEntityHandler(IUnitOfWork unitOfWork, IUnitRepository unitRepository,IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitRepository = unitRepository;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteUnitResponse> Handle(DeleteUnitRequest request, CancellationToken cancellationToken)
    {
        var uf = await _unitFieldRepository.GetFieldsByEntityIdAsync(request.id);
        if (uf.Count() != 0)
            throw  new ImpossibleDeleteExceptions("Невозможно удалить сущность, т.к. к данной сущность привязаны поля!");
        
        var u = await _unitRepository.GetAsync(request.id,true);
        if (u == null)
            throw  new NotExistException("Сущность с данным идентификатором не существует!");
            _unitRepository.Delete(request.id);
        await _unitOfWork.Save(cancellationToken);
        return new DeleteUnitResponse{isSuccessful = true};
    }
}