using AutoMapper;
using BusinessTable.Application.Common.Exceptions;
using BusinessTable.Application.Repositories;
using MediatR;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class GetAllUnitHandler : IRequestHandler<GetAllUnitRequest, List<GetAllUnitResponse>>
{
    private readonly IUnitRepository _unitRepository;
    private readonly IMapper _mapper;

    public GetAllUnitHandler(IUnitRepository unitRepository, IMapper mapper)
    {
        _unitRepository = unitRepository;
        _mapper = mapper;
    }
    
    public async Task<List<GetAllUnitResponse>?> Handle(GetAllUnitRequest request, CancellationToken cancellationToken)
    {
        var entities = await _unitRepository.GetAll(cancellationToken);
        try
        {
            return _mapper.Map<List<GetAllUnitResponse>>(entities);
        }
        catch(Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }
    }
}