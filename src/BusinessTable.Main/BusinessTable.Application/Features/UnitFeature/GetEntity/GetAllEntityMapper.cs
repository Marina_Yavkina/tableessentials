using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class GetAllUnitMapper : Profile
{
    public GetAllUnitMapper()
    {
        CreateMap<Unit, GetAllUnitResponse>()
        .ForMember(dest => dest.displayName, opt => opt.MapFrom(src => src.DisplayName))
        .ForMember(dest => dest.definiton, opt => opt.MapFrom(src => src.Definiton))
        .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
        .ForMember(dest => dest.isTaskConnnected, opt => opt.MapFrom(src => src.isTaskConnnected)); 
    }
}