using MediatR;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed record GetAllUnitRequest : IRequest<List<GetAllUnitResponse>>;