namespace BusinessTable.Application.Features.UnitFeature;

public sealed record GetAllUnitResponse
{
    public Guid id {get;set;} 
    public string displayName { get; set; }
    public string definiton {get;set;}
    public bool isTaskConnnected {get;set;}
}