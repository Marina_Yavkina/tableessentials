namespace BusinessTable.Application.Features.UnitFeature;

public sealed record UpdateUnitResponse
{
    public bool isSuccessful {get;set;}
}