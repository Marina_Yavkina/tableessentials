using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class UpdateUnitMapper : Profile
{
    public UpdateUnitMapper()
    {
        CreateMap<UpdateUnitRequest,Unit>()
        .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.displayName))
        .ForMember(dest => dest.Definiton, opt => opt.MapFrom(src => src.definiton))
        .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
        .ForMember(dest => dest.isTaskConnnected, opt => opt.MapFrom(src => src.isTaskConnnected)); 
    }
}