using MediatR;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed record UpdateUnitRequest( Guid id, string displayName, string definiton,bool isTaskConnnected) : IRequest<UpdateUnitResponse>;