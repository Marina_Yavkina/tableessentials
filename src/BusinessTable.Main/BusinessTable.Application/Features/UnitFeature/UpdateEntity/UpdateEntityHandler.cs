using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using U=BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.UnitFeature;

public sealed class UpdateUnitHandler : IRequestHandler<UpdateUnitRequest, UpdateUnitResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitRepository _unitRepository;
    private readonly IMapper _mapper;

    public UpdateUnitHandler(IUnitOfWork unitOfWork, IUnitRepository unitRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitRepository = unitRepository;
        _mapper = mapper;
    }
    
    public async Task<UpdateUnitResponse> Handle(UpdateUnitRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entity = _mapper.Map<U.Unit>(request);
            _unitRepository.Update(entity);
            await _unitOfWork.Save(cancellationToken);
            return new UpdateUnitResponse{isSuccessful = true};
        }
        catch(Exception ex)
        {
           throw new BadRequestException(ex.Message);
        }
    }
}