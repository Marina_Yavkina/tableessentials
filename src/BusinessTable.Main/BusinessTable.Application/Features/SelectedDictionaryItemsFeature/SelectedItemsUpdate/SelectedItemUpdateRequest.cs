namespace BusinessTable.Application.Features.SelectedItemFeature;

public sealed record SelectedItemUpdateRequest
{
   public string id {get;set;}
   public string value_id {get;set;}
   public string selectedItem_id {get;set;}
}