namespace BusinessTable.Application.Features.SelectedItemFeature;

public sealed record GetSelectedItemResponse
{
   public string id {get;set;} 
   public string selectedItemId {get;set;}
}