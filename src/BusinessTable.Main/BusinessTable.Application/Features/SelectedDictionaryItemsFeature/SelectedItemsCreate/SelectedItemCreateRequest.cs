namespace BusinessTable.Application.Features.SelectedItemFeature;

public sealed record SelectedItemCreateRequest
{
   public string value_id {get;set;}
   public string selectedItem_id {get;set;}
}