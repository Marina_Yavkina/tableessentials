using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.SelectedItemFeature;

public sealed class CreateSelectedItemMapper : Profile
{
    public CreateSelectedItemMapper()
    {
        CreateMap<SelectedItemCreateRequest,SelectedDictionaryItem>()
        .ForMember(dest => dest.ValueId, opt => opt.MapFrom(src =>  Guid.Parse(src.value_id)))
        .ForMember(dest => dest.SelectedItemId, opt => opt.MapFrom(src =>  Guid.Parse(src.selectedItem_id)))
        .ForMember(dest => dest.IsDeleted, opt => opt.MapFrom(src => src.value_id))
        .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
        .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
        .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
        .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
        .ForMember(dest => dest.Value, opt => opt.Ignore());
        
    }
}