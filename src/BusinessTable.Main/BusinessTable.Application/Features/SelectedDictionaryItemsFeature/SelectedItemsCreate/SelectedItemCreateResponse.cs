namespace BusinessTable.Application.Features.SelectedItemFeature;

public sealed record SelectedItemCreateResponse
{
   public string id {get;set;}
   public string value_id {get;set;}
   public string selectedItem_id {get;set;}
}