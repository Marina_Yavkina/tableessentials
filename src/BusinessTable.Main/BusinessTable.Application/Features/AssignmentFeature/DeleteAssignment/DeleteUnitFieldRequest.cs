using MediatR;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed record  BatchDeleteAssignmentRequest(List<Guid> ids) : IRequest<DeleteAssignmentResponse>;

public sealed record DeleteAssignmentRequest( Guid id) : IRequest<DeleteAssignmentResponse>;