using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class BatchDeleteAssignmentHandler : IRequestHandler<BatchDeleteAssignmentRequest, DeleteAssignmentResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public BatchDeleteAssignmentHandler(IUnitOfWork unitOfWork, IAssignmentRepository assignmentRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteAssignmentResponse> Handle(BatchDeleteAssignmentRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entities = await _assignmentRepository.GetListWithInclude(p => request.ids.Contains(p.Id));
            await _assignmentRepository.RemoveRange(entities);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteAssignmentResponse{ isSuccessful = true} ;
        }
        catch (Exception ex)
        {
            return new DeleteAssignmentResponse{ isSuccessful =false} ;
        }

        
    }
}