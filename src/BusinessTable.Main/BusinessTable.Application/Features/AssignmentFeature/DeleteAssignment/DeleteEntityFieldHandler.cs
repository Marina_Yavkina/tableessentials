using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class DeleteAssignmentHandler : IRequestHandler<DeleteAssignmentRequest, DeleteAssignmentResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public DeleteAssignmentHandler(IUnitOfWork unitOfWork, IAssignmentRepository assignmentRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteAssignmentResponse> Handle(DeleteAssignmentRequest request, CancellationToken cancellationToken)
    {
        try
        {
            _assignmentRepository .Delete(request.id);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteAssignmentResponse{isSuccessful = true};
        }
         catch
        {
            return new DeleteAssignmentResponse{isSuccessful = false};
        }

    }
}