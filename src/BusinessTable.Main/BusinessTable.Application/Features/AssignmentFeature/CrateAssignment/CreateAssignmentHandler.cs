using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class CreateAssignmentHandler : IRequestHandler<CreateAssignmentRequest, CreateAssignmentResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public CreateAssignmentHandler(IUnitOfWork unitOfWork, IAssignmentRepository assignmentRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<CreateAssignmentResponse> Handle(CreateAssignmentRequest request, CancellationToken cancellationToken)
    {
        var assignment = _mapper.Map<Assignment>(request);
        var resp =  await _assignmentRepository.AddAsync(assignment);
        await _unitOfWork.Save(cancellationToken);
        return  new CreateAssignmentResponse{ id = resp.Id};
    }
}