namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed record CreateAssignmentResponse
{
    public Guid id {get;set;} 
}