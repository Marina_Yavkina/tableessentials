using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class CreateAssignmentMapper : Profile
{
    public CreateAssignmentMapper()
    {
        CreateMap<CreateAssignmentRequest, Assignment>()
        .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.duration))
        .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => Convert.ToDateTime(src.start_date, 	System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)))//DateTime.Parse(src.start_date, Thread.CurrentThread.CurrentCulture)))
        .ForMember(dest => dest.EndDate, opt => opt.Ignore())
        .ForMember(dest => dest.Delay, opt => opt.MapFrom(src => src.delay))
        .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.resource_id))
        .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.value))
        .ForMember(dest => dest.Mode, opt => opt.MapFrom(src => src.mode))
        .ForMember(dest => dest.Id, opt => opt.Ignore())
        .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
        .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
        .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
        .ForMember(dest => dest.Task, opt => opt.Ignore())
        .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
        .ForMember(dest => dest.TaskId, opt => opt.Ignore())
        .ForMember(dest => dest.Unit, opt => opt.Ignore()); 
    }
}