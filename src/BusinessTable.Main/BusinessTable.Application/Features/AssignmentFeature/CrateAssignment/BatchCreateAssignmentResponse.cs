namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed record BatchCreateAssignmentResponse
{
    public bool isSuccessful {get;set;}
}