using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class BatchCreateAssignmentHandler : IRequestHandler<BatchCreateAssignmentRequest, BatchCreateAssignmentResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public BatchCreateAssignmentHandler(IUnitOfWork unitOfWork, IAssignmentRepository assignmentRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchCreateAssignmentResponse> Handle(BatchCreateAssignmentRequest request, CancellationToken cancellationToken)
    {
        try {
        var assignment= _mapper.Map<List<Assignment>>(request.batchAssignment);
        await _assignmentRepository.AddRange(assignment);
        await _unitOfWork.Save(cancellationToken);
        }
        catch (Exception ex)
        {
         return new BatchCreateAssignmentResponse{ isSuccessful =false} ;
        }

        return new BatchCreateAssignmentResponse{ isSuccessful = true} ;
    }
}