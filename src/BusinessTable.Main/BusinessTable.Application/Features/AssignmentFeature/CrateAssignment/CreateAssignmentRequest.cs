using MediatR;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed record  BatchCreateAssignmentRequest(List<CreateAssignmentRequest> batchAssignment) : IRequest<BatchCreateAssignmentResponse>;

public sealed record CreateAssignmentRequest( 
        string start_date ,int? duration ,
        string? end_date,int? delay ,string resource_id ,
        string? mode,string value) : IRequest<CreateAssignmentResponse>;