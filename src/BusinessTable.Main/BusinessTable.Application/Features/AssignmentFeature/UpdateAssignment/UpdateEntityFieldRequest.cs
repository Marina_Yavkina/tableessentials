using MediatR;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed record UpdateAssignmentRequest( string id,string start_date, int? duration , string? end_date,
        int? delay, string resource_id, string? task_id , string? mode , string value ) : IRequest<UpdateAssignmentResponse>;