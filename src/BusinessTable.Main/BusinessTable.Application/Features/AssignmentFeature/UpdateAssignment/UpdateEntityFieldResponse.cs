namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed record UpdateAssignmentResponse
{
    public bool isSuccessful {get;set;}
}