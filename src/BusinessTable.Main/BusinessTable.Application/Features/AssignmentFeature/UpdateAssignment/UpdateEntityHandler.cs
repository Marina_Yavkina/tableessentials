using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class UpdateAssignmentHandler : IRequestHandler<UpdateAssignmentRequest, UpdateAssignmentResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public UpdateAssignmentHandler(IUnitOfWork unitOfWork, IAssignmentRepository assignmentRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<UpdateAssignmentResponse> Handle(UpdateAssignmentRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var assignment = _mapper.Map<Assignment>(request);
            _assignmentRepository.Update(assignment);
            await _unitOfWork.Save(cancellationToken);
            return new UpdateAssignmentResponse{isSuccessful = true};
        }
        catch
        {
            return new UpdateAssignmentResponse{isSuccessful = false};
        }
    }
}