using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class GetAssignmentMapper : Profile
{
    public GetAssignmentMapper()
    {
         CreateMap<Assignment, GetAssignmentResponse>()
      .ForMember(dest => dest.duration, opt => opt.MapFrom(src => src.Duration))
      .ForMember(dest => dest.start_date, opt => opt.MapFrom(src => src.StartDate.HasValue ? src.StartDate.Value.ToString("dd-MM-yyyy HH:mm") : null))
      .ForMember(dest => dest.end_date, opt => opt.MapFrom(src => src.EndDate.HasValue ? src.EndDate.Value.ToString("dd-MM-yyyy HH:mm") : null))
      .ForMember(dest => dest.delay, opt => opt.MapFrom(src => src.Delay))
      .ForMember(dest => dest.duration, opt => opt.MapFrom(src => src.Duration))
      .ForMember(dest => dest.resource_id, opt => opt.MapFrom(src => src.UserId))
      .ForMember(dest => dest.value, opt => opt.MapFrom(src => src.Value))
      .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
      .ForMember(dest => dest.task_id, opt => opt.MapFrom(src => src.TaskId))
      .ForMember(dest => dest.mode, opt => opt.MapFrom(src => src.Mode));  
    }
}