using MediatR;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed record GetAssignmentRequest(Guid project_id) : IRequest<List<GetAssignmentResponse>>;