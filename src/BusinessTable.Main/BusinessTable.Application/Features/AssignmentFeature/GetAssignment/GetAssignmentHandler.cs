using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.AssignmentFeature;

public sealed class GetAssignmentHandler : IRequestHandler<GetAssignmentRequest, List<GetAssignmentResponse>>
{
    private readonly IUnitRepository _unitRepository;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly ITaskRepository _taskRepository;
    private readonly IMapper _mapper;

    public GetAssignmentHandler(IAssignmentRepository assignmentRepository,ITaskRepository taskRepository, IMapper mapper)
    {
        _assignmentRepository = assignmentRepository;
        _taskRepository = taskRepository;
        _mapper = mapper;
    }
    
    public async Task<List<GetAssignmentResponse>> Handle(GetAssignmentRequest request, CancellationToken cancellationToken)
    {
        var tasks = await _taskRepository.GetTaskByProjectIdAsync(request.project_id);
        var ids = tasks.Select(c => c.Id ).ToArray();
        var list = await _assignmentRepository.GetPagedByTaskIdsAsync(ids);
            return _mapper.Map<List<Assignment>, List<GetAssignmentResponse>>(list);
    }
}