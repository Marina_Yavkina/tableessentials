using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class BatchDeleteItemHandler : IRequestHandler<BatchDeleteItemRequest, DeleteItemResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IItemRepository _itemRepository;
    private readonly IMapper _mapper;

    public BatchDeleteItemHandler(IUnitOfWork unitOfWork, IItemRepository itemRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _itemRepository = itemRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteItemResponse> Handle(BatchDeleteItemRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entities = await _itemRepository.GetListWithInclude(p => request.ids.Contains(p.Id));
            await _itemRepository.RemoveRange(entities);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteItemResponse{ isSuccessful = true} ;
        }
        catch (Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }

        
    }
}