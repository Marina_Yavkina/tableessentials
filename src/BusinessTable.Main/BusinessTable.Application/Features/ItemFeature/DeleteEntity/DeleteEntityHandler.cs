using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class DeleteItemHandler : IRequestHandler<DeleteItemRequest, DeleteItemResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IItemRepository _itemRepository;
    private readonly IValueRepository _valueRepository;
    private readonly IMapper _mapper;

    public DeleteItemHandler(IUnitOfWork unitOfWork, IItemRepository itemRepository, IValueRepository valueRepository,IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _itemRepository = itemRepository;
        _valueRepository = valueRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteItemResponse> Handle(DeleteItemRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var uf =  _valueRepository.GetAllAsync().Where(c => c.ItemId == request.id).ToList();
            if (uf.Count() != 0)
                throw  new ImpossibleDeleteExceptions("Невозможно удалить item, т.к. к данный item привязан к значению!");
            var u = await _itemRepository.GetAsync(request.id,true);
            if (u == null)
                throw  new NotExistException("Item с данным идентификатором не существует!");
            _itemRepository.Delete(request.id);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteItemResponse{isSuccessful = true};
        }
         catch( Exception ex )
        {
            throw new BadRequestException(ex.Message);
        }

    }
}