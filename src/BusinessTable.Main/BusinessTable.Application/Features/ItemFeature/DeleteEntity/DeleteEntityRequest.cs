using MediatR;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed record  BatchDeleteItemRequest(List<Guid> ids) : IRequest<DeleteItemResponse>;

public sealed record DeleteItemRequest( Guid id) : IRequest<DeleteItemResponse>;