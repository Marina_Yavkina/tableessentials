using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class BatchCreateItemHandler : IRequestHandler<BatchCreateItemRequest, BatchCreateItemResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IItemRepository _itemRepository;
    private readonly IMapper _mapper;

    public BatchCreateItemHandler(IUnitOfWork unitOfWork, IItemRepository itemRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _itemRepository = itemRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchCreateItemResponse> Handle(BatchCreateItemRequest request, CancellationToken cancellationToken)
    {
        try {
        var item = _mapper.Map<List<Item>>(request.batchCreateItem);
        await _itemRepository.AddRange(item);
        await _unitOfWork.Save(cancellationToken);
        }
        catch (Exception ex)
        {
         throw new BadRequestException(ex.Message);
        }

        return new BatchCreateItemResponse{ isSuccessful = true} ;
    }
}