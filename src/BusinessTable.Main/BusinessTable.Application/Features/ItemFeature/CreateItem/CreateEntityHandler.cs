using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class CreateItemHandler : IRequestHandler<CreateItemRequest, CreateItemResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IItemRepository _itemRepository;
    private readonly IMapper _mapper;

    public CreateItemHandler(IUnitOfWork unitOfWork, IItemRepository itemRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _itemRepository = itemRepository;
        _mapper = mapper;
    }
    
    public async Task<CreateItemResponse> Handle(CreateItemRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var item = _mapper.Map<Item>(request);
            await _itemRepository.AddAsync(item);
            await _unitOfWork.Save(cancellationToken);
            return _mapper.Map<CreateItemResponse>(item);
        }
        catch(Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }
    }
}