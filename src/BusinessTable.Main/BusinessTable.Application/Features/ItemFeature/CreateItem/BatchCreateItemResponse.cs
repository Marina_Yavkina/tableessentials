namespace BusinessTable.Application.Features.ItemFeature;

public sealed record BatchCreateItemResponse
{
    public bool isSuccessful {get;set;}
}