using MediatR;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed record  BatchCreateItemRequest(List<CreateItemRequest> batchCreateItem) : IRequest<BatchCreateItemResponse>;

public sealed record CreateItemRequest( string entityId, string parentItemId) : IRequest<CreateItemResponse>;