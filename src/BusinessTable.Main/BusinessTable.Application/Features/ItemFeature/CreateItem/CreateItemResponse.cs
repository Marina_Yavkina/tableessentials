namespace BusinessTable.Application.Features.ItemFeature;

public sealed record CreateItemResponse
{
    public string entityId {get;set;}
    public string id {get;set;}
    public string parentItemId{get;set;}
}