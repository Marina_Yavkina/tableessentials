using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class CreateItemMapper : Profile
{
    public CreateItemMapper()
    {
        CreateMap<Item, CreateItemResponse>()
        .ForMember(dest => dest.entityId, opt => opt.MapFrom(src => src.UnitId))
        .ForMember(dest => dest.parentItemId, opt => opt.MapFrom(src => src.ParentId))
        .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id)); 
        CreateMap<CreateItemRequest,Item>()
        .ForMember(dest => dest.UnitId, opt => opt.MapFrom(src => src.entityId))
        .ForMember(dest => dest.ParentId, opt => opt.MapFrom(src => src.parentItemId))
        .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
        .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
        .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
        .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
        .ForMember(dest=>dest.Values, opt => opt.Ignore())
        .ForMember(dest=>dest.SubItems, opt => opt.Ignore()); 
    }
}