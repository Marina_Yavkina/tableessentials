using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class UpdateItemMapper : Profile
{
    public UpdateItemMapper()
    {
        CreateMap<UpdateItemRequest,Item>()
        .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
        .ForMember(dest => dest.ParentId, opt => opt.MapFrom(src => src.parentItemId))
        .ForMember(dest => dest.UnitId, opt => opt.MapFrom(src => src.entityId)); 
    }
}