using MediatR;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed record UpdateItemRequest( string id, string entityId, string parentItemId) : IRequest<UpdateItemResponse>;