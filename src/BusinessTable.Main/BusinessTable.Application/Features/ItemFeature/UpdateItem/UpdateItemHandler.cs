using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class UpdateItemHandler : IRequestHandler<UpdateItemRequest, UpdateItemResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IItemRepository _itemRepository;
    private readonly IMapper _mapper;

    public UpdateItemHandler(IUnitOfWork unitOfWork, IItemRepository itemRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _itemRepository = itemRepository;
        _mapper = mapper;
    }
    
    public async Task<UpdateItemResponse> Handle(UpdateItemRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var item = _mapper.Map<Item>(request);
            _itemRepository.Update(item);
            await _unitOfWork.Save(cancellationToken);
            return new UpdateItemResponse{isSuccessful = true};
        }
        catch
        {
            return new UpdateItemResponse{isSuccessful = false};
        }
    }
}