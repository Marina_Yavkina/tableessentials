namespace BusinessTable.Application.Features.ItemFeature;

public sealed record UpdateItemResponse
{
    public bool isSuccessful {get;set;}
}