using AutoMapper;
using BusinessTable.Application.Repositories;
using BusinessTable.Domain.Entities;
using MediatR;
using BusinessTable.Application.Features.PeriodValueFeature;
using BusinessTable.Application.Features.SelectedItemFeature;
using System.Collections.Generic;
using BusinessTable.Application.Common.Exceptions;
using BusinessTable.Domain.Common.Constants;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed class GetItemValueHandler : IRequestHandler<GetItemValueRequest, List<GetItemValueResponse>>
{
    private readonly IUnitRepository _unitRepository;
    private readonly IItemRepository _itemRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IPeriodRepository _periodRepository;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IFieldRepository _fieldRepository;
    private readonly IValueRepository _valueRepository;
    private readonly ISelectedDataDictionaryRepository _selectedDataDictionaryRepository;
    private readonly IMapper _mapper;

    public GetItemValueHandler(IUnitRepository unitRepository,IItemRepository itemRepository, IUnitFieldRepository unitFieldRepository,
    IPeriodRepository periodRepository,IPeriodValueRepository periodValueRepository,
     IValueRepository valueRepository,IFieldRepository fieldRepository , ISelectedDataDictionaryRepository selectedDataDictionaryRepository,
     IMapper mapper)
    {
        _unitRepository = unitRepository;
        _itemRepository = itemRepository;
        _unitFieldRepository = unitFieldRepository;
        _periodRepository = periodRepository;
        _periodValueRepository = periodValueRepository;
        _fieldRepository = fieldRepository;
        _valueRepository =valueRepository;
        _selectedDataDictionaryRepository = selectedDataDictionaryRepository;
        _mapper = mapper;
    }
    //fieldTypeId : 0- dateType, 1 - integer, 2 - numeric(30,4), 3 - bool,  4 - string , 5 - DictionaryId, 6 - pivotColumn
    public async Task<List<GetItemValueResponse>> Handle(GetItemValueRequest request, CancellationToken cancellationToken)
    {
        var items = await _itemRepository.GetItemsByEntityIdAsync(request.entityId);
        if (items.Count() == 0)
        {
            throw new NotExistException($"Экземпляров с идентификатором сущности {request.entityId} не существует!");
        }
        
        List<GetItemValueResponse> response =new List<GetItemValueResponse>();
        var ef = _unitFieldRepository.GetAllAsync();
        
        var fieldItems =  items.Join(ef, c => c.UnitId, ct => ct.UnitId, (c,ct) => 
            new { fieldId = ct.FieldId, itemId = c.Id, entityFieldId = ct.Id ,  entityId = ct.UnitId})
            .Join(_fieldRepository.GetAllAsync() , n => n.fieldId , f => f.Id, (n,f) => 
                new { fieldId = n.fieldId, itemId = n.itemId,entityId = n.entityId, typeFieldId = f.FieldTypeId, dataTypeId = f.DataTypeId, entityFieldId = n.entityFieldId, fieldName = f.DisplayName})
                .GroupJoin(_valueRepository.GetAllAsync(),nw => (nw.itemId,nw.entityFieldId),v => (v.ItemId,v.EntityFieldId),
                 (nw,v) => new { nw,  v }).SelectMany( x => x.v.DefaultIfEmpty(),
                                    ( nww, v1) => new GetItemValue()
                { 
                    entityId = nww.nw.entityId.ToString(),
                    itemId = nww.nw.itemId.ToString(),
                    field_id = nww.nw.fieldId.ToString(),
                    fieldName = nww.nw.fieldName,
                    value = ReturnValue(v1),
                    typeFieldId = nww.nw.typeFieldId,
                    dataTypeId = nww.nw.dataTypeId,
                    valueId = (v1!=null)?v1.Id.ToString():"",
                    periodValue =  (v1!=null)? ReturnPeriodValue(v1) : null
                }).ToList();

        response = fieldItems.Where(p => p.typeFieldId < 5).Select(g =>  new GetItemValueResponse()
        {
            entityId = g.entityId,
            itemId = g.itemId,
            field_id = g.field_id,
            fieldName = g.fieldName,
            value = g.value,
            typeFieldId = g.typeFieldId,
            dataTypeId = g.dataTypeId,
            periodValues = null,
            selectedItems = null
         }).ToList();  

        response.AddRange(fieldItems.Where(p => p.typeFieldId == (int)DataFieldType.PivotColumn).Select(g =>  new GetItemValueForResp()
        {
            entityId = g.entityId,
            itemId = g.itemId,
            field_id = g.field_id,
            fieldName = g.fieldName,
            periodValue = g.periodValue,
            typeFieldId = g.typeFieldId,
            dataTypeId = g.dataTypeId
         }).ToList()
        .GroupBy(c => new{ c.entityId,c.itemId,c.field_id ,c.fieldName})
                .Select( cl => new GetItemValueResponse()
                {
                    entityId = cl.Key.entityId,
                    itemId = cl.Key.itemId,
                    field_id = cl.Key.field_id,
                    fieldName = cl.Key.fieldName,
                    typeFieldId = (int)DataFieldType.PivotColumn,
                    value = null,
                    periodValues = cl.ToList(),
                    selectedItems = null
                }).ToList());

        var ls = fieldItems.Where(p => p.typeFieldId == (int)DataFieldType.DictionaryId).Select( g =>  new GetItemValueResponse()
                {
                    entityId = g.entityId,
                    itemId = g.itemId,
                    field_id = g.field_id,
                    fieldName = g.fieldName,
                    typeFieldId = g.typeFieldId,
                    dataTypeId = g.dataTypeId,
                    value = null,
                    periodValues = null,
                    selectedItems = ConvertSelectedItemToResponse(Guid.Parse(g.valueId))
                }).ToList();
        response.AddRange(ls);
        return response;
    }

    public List<GetSelectedItemResponse>? ConvertSelectedItemToResponse(Guid value_id)
    {
        var items = _selectedDataDictionaryRepository.GetSelectedItemsByValueIdAsync(value_id).Result;
        var itms = items.ToList();
        var res =itms.Select(f => new GetSelectedItemResponse {id = f.Id.ToString(), selectedItemId = f.SelectedItemId.ToString()}).ToList();
        return res;
    }
    private  GetPeriodValueResponse? ReturnPeriodValue (Value value)
    {
        //var p =  _periodValueRepository.GetPeriodValueByValueIdAsync(value.Id);
        var p =  _periodValueRepository.GetAllAsync().Where(p => p.ValueId == value.Id).ToList();
        GetPeriodValueResponse? result = p.Join(_periodRepository.GetAllAsync(),pr => pr.PeriodId, pv => pv.Id, (pr,pv) => new  GetPeriodValueResponse
        {
                periodId = pr.PeriodId.ToString(),
                periodName = pv.Name,
                value_dt = pr.Value_dt.ToString("dd-MM-yyyy"),
                value = ReturnValue(value)
        } ).FirstOrDefault();
        return result;
    }
    

 private string? ReturnValue (Value? value)
    {
        if (value == null) return null;
        if ( value.Text != null && value.Text != "")
            return value.Text;
        else if (value!.NumberData != null )
            return value.NumberData.ToString()!;
        else if (value.BoolData != null)
        {
            if (value.BoolData == true)
                return "true";
            else
                return "false"; 
        }
        else if (value.IntegerData != null )
            return value.IntegerData.ToString()!;
        else
            return null;
    }
    
}
