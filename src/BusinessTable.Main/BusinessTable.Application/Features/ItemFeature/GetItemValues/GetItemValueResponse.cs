using BusinessTable.Application.Features.PeriodValueFeature;
using BusinessTable.Application.Features.SelectedItemFeature;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed record GetItemValueResponse
{
    public string entityId { get; set; }
    public string itemId { get; set; }
    public string field_id { get; set; }
    public string fieldName { get; set; }
    public string? value { get; set; }
    public int typeFieldId { get; set; }
    public int dataTypeId { get; set; }
    public List<GetItemValueForResp>? periodValues {get;set;}
    public List<GetSelectedItemResponse>? selectedItems {get;set;}
}

public sealed record GetItemValue
{
    public string entityId { get; set; }
    public string itemId { get; set; }
    public string field_id { get; set; }
    public string fieldName { get; set; }
    public int typeFieldId { get; set; }
    public int dataTypeId { get; set; }
    public string valueId { get; set; }
    public string? value { get; set; }
    public GetPeriodValueResponse? periodValue {get;set;}
}
public sealed record GetItemValueForResp
{
    public string entityId { get; set; }
    public string itemId { get; set; }
    public string field_id { get; set; }
    public string fieldName { get; set; }
    public int typeFieldId { get; set; }
    public int dataTypeId { get; set; }
    public string? value { get; set; }
    public GetPeriodValueResponse? periodValue {get;set;}
}
// public sealed record GetItemValue
// {
//     public string entityId { get; set; }
//     public string itemId { get; set; }
//     public string field_id { get; set; }
//     public int typeFieldId { get; set; }
//     public GetPeriodValueResponse? periodValue {get;set;}
// }