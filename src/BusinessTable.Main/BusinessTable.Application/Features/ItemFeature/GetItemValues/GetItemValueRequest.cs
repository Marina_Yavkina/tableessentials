using MediatR;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed record GetItemValueRequest(Guid entityId) : IRequest<List<GetItemValueResponse>>;