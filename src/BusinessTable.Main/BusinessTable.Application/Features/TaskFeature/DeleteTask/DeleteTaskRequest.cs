using MediatR;
using BusinessTable.Application.Features.AssignmentFeature;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed record RemoveTaskRequest( Guid id) : IRequest<RemoveTaskResponse>;