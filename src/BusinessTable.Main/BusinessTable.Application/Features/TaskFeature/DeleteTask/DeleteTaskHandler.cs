using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using A=BusinessTable.Domain.Entities;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Features.AssignmentFeature;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed class DeleteTaskHandler : IRequestHandler<RemoveTaskRequest, RemoveTaskResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly ITaskRepository _taskRepository;
    private readonly IProjectTypeRepository _projectTypeRepository;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public DeleteTaskHandler(IUnitOfWork unitOfWork, ITaskRepository taskRepository,IProjectTypeRepository projectTypeRepository, IAssignmentRepository assignmentRepository,IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _taskRepository = taskRepository;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<RemoveTaskResponse> Handle(RemoveTaskRequest request, CancellationToken cancellationToken)
    {
       var item = await _taskRepository.GetById(request.id);
        if (item == null)
             throw new NotFoundException("задача", request.id);
        var assigns =  await _assignmentRepository.GetByTaskIdAsync(request.id);
        if (assigns != null)
        {
            await _assignmentRepository.RemoveRange(assigns);
            await _unitOfWork.Save(cancellationToken);
        }
        _taskRepository.Delete(request.id);
         await _unitOfWork.Save(cancellationToken);
         return new RemoveTaskResponse();
    }
}