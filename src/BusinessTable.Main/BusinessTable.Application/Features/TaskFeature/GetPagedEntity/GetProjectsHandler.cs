using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Models;
using A=BusinessTable.Domain.Entities;
using BusinessTable.Application.Contracts;
using BusinessTable.Application.Common.Helpers;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed class GetProjectsHandler : IRequestHandler<GetProjectsRequest, PagedList<GetTaskViewProjects>>
{
    
    private readonly IMapper _mapper;
    private readonly ITaskRepository _taskRepository;
    private readonly IProjectTypeRepository _projectTypeRepository;
        

        public GetProjectsHandler(IMapper mapper, ITaskRepository repository,ITaskValueRepository taskValueRepository,IFieldRepository fieldRepository,IUnitFieldRepository unitFieldRepository,
        IProjectTypeRepository projectTypeRepository, IItemRepository itemRepository, IAssignmentRepository assignmentRepository,ISelectedDataDictionaryRepository selectedDataDictionaryRepository,IValueRepository valueRepository)
        {
            _mapper = mapper;
            _taskRepository = repository;
            _projectTypeRepository = projectTypeRepository;
        }


    public async Task<PagedList<GetTaskViewProjects>> Handle(GetProjectsRequest request, CancellationToken cancellationToken)
    {
        ProjectType projectType = await _projectTypeRepository.GetByName("project");
        var tasks = await _taskRepository.GetOnlyProjectsAsync(request.pageNumber,request.pageSize );
        if (tasks.Entities != null)
        {
            var task = _mapper.Map<IEnumerable<GetTaskViewProjects>>(tasks.Entities);
            return new PagedList<GetTaskViewProjects>
            {
                Entities = task.ToList() ,
                Pagination = tasks.Pagination
            };
        }
        else
        {
            throw new NotFoundException("projects", "" );
        }
    }
}