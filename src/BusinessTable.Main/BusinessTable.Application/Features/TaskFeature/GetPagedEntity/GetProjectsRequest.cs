using MediatR;
using BusinessTable.Domain.Models;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed record GetProjectsRequest(int pageNumber, int pageSize) : IRequest<PagedList<GetTaskViewProjects>>;