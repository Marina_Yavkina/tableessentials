using AutoMapper;
using A=BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed class  UpdateTaskMapper : Profile
{
    public UpdateTaskMapper()
    {
      CreateMap< UpdateTaskRequest,A.Task>()
      .ForMember(dest => dest.Duration, opt => opt.MapFrom(src => src.duration))
      .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => Convert.ToDateTime(src.start_date, 	System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)))
      .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => Convert.ToDateTime(src.end_date, 	System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)))
      .ForMember(dest => dest.BaseStart, opt => opt.MapFrom(src =>  (src.planned_start != "") ? Convert.ToDateTime(src.planned_start , 	System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat): (DateTime?)null))
      .ForMember(dest => dest.BaseEnd, opt => opt.MapFrom(src =>  (src.planned_end != "") ? Convert.ToDateTime(src.planned_end, 	System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat): (DateTime?)null))
      .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.text))
      .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.type))
      .ForMember(dest => dest.Progress, opt => opt.MapFrom(src => src.progress))
      .ForMember(dest => dest.ParentId, opt => opt.MapFrom(src => src.parent))
      .ForMember(dest => dest.ParentTask, opt => opt.Ignore())
      .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
      .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
      .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
      .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
      .ForMember(dest => dest.Priority, opt => opt.Ignore())
      .ForMember(dest => dest.SubTasks, opt => opt.Ignore())
      .ForMember(dest => dest.Assignments, opt => opt.MapFrom(src => src.user))
      .ForMember(dest => dest.ProjectType, opt => opt.Ignore())
      .ForMember(dest => dest.Open, opt => opt.Ignore())
      .ForMember(dest => dest.Assignments, opt => opt.Ignore())
      .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
      .ForMember(dest => dest.ProjectTypeId, opt => opt.MapFrom(src => src.project_type_id));  
    }
}