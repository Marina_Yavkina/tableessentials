using MediatR;
using BusinessTable.Application.Features.AssignmentFeature;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed record UpdateTaskRequest( 
        int duration ,string end_date , Guid id, Guid? parent,decimal progress,string start_date,string? planned_start,
        string? planned_end ,string text,string? type,List<CreateAssignmentRequest>? user,string? project_type_id) : IRequest<UpdateTaskResponse>;