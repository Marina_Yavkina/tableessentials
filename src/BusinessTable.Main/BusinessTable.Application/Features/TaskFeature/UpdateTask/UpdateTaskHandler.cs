using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using A=BusinessTable.Domain.Entities;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Features.AssignmentFeature;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed class UpdateTaskHandler : IRequestHandler<UpdateTaskRequest, UpdateTaskResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly ITaskRepository _taskRepository;
    private readonly IProjectTypeRepository _projectTypeRepository;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public UpdateTaskHandler(IUnitOfWork unitOfWork, ITaskRepository taskRepository,IProjectTypeRepository projectTypeRepository, IAssignmentRepository assignmentRepository,IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _taskRepository = taskRepository;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<UpdateTaskResponse> Handle(UpdateTaskRequest request, CancellationToken cancellationToken)
    {
       var item = await _taskRepository.GetById(request.id,true);
        if (item == null)
            throw new NotExistException("задача");
        var task = _mapper.Map<A.Task>(request);
        if (request.parent != null)
        {
            var subtask = await _taskRepository.GetById(new Guid(request.parent.ToString()),true);
            task.ParentTask = subtask;
        }
        if (request.project_type_id != null)
        {
            Guid pr_id = new Guid(request.project_type_id!);
            var projectType = await _projectTypeRepository.GetById(pr_id);
            task.ProjectType = projectType;
            
        }
        else if  (request.type == "project")
        {
            task.ProjectTypeId = item.ProjectTypeId;
            
        }
        _taskRepository.Update(task);
        await _unitOfWork.Save(cancellationToken);
        //  adding new assignments
        if  (!(request.user== null))
        {
            var users = _mapper.Map<List<Assignment>>(request.user);
            foreach(Assignment user in  users )
            { 
                var assign = await _assignmentRepository.GetById(user.Id,true);
                if (assign == null)
                {
                    user.Id =  Guid.NewGuid();
                    user.TaskId =  task.Id;
                    await _assignmentRepository.AddAsync(user);
                }
            }
            //updating existing assignments or deleting old
            var assigns =  await _assignmentRepository.GetByTaskIdAsync(task.Id);
            if (assigns != null){
                foreach(Assignment assign in  assigns )
                {
                    var assgn = users.Where(c => c.Id == assign.Id).FirstOrDefault();
                    if (assgn != null)
                    {
                        _assignmentRepository.Update(assgn);
                    }
                    else
                    {
                        _assignmentRepository.Delete(assign.Id);
                    }
                }
            }
            await _unitOfWork.Save(cancellationToken);
        }    
        return new UpdateTaskResponse()  ;  
    }
}