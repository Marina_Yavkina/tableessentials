using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Models;
using A=BusinessTable.Domain.Entities;
using BusinessTable.Application.Contracts;
using BusinessTable.Application.Common.Helpers;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed class GetTaskHandler : IRequestHandler<GetTaskRequest, PagedList<GetTaskViewResponse>>
{
    
    private readonly IMapper _mapper;
    private readonly ITaskRepository _taskRepository;
    private readonly ITaskValueRepository _taskValueRepository;
    private readonly IFieldRepository _fieldRepository;
    private readonly IItemRepository _itemRepository;
    private readonly IProjectTypeRepository _projectTypeRepository;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly ISelectedDataDictionaryRepository _selectedDataDictionaryRepository;
    private readonly IValueRepository _valueRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
        

        public GetTaskHandler(IMapper mapper, ITaskRepository repository,ITaskValueRepository taskValueRepository,IFieldRepository fieldRepository,IUnitFieldRepository unitFieldRepository,
        IProjectTypeRepository projectTypeRepository, IItemRepository itemRepository, IAssignmentRepository assignmentRepository,ISelectedDataDictionaryRepository selectedDataDictionaryRepository,IValueRepository valueRepository)
        {
            _mapper = mapper;
            _taskRepository = repository;
            _taskValueRepository = taskValueRepository;
            _fieldRepository = fieldRepository;
            _itemRepository = itemRepository;
            _projectTypeRepository = projectTypeRepository;
            _assignmentRepository = assignmentRepository;
            _selectedDataDictionaryRepository = selectedDataDictionaryRepository;
            _valueRepository = valueRepository;
            _unitFieldRepository = unitFieldRepository;
        }

    public async  Task<PagedList<GetTaskViewResponse>> Handle(GetTaskRequest request, CancellationToken cancellationToken)
    {
        var pagedList =  await _taskRepository.GetTaskByProjectIdPagedAsync(request.id,request.pageNumber, request.pageSize);
        var rootTask = await _taskRepository.GetById(request.id,false);
        var rootTaskVm =_mapper.Map<A.Task, GetTaskViewResponse>(rootTask);
        var taskvm = _mapper.Map<List<A.Task>, List<GetTaskViewResponse>>(pagedList.Entities);
        var res2 = new List<GetTaskViewResponse>();
        res2.Add(rootTaskVm);
        foreach(GetTaskViewResponse tv in taskvm)
        {
                //  находим значения полей для текущей задачи в values
                var taskValues = _taskValueRepository.GetAllAsync().Where(x => x.TaskId == new Guid(tv.id)).Join(_valueRepository.GetAllAsync(),x => new MultipleKey{ val_id = x.ValueId, id= x.ItemId}, y => new MultipleKey{ val_id = y.Id, id= y.ItemId},
                (x,y) => new  TaskValueEF{ entity_field_id = y.EntityFieldId, item_id = y.ItemId, task_id = x.TaskId, value_id = y.Id, text_data = y.Text,numeric_data = y.NumberData,bool_data = y.BoolData } )
                .Join(_unitFieldRepository.GetAllAsync(), x => x.entity_field_id ,y => y.Id, (x,y) => new  TaskValueEF{ entity_field_id = x.entity_field_id, item_id = x.item_id, task_id = x.task_id, value_id = x.value_id, text_data = x.text_data,numeric_data = x.numeric_data,bool_data = x.bool_data,field_id = y.FieldId } )
                .Join(_fieldRepository.GetAllAsync(), x => x.field_id, y => y.Id, (x,y) =>  new TaskValueFieldDto{
                    field_id = y.Id,
                    task_id = x.task_id,
                    text_data = x.text_data,       
                    numeric_data = x.numeric_data,
                    bool_data = x.bool_data,
                    item_id = x.item_id,
                    value_id = x.value_id,
                    unit_field_id = x.entity_field_id,
                    field = y.DisplayName,
                    lookupEntityId = y.LookupEntityId,
                    lookupEntityFieldId = y.LookupEntityFieldId,
                    lookupMultipleSelection = y.LookupMultipleSelection                
                }).ToList();
                
               
                List<TaskValueView> tvList = new List<TaskValueView>();
                Item instance = new Item();
                if (taskValues != null)  
                    foreach( var item in taskValues )
                    {
                        List<Guid> selectedIds = new List<Guid>();
                        string value = "";
                        if (item.text_data != null && item.text_data != "")
                            value = item.text_data;
                        else if (item.numeric_data != null )
                            value = item.numeric_data.ToString()!;
                        else if (item.bool_data != null)
                        {
                            if (item.bool_data == true)
                                value = "true";
                            else
                                value = "false"; 
                        }
                        else if (item.lookupEntityId != null )
                        {
                            if (item.value_id != null)
                            {
                                Guid value_id =   (Guid)item.value_id;
                                List<SelectedDictionaryItem> selectedItemsList = await _selectedDataDictionaryRepository.GetSelectedItemsByValueIdAsync(value_id );
                                selectedIds = (List<Guid>)selectedItemsList.Select(c => new List<Guid>{c.SelectedItemId});
                               
                            }
                        }
                        TaskValueView taskView = new TaskValueView{ field = item.field,value = value, field_id = item.field_id.ToString(),lookupEntityFieldId = item.lookupEntityFieldId,
                        lookupMultipleSelection = item.lookupMultipleSelection, lookupEntityId = item.lookupEntityId, ids = selectedIds } ;
                        tvList.Add(taskView);   
                    }
                tv.values = tvList;
                res2.Add(tv);
            }              
                
        return new PagedList<GetTaskViewResponse>
        {
            Entities = res2,
            Pagination = pagedList.Pagination
        };
    }
    // public async Task<PagedList<TaskViewModel>> GetOnlyProjectsPage(int pageNumber, int pageSize)
    // {
    //     ProjectType projectType = await _projectTypeRepository.GetByName("project");
    //     var tasks = await _taskRepository.GetOnlyProjectsAsync(pageNumber, pageSize );
    //     if (tasks.Entities != null)
    //     {
    //         var task = _mapper.Map<IEnumerable<TaskViewModel>>(tasks.Entities);
    //         return new PagedList<TaskViewModel>
    //         {
    //             Entities = task.ToList() ,
    //             Pagination = tasks.Pagination
    //         };
    //     }
    //     else
    //     {
    //         throw new NotFoundException("projects", "" );
    //     }
    // }
    
    // public async Task<TaskDto> Get(Guid id)
    // {
    //     var task = await _taskRepository.GetAsync(id, true);

    //     if (task == null)
    //         throw new NotFoundException("projects", id);
    //     return _mapper.Map<TaskDto>(task);
    // }    
}