namespace BusinessTable.Application.Features.TaskFeature;

public class GetTaskViewResponse{public string id { get; set; } 
public string text { get; set; } 
 public string start_date { get; set; } 
 public int duration{ get; set; } public decimal progress{ get; set; } 
 public string parent { get; set; }
 public string type { get; set; } 
 public string planned_start{ get; set; }
 public string planned_end{ get; set; }
 public bool open { get; set; }
 public List<AssignViewModel>? user{ get; set; }
 public string? holder  { get; set; }
 public string? priority { get; set; }
 public string? project_type_id { get; set; }
 public List<TaskValueView>? values { get; set; }
 };


public sealed record AssignViewModel
{
    public string Id { get; set; }
    public string Resource_id { get; set; }
    public string Value { get; set; }
    public string? Start_date { get; set; }
    public int? Delay {get; set;}
    public int? Duration {get; set;}
    public string? Mode {get;set;}
    public string? Unit {get;set;}
}
public sealed record TaskValueView
{
    public string field { get; set; }
    public string field_id { get; set; }
    public string value { get; set; }
    public Guid? lookupEntityId { get; set; }
    public Guid? lookupEntityFieldId { get; set; }
    public bool? lookupMultipleSelection {get;set;}
    public List<Guid>? ids {get;set;} 
}
public sealed record GetSelectedIdsResponse( List<Guid> id );



