using System.Globalization;
using AutoMapper;
using A=BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed class GetTaskMapper : Profile
{
    public GetTaskMapper()
    {
        CultureInfo currentCulture = CultureInfo.CurrentCulture;
        CreateMap<A.Task, GetTaskViewResponse>()
      .ForMember(dest => dest.duration, opt => opt.MapFrom(src => src.Duration))
      .ForMember(dest => dest.start_date, opt => opt.MapFrom(src => src.StartDate.ToString("dd-MM-yyyy", currentCulture)))
      .ForMember(dest => dest.planned_start, opt => opt.MapFrom(src => src.BaseStart.HasValue ? src.BaseStart.Value.ToString("dd-MM-yyyy", currentCulture): null))
      .ForMember(dest => dest.planned_end, opt => opt.MapFrom(src => src.BaseEnd.HasValue ? src.BaseEnd.Value.ToString("dd-MM-yyyy", currentCulture): null))
      .ForMember(dest => dest.text, opt => opt.MapFrom(src => src.Text))
      .ForMember(dest => dest.progress, opt => opt.MapFrom(src => src.Progress))
      .ForMember(dest => dest.type, opt => opt.MapFrom(src => src.Type))
      .ForMember(dest => dest.project_type_id, opt => opt.MapFrom(src => src.ProjectTypeId))
      .ForMember(dest => dest.parent,opt =>  opt.MapFrom(src => src.ParentId ))
      .ForMember(dest => dest.user, opt => opt.MapFrom(src =>src.Assignments))
      .ForMember(dest => dest.values, opt => opt.Ignore())
      .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id));  
   }  
    
}