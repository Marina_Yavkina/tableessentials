using MediatR;
using BusinessTable.Domain.Models;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed record GetTaskRequest(Guid id, int pageNumber, int pageSize) : IRequest<PagedList<GetTaskViewResponse>>;