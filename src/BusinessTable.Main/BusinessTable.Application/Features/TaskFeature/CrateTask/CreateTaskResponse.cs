namespace BusinessTable.Application.Features.TaskFeature;

public sealed record CreateTaskResponse
{
   public  Guid id {get;set;}
}