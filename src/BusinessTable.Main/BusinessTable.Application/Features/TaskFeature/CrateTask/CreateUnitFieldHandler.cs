using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using A=BusinessTable.Domain.Entities;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Features.AssignmentFeature;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.TaskFeature;

public sealed class CreateTaskHandler : IRequestHandler<CreateTaskRequest, CreateTaskResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly ITaskRepository _taskRepository;
    private readonly IProjectTypeRepository _projectTypeRepository;
    private readonly IAssignmentRepository _assignmentRepository;
    private readonly IMapper _mapper;

    public CreateTaskHandler(IUnitOfWork unitOfWork, ITaskRepository taskRepository,IProjectTypeRepository projectTypeRepository, IAssignmentRepository assignmentRepository,IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _taskRepository = taskRepository;
        _assignmentRepository = assignmentRepository;
        _mapper = mapper;
    }
    
    public async Task<CreateTaskResponse> Handle(CreateTaskRequest request, CancellationToken cancellationToken)
    {
        try {
        var task = _mapper.Map<A.Task>(request);
                if (request.parent != null)
                {
                    var subtask = await _taskRepository.GetById((Guid)request.parent);
                    task.ParentTask = subtask;
                }
                if (request.project_type_id != null)
                {
                    var projectType = await _projectTypeRepository.GetById(new Guid(request.project_type_id));
                    task.ProjectType = projectType;
                }
            
                task.Id = Guid.NewGuid();
                var new_task =  await _taskRepository.AddAsync(task);
                await _unitOfWork.Save(cancellationToken);
                if  (!(request.user == null))
                {
                    var users = _mapper.Map<List<Assignment>>(request.user);
                    foreach(Assignment user in  users )
                    {
                        user.TaskId =  new_task.Id;
                        await _assignmentRepository.AddAsync(user);
                    }
                }
                await _unitOfWork.Save(cancellationToken);
                var item = await _taskRepository.GetById(task.Id);

                if (item == null)
                    throw new NotFoundException("задача", task.Id);
                return new CreateTaskResponse(){id = task.Id};
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
            return new CreateTaskResponse();
        }
       
        
    }
}