using MediatR;
using BusinessTable.Application.Features.SelectedItemFeature;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed record  BatchCreateItemValueRequest(List<CreateItemValueRequest> batchCreateItemValue) : IRequest<BatchCreateItemValueResponse>;

public sealed record CreateItemValueRequest( 
     string field_id, string? value_dt, string value, string unit_id, Guid? period_id,  List<SelectedItemCreate>? selectedItems) : IRequest<CreateItemValueResponse>;




