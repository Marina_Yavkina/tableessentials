namespace BusinessTable.Application.Features.ValueFeature;

public sealed record BatchCreateItemValueResponse
{
    public bool isSuccessful {get;set;}
}