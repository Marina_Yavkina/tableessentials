namespace BusinessTable.Application.Features.ValueFeature;

public sealed record UpdateItemValueResponse
{
    public bool isSuccessful {get;set;}
    
}