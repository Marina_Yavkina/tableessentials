using MediatR;
using BusinessTable.Application.Features.SelectedItemFeature;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed record  BatchUpdateItemValueRequest(List<UpdateItemValueRequest> batchUpdateItemValue) : IRequest<BatchUpdateItemValueResponse>;

public sealed record UpdateItemValueRequest( 
     string? item_id , string field_id, string? value_dt, string value, Guid? period_id,  List<SelectedItemCreate>? selectedItems) : IRequest<UpdateItemValueResponse>;




