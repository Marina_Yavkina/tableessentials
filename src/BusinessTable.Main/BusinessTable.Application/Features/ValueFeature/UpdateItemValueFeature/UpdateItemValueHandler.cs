using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Base;
using BusinessTable.Application.Features.SelectedItemFeature;
using BusinessTable.Application.Features.PeriodValueFeature;
using BusinessTable.Domain.Common.Constants;
namespace BusinessTable.Application.Features.ValueFeature;

public sealed class UpdateItemValueHandler : IRequestHandler<UpdateItemValueRequest, UpdateItemValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValueRepository _valueRepository;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IFieldRepository _fieldRepository;
    private readonly IItemRepository _itemRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly ISelectedDataDictionaryRepository _selectedDataDictionaryRepository;
    
    private readonly IMapper _mapper;

    public UpdateItemValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository,IValueRepository valueRepository, IUnitFieldRepository unitFieldRepository,    
    ISelectedDataDictionaryRepository selectedDataDictionaryRepository,IFieldRepository fieldRepository, IItemRepository itemRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _valueRepository = valueRepository;
        _selectedDataDictionaryRepository = selectedDataDictionaryRepository;
        _periodValueRepository = periodValueRepository;
        _itemRepository = itemRepository;
        _fieldRepository = fieldRepository;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<UpdateItemValueResponse> Handle(UpdateItemValueRequest request, CancellationToken cancellationToken)
    {
        string value_id = "";
        if (request.item_id != null) 
        {
            Field field = await _fieldRepository.GetAsync(new Guid(request.field_id),true);
            Value? valueToAdd; 
            
            Item item = await _itemRepository.GetAsync(new Guid(request.item_id),true) ;
            EntityField? unit_field =  _unitFieldRepository.GetAllAsync().Where(c => c.UnitId == item.UnitId && c.FieldId == new Guid(request.field_id)).FirstOrDefault();
            
            var valueList = await  _valueRepository.GetListWithInclude(c => c.ItemId == item.Id && c.EntityFieldId == unit_field!.Id, c => c.PeriodValue);
        
            if (request.value_dt != null)
            {
                valueToAdd = valueList.Where(c => c.PeriodValue.Value_dt ==  Convert.ToDateTime(request.value_dt ,System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)).FirstOrDefault();
            }
            else
            {
                valueToAdd = valueList.FirstOrDefault();  
            }
            value_id = valueToAdd.Id.ToString();
            if (valueToAdd == null)
            {
                valueToAdd = new Value();
                valueToAdd.ItemId = item.Id;
                valueToAdd.EntityFieldId = unit_field!.Id;
            }
            
            switch (field.DataTypeId)
            {
                case (int)DataType.Cost :
                {
                    valueToAdd.NumberData = float.Parse(request.value);
                    break;
                }
                case (int)DataType.Float:
                {
                    valueToAdd.NumberData = float.Parse(request.value);
                    break;
                }
                case (int)DataType.Integer:
                {
                    valueToAdd.IntegerData = int.Parse(request.value);
                    break;
                }
                case (int)DataType.Text:
                {
                    valueToAdd.Text = request.value;
                    break;
                }
                case (int)DataType.Bool:
                {
                    valueToAdd.BoolData = bool.Parse(request.value);
                    break;
                }
                case (int)DataType.Date:
                {
                    valueToAdd.DateData =  Convert.ToDateTime(request.value ,System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat);
                    break;
                }
                 default:
                {
                    break;
                }
            }
            if (value_id == "")
            {
                var v = await _valueRepository.AddAsync(valueToAdd);
                value_id = v.Id.ToString();
            }
            else
            {
                _valueRepository.Update(valueToAdd);
            }
            switch (field.FieldTypeId)
            {
                case (int)DataFieldType.DictionaryId:
                {
                    if (request.selectedItems != null)
                        {
                            foreach (var sel_item in request.selectedItems)
                            {
                                SelectedItemUpdateRequest req = new SelectedItemUpdateRequest();                            
                                req = new SelectedItemUpdateRequest{id = sel_item.selectedItem_id, value_id = value_id,selectedItem_id = sel_item.selectedItem_id };
                                _selectedDataDictionaryRepository.Update(_mapper.Map<SelectedDictionaryItem>(req));
                            }
                        }
                    break;
                }
                case  (int)DataFieldType.PivotColumn:
                {
                    var periodVal = _periodValueRepository.GetWithIncludeAsync(c => c.PeriodId == request.period_id && c.ValueId == new Guid(value_id) && c.Value_dt ==  Convert.ToDateTime(request.value_dt ,System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat));
                    if (periodVal == null && request.period_id != null)
                    {
                        var pvr = new PeriodValue();
                        pvr.PeriodId = (Guid)request.period_id;  
                        pvr.Value_dt = Convert.ToDateTime(request.value_dt ,System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat);
                        pvr.ValueId = new Guid(value_id);
                      
                        await _periodValueRepository.AddAsync(pvr);
                    }
                    break;
                }
                 default:
                {
                    break;
                }


            }
        }
        try
        {
            await _unitOfWork.Save(cancellationToken);
            return  new UpdateItemValueResponse{isSuccessful = true};
        }
        catch
        {
            return  new UpdateItemValueResponse{isSuccessful = false};
        }
    }
}