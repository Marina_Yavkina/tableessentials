namespace BusinessTable.Application.Features.ValueFeature;

public sealed record BatchUpdateItemValueResponse
{
    public bool isSuccessful {get;set;}
}