namespace BusinessTable.Application.Features.ValueFeature;

public sealed record BatchUpdateValueResponse
{
    public bool isSuccessful {get;set;}
}