using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Base;
using BusinessTable.Application.Features.SelectedItemFeature;
using BusinessTable.Application.Features.PeriodValueFeature;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed class UpdateValueHandler : IRequestHandler<ValueUpdateRequest, ValueUpdateResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValueRepository _valueRepository;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly ISelectedDataDictionaryRepository _selectedDataDictionaryRepository;
    
    private readonly IMapper _mapper;

    public UpdateValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository,IValueRepository valueRepository,
    ISelectedDataDictionaryRepository selectedDataDictionaryRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _valueRepository = valueRepository;
        _selectedDataDictionaryRepository = selectedDataDictionaryRepository;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<ValueUpdateResponse> Handle(ValueUpdateRequest request, CancellationToken cancellationToken)
    {
        var pv = _mapper.Map<Value>(request);
        _valueRepository.Update(pv);
        switch (request.dataFieldType)
        {
            case 5:
            {   
                if (request.selectedItems != null)
                {
                   
                    foreach (var item in request.selectedItems)
                    {
                         SelectedItemUpdateRequest req = new SelectedItemUpdateRequest();
                    
                        req = new SelectedItemUpdateRequest{id = item.selectedItem_id, value_id = request.id,selectedItem_id = item.selectedItem_id };
                        _selectedDataDictionaryRepository.Update(_mapper.Map<SelectedDictionaryItem>(req));
                    }
                }
                break;
            }
            case 6:
            {
                if (request.periodValue != null)
                {
                    var pvr = new PeriodValueUpdateRequest
                    (
                        request.periodValue.id,
                        request.id,
                        request.periodValue.value_dt, 
                        request.periodValue.period_id.ToString() 
                    );
                    _periodValueRepository.Update(_mapper.Map<PeriodValue>(pvr));
                }
                break;
            }
            default:
            {
                break;
            }
        }
        try
        {
            await _unitOfWork.Save(cancellationToken);
            return  new ValueUpdateResponse{isSuccessful = true};
        }
        catch
        {
            return  new ValueUpdateResponse{isSuccessful = false};
        }
    }
}