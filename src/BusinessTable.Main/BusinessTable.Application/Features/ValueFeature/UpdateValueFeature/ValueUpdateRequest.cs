using MediatR;
using BusinessTable.Application.Features.SelectedItemFeature;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed record  BatchUpdateValueRequest(List<ValueUpdateRequest> batchUpdateValue) : IRequest<BatchUpdateValueResponse>;

public sealed record ValueUpdateRequest( 
   string  entityField_id ,int dataFieldType,int dataType, string item_id, string? text_data,double numeric_data ,int integer_data,
   bool? bool_data ,string? instance_id ,string? value_id, UpdatePeriodValue? periodValue, string id,
   List<SelectedItemCreate>? selectedItems  ) : IRequest<ValueUpdateResponse>;



public sealed record SelectedItemUpdate
{
   public string id {get;set;}
   public string selectedItem_id {get;set;}
}

public sealed record UpdatePeriodValue
{
    public string? id {get;set;}
    public string period_id {get;set;}
    public string value_dt {get;set;}
}