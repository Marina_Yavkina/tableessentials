using AutoMapper;
using BusinessTable.Domain.Entities;
using System.Globalization;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed class UpdateValueMapper : Profile
{
    CultureInfo currentCulture = CultureInfo.CurrentCulture;
    public UpdateValueMapper()
    {
        CreateMap<ValueUpdateRequest,Value>()
        .ForMember(dest => dest.DateData, opt => {
            opt.PreCondition(src => (src.dataType == 0));
            opt.MapFrom(src =>  Convert.ToDateTime(src.text_data,System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat));
            })
        .ForMember(dest => dest.Text, opt => {
            opt.PreCondition(src => (src.dataFieldType != 0));
            opt.MapFrom(src =>  src.text_data);
            })
        .ForMember(dest => dest.ItemId, opt => opt.MapFrom(src => src.item_id))
        .ForMember(dest => dest.IntegerData, opt => opt.MapFrom(src => src.integer_data))
        .ForMember(dest => dest.NumberData, opt => opt.MapFrom(src => src.numeric_data))
        .ForMember(dest => dest.BoolData, opt => opt.MapFrom(src => src.bool_data))
        .ForMember(dest => dest.EntityFieldId, opt => opt.MapFrom(src => src.entityField_id))
        .ForMember(dest => dest.IsDeleted, opt => opt.MapFrom(src => src.value_id))
        .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
        .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
        .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
        .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
        .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
        .ForMember(dest => dest.PeriodValue, opt => opt.Ignore())
        .ForMember(dest => dest.SelectedDictionaryItems, opt => opt.Ignore());
        
    }
}