namespace BusinessTable.Application.Features.ValueFeature;

public sealed record ValueUpdateResponse
{
    public bool isSuccessful {get;set;}
    
}