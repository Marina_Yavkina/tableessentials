using MediatR;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed record  BatchDeleteValueRequest(List<Guid> ids) : IRequest<DeleteValueResponse>;

public sealed record DeleteValueRequest( Guid id) : IRequest<DeleteValueResponse>;