namespace BusinessTable.Application.Features.ValueFeature;

public sealed record DeleteValueResponse
{    
    public bool isSuccessful {get;set;}
}