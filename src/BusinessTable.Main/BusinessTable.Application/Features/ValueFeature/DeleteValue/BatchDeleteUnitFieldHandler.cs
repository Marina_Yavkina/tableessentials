using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed class BatchDeleteValueHandler : IRequestHandler<BatchDeleteValueRequest, DeleteValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValueRepository _valueRepository;
    private readonly IMapper _mapper;

    public BatchDeleteValueHandler(IUnitOfWork unitOfWork, IValueRepository valueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _valueRepository = valueRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteValueResponse> Handle(BatchDeleteValueRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var values = await _valueRepository.GetListWithInclude(p => request.ids.Contains(p.Id));
            await _valueRepository.RemoveRange(values);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteValueResponse{ isSuccessful = true} ;
        }
        catch (Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }

        
    }
}