using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed class DeleteValueHandler : IRequestHandler<DeleteValueRequest, DeleteValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValueRepository _valueRepository;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IMapper _mapper;

    public DeleteValueHandler(IUnitOfWork unitOfWork, IValueRepository valueRepository,IPeriodValueRepository periodValueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _valueRepository = valueRepository;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteValueResponse> Handle(DeleteValueRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var periodValue = _periodValueRepository.GetAllAsync().Where(c => c.ValueId == request.id).FirstOrDefault();
            if (periodValue != null)
            {
                _periodValueRepository.Delete(periodValue.Id);
            }
            _valueRepository.Delete(request.id);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteValueResponse{isSuccessful = true};
        }
         catch(Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }

    }
}