using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Base;
using BusinessTable.Application.Features.SelectedItemFeature;
using BusinessTable.Application.Features.PeriodValueFeature;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed class CreateValueHandler : IRequestHandler<ValueCreateRequest, ValueCreateResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValueRepository _valueRepository;
    private readonly IPeriodValueRepository _periodValueRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IFieldRepository _fieldRepository;

    private readonly ISelectedDataDictionaryRepository _selectedDataDictionaryRepository;
    
    private readonly IMapper _mapper;

    public CreateValueHandler(IUnitOfWork unitOfWork, IPeriodValueRepository periodValueRepository,IValueRepository valueRepository,
    IUnitFieldRepository unitFieldRepository,ISelectedDataDictionaryRepository selectedDataDictionaryRepository,IFieldRepository fieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _valueRepository = valueRepository;
        _unitFieldRepository = unitFieldRepository;
        _fieldRepository = fieldRepository;
        _selectedDataDictionaryRepository = selectedDataDictionaryRepository;
        _periodValueRepository = periodValueRepository;
        _mapper = mapper;
    }
    
    public async Task<ValueCreateResponse> Handle(ValueCreateRequest request, CancellationToken cancellationToken)
    {
        var pv = _mapper.Map<Value>(request);
        Value result = await _valueRepository.AddAsync(pv);
        switch (request.dataFieldType)
        {
            case 5:
            {   
                if (request.selectedItems != null)
                {
                    List<SelectedItemCreateRequest> list = new List<SelectedItemCreateRequest>();
                    foreach (var item in request.selectedItems)
                    {
                    
                        list.Add(new SelectedItemCreateRequest{ value_id = result.Id.ToString(), selectedItem_id = item.selectedItem_id });
                    }
                    
                    await _selectedDataDictionaryRepository.AddRange(_mapper.Map<List<SelectedDictionaryItem>>(list));
                }
                break;
            }
            case 6:
            {
                if (request.periodValue != null)
                {
                    var pvr = new PeriodValueCreateRequest
                    (
                        result.Id.ToString(),
                        request.periodValue.value_dt, 
                        request.periodValue.period_id.ToString() 
                    );
                    await _periodValueRepository.AddAsync(_mapper.Map<PeriodValue>(pvr));
                }
                break;
            }
            default:
            {
                break;
            }
        }
        
        await _unitOfWork.Save(cancellationToken);

        return  new ValueCreateResponse{id = result.Id.ToString()};
    }
}