using MediatR;
using BusinessTable.Application.Features.SelectedItemFeature;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed record  BatchCreateValueRequest(List<ValueCreateRequest> batchCreatePeriodValue) : IRequest<BatchCreateValueResponse>;

public sealed record ValueCreateRequest( 
   string  entityField_id ,int dataFieldType, string item_id, string? text_data,double numeric_data ,int integer_data,
   bool? bool_data ,string? instance_id ,string? value_id, CreatePeriodValue? periodValue, 
   List<SelectedItemCreate>? selectedItems  ) : IRequest<ValueCreateResponse>;



public sealed record SelectedItemCreate
{
   public string selectedItem_id {get;set;}
}

public sealed record CreatePeriodValue
{
    public string period_id {get;set;}
    public string value_dt {get;set;}
}