using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed class BatchCreateValueHandler : IRequestHandler<BatchCreateValueRequest, BatchCreateValueResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IValueRepository _valueRepository;
    private readonly IMapper _mapper;

    public BatchCreateValueHandler(IUnitOfWork unitOfWork, IValueRepository valueRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _valueRepository = valueRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchCreateValueResponse> Handle(BatchCreateValueRequest request, CancellationToken cancellationToken)
    {
        try {
        var pv = _mapper.Map<List<Value>>(request.batchCreatePeriodValue);
        await _valueRepository.AddRange(pv);
        await _unitOfWork.Save(cancellationToken);
        }
        catch (Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }
            return new BatchCreateValueResponse{ isSuccessful = true} ;
    }
}