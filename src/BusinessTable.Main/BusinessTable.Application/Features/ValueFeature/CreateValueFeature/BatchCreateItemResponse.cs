namespace BusinessTable.Application.Features.ValueFeature;

public sealed record BatchCreateValueResponse
{
    public bool isSuccessful {get;set;}
}