using MediatR;

namespace BusinessTable.Application.Features.ItemFeature;

public sealed record GetValueRequest(Guid entityId) : IRequest<List<GetItemValueResponse>>;