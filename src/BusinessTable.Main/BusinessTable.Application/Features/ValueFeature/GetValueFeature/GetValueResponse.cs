using BusinessTable.Application.Features.PeriodValueFeature;
using BusinessTable.Application.Features.SelectedItemFeature;

namespace BusinessTable.Application.Features.ValueFeature;

public sealed record GetValueResponse
{
    public string entityId { get; set; }
    public string itemId { get; set; }
    public string field_id { get; set; }
    public string? value { get; set; }
    public List<GetPeriodValueResponse>? periodValues {get;set;}
    public List<GetSelectedItemResponse>? selectedItems {get;set;}
}

