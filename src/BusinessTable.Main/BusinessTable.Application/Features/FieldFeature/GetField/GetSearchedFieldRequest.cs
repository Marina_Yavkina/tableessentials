using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed record GetSearchedFieldRequest(string text) : IRequest<List<GetAllFieldResponse>>;