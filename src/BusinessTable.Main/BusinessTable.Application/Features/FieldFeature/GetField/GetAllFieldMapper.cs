using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class GetAllFieldMapper : Profile
{
    public GetAllFieldMapper()
    {
        CreateMap<Field, GetAllFieldResponse>()
        .ForMember(dest => dest.displayName, opt => opt.MapFrom(src => src.DisplayName))
        .ForMember(dest => dest.fieldTypeId, opt => opt.MapFrom(src => src.FieldTypeId))
        .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
        .ForMember(dest => dest.lookupEntityId, opt => opt.MapFrom(src => src.LookupEntityId))
        .ForMember(dest => dest.lookupEntityFieldId, opt => opt.MapFrom(src => src.LookupEntityFieldId))
        .ForMember(dest => dest.lookupMultipleSelection, opt => opt.MapFrom(src => src.LookupMultipleSelection)); 
    }
}
