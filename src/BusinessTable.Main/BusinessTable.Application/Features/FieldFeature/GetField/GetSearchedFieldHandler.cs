using AutoMapper;
using BusinessTable.Application.Common.Exceptions;
using BusinessTable.Application.Repositories;
using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class GetSearchedFieldHandler : IRequestHandler<GetSearchedFieldRequest, List<GetAllFieldResponse>>
{
    private readonly IFieldRepository _fieldRepository;
    private readonly IMapper _mapper;

    public GetSearchedFieldHandler(IFieldRepository fieldRepository, IMapper mapper)
    {
        _fieldRepository = fieldRepository;
        _mapper = mapper;
    }
    
    public async Task<List<GetAllFieldResponse>> Handle(GetSearchedFieldRequest request, CancellationToken cancellationToken)
    {
        var fields = await _fieldRepository.GetPagedByTextAsync(request.text);
        if (fields == null)
                throw new NotExistException($"Полей с наименованием, включающим строку {request.text} не существует! ");
        return _mapper.Map<List<GetAllFieldResponse>>(fields);
    }
}