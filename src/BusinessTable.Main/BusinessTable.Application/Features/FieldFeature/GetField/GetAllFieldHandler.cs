using AutoMapper;
using BusinessTable.Application.Common.Exceptions;
using BusinessTable.Application.Repositories;
using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class GetAllFieldHandler : IRequestHandler<GetAllFieldRequest, List<GetAllFieldResponse>>
{
    private readonly IFieldRepository _fieldRepository;
    private readonly IMapper _mapper;

    public GetAllFieldHandler(IFieldRepository fieldRepository, IMapper mapper)
    {
        _fieldRepository = fieldRepository;
        _mapper = mapper;
    }
    
    public async Task<List<GetAllFieldResponse>> Handle(GetAllFieldRequest request, CancellationToken cancellationToken)
    {
        var fields = await _fieldRepository.GetAll(cancellationToken);
        if (fields == null)
                throw new NotExistException("Список полей пуст!");
        return _mapper.Map<List<GetAllFieldResponse>>(fields);
    }
}