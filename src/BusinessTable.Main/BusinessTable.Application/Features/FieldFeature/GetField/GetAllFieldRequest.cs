using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed record GetAllFieldRequest : IRequest<List<GetAllFieldResponse>>;