namespace BusinessTable.Application.Features.FieldFeature;

public sealed record BatchCreateFieldResponse
{
    public bool isSuccessful {get;set;}
}