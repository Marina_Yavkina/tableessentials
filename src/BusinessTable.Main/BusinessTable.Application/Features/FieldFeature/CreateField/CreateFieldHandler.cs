using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class CreateFieldHandler : IRequestHandler<CreateFieldRequest, CreateFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IFieldRepository _fieldRepository;
    private readonly IMapper _mapper;

    public CreateFieldHandler(IUnitOfWork unitOfWork, IFieldRepository fieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _fieldRepository = fieldRepository;
        _mapper = mapper;
    }
    
    public async Task<CreateFieldResponse> Handle(CreateFieldRequest request, CancellationToken cancellationToken)
    {
        var field = _mapper.Map<Field>(request);
        await _fieldRepository.AddAsync(field);
        await _unitOfWork.Save(cancellationToken);
        return _mapper.Map<CreateFieldResponse>(field);
    }
}