using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed record  BatchCreateFieldRequest(List<CreateFieldRequest> batchCreateField) : IRequest<BatchCreateFieldResponse>;

public sealed record CreateFieldRequest(string displayName, int fieldTypeId, Guid? lookupEntityId ,Guid? lookupEntityFieldId,
        bool? lookupMultipleSelection) : IRequest<CreateFieldResponse>;