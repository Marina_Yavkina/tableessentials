using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class BatchCreateFieldHandler : IRequestHandler<BatchCreateFieldRequest, BatchCreateFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IFieldRepository _fieldRepository;
    private readonly IMapper _mapper;

    public BatchCreateFieldHandler(IUnitOfWork unitOfWork, IFieldRepository fieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _fieldRepository = fieldRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchCreateFieldResponse> Handle(BatchCreateFieldRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var fields = _mapper.Map<List<Field>>(request.batchCreateField);
            await _fieldRepository.AddRange(fields);
            await _unitOfWork.Save(cancellationToken);
            return new BatchCreateFieldResponse{ isSuccessful = true};
        }
        catch (Exception ex)
        {
             throw new BadRequestException(ex.Message);
        }

        ;
    }
}