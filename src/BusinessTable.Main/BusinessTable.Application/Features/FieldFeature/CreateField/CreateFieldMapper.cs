using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class CreateFieldMapper : Profile
{
    public CreateFieldMapper()
    {
        CreateMap<Field, CreateFieldResponse>()
        .ForMember(dest => dest.displayName, opt => opt.MapFrom(src => src.DisplayName))
        .ForMember(dest => dest.fieldTypeId, opt => opt.MapFrom(src => src.FieldTypeId)) 
        .ForMember(dest => dest.lookupEntityId, opt => opt.MapFrom(src => src.LookupEntityId))
        .ForMember(dest => dest.lookupEntityFieldId, opt => opt.MapFrom(src => src.LookupEntityFieldId))
        .ForMember(dest => dest.lookupMultipleSelection, opt => opt.MapFrom(src => src.LookupMultipleSelection));

        CreateMap<CreateFieldRequest,Field>()
        .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.displayName))
        .ForMember(dest => dest.FieldTypeId, opt => opt.MapFrom(src => src.fieldTypeId)) 
        .ForMember(dest => dest.LookupEntityId, opt => opt.MapFrom(src => src.lookupEntityId))
        .ForMember(dest => dest.LookupEntityFieldId, opt => opt.MapFrom(src => src.lookupEntityFieldId))
        .ForMember(dest => dest.LookupMultipleSelection, opt => opt.MapFrom(src => src.lookupMultipleSelection))
        .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
        .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
        .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
        .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
        .ForMember(dest => dest.EntityFields, opt => opt.Ignore());
    }
}