using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed record GetFieldByUnitRequest(Guid entityId) : IRequest<List<GetFieldByUnitResponse>>;