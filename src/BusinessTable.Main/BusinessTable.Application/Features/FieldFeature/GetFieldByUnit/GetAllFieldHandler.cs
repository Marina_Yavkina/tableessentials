using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class GetFieldByUnitHandler : IRequestHandler<GetFieldByUnitRequest, List<GetFieldByUnitResponse>>
{
    private readonly IFieldRepository _fieldRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public GetFieldByUnitHandler(IFieldRepository fieldRepository,IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _fieldRepository = fieldRepository;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<List<GetFieldByUnitResponse>> Handle(GetFieldByUnitRequest request, CancellationToken cancellationToken)
    {
        List<Field> fields = new List<Field>();
        var entityFields = await _unitFieldRepository.GetFieldsByEntityIdAsync(request.entityId);
        if (fields == null)
                throw new NotFoundException("FieldsByUnitId",request.entityId);
        foreach ( EntityField field in entityFields )
        {
            fields.Add(field.Field);
        }
        return _mapper.Map<List<GetFieldByUnitResponse>>(fields);
    }
}