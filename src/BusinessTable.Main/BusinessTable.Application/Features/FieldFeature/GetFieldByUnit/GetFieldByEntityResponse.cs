namespace BusinessTable.Application.Features.FieldFeature;

public sealed record GetFieldByUnitResponse
{
    public Guid id {get;set;} 
    public string displayName { get; set; }
    public int fieldTypeId { get; set; }
    public Guid? lookupEntityId { get; set; }
    public Guid? lookupEntityFieldId { get; set; }
    public bool? lookupMultipleSelection {get;set;}
}