using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed record UpdateFieldRequest( Guid id, string displayName, int fieldTypeId, Guid? lookupEntityId ,Guid? lookupEntityFieldId,
        bool? lookupMultipleSelection) : IRequest<UpdateFieldResponse>;