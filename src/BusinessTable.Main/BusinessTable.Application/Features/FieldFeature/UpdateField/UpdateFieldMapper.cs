using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class UpdateFieldMapper : Profile
{
    public UpdateFieldMapper()
    {
        CreateMap<UpdateFieldRequest,Field>()
        .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.displayName))
        .ForMember(dest => dest.FieldTypeId, opt => opt.MapFrom(src => src.fieldTypeId)) 
        .ForMember(dest => dest.LookupEntityId, opt => opt.MapFrom(src => src.lookupEntityId))
        .ForMember(dest => dest.LookupEntityFieldId, opt => opt.MapFrom(src => src.lookupEntityFieldId))
        .ForMember(dest => dest.LookupMultipleSelection, opt => opt.MapFrom(src => src.lookupMultipleSelection));
    }
}