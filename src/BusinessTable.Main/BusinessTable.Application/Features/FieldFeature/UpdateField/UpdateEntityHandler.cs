using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class UpdateFieldHandler : IRequestHandler<UpdateFieldRequest, UpdateFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IFieldRepository _fieldRepository;
    private readonly IMapper _mapper;

    public UpdateFieldHandler(IUnitOfWork unitOfWork, IFieldRepository fieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _fieldRepository = fieldRepository;
        _mapper = mapper;
    }
    
    public async Task<UpdateFieldResponse> Handle(UpdateFieldRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var field = _mapper.Map<Field>(request);
            _fieldRepository.Update(field);
            await _unitOfWork.Save(cancellationToken);
            return new UpdateFieldResponse{isSuccessful = true};
        }
        catch(Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }
    }
}