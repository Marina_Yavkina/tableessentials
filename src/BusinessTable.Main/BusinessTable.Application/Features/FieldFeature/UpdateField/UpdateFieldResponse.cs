namespace BusinessTable.Application.Features.FieldFeature;

public sealed record UpdateFieldResponse
{
    public bool isSuccessful {get;set;}
}