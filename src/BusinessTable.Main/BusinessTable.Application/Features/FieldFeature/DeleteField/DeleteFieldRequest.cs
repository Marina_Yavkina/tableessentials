using MediatR;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed record  BatchDeleteFieldRequest(List<Guid> ids) : IRequest<DeleteFieldResponse>;

public sealed record DeleteFieldRequest( Guid id) : IRequest<DeleteFieldResponse>;