using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class BatchDeleteFieldHandler : IRequestHandler<BatchDeleteFieldRequest, DeleteFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IFieldRepository _fieldRepository;
    private readonly IMapper _mapper;

    public BatchDeleteFieldHandler(IUnitOfWork unitOfWork, IFieldRepository fieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _fieldRepository = fieldRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteFieldResponse> Handle(BatchDeleteFieldRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entities = await _fieldRepository.GetListWithInclude(p => request.ids.Contains(p.Id));
            await _fieldRepository.RemoveRange(entities);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteFieldResponse{ isSuccessful = true} ;
        }
        catch (Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }

        
    }
}