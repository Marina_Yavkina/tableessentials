using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;
using BusinessTable.Application.Common.Exceptions;

namespace BusinessTable.Application.Features.FieldFeature;

public sealed class DeleteFieldHandler : IRequestHandler<DeleteFieldRequest, DeleteFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IFieldRepository _fieldRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public DeleteFieldHandler(IUnitOfWork unitOfWork, IFieldRepository fieldRepository,IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _fieldRepository = fieldRepository;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteFieldResponse> Handle(DeleteFieldRequest request, CancellationToken cancellationToken)
    {
        try
        {
            var uf =  _unitFieldRepository.GetAllAsync().Where(c => c.FieldId == request.id).ToList();
              if (uf.Count() != 0)
                throw  new ImpossibleDeleteExceptions("Невозможно удалить поле, т.к. к данное поле привязано к сущности!");
            var u = await _fieldRepository.GetAsync(request.id,true);
            if (u == null)
                throw  new NotExistException("Поле с данным идентификатором не существует!");
            _fieldRepository.Delete(request.id);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteFieldResponse{isSuccessful = true};
        }
         catch(Exception ex)
        {
            throw new BadRequestException(ex.Message);
        }

    }
}
