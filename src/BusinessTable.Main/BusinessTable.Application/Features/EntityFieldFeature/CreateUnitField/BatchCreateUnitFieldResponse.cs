namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record BatchCreateUnitFieldResponse
{
    public bool isSuccessful {get;set;}
}