using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class CreateEntityFieldMapper : Profile
{
    public CreateEntityFieldMapper()
    {
        CreateMap<EntityField, CreateUnitFieldResponse>()
        .ForMember(dest => dest.EntityId, opt => opt.MapFrom(src => src.UnitId))
        .ForMember(dest => dest.FieldId, opt => opt.MapFrom(src => src.FieldId))
        .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id)); 
        CreateMap<CreateEntityFieldRequest,EntityField>()
        .ForMember(dest => dest.UnitId, opt => opt.MapFrom(src => src.EntityId))
        .ForMember(dest => dest.FieldId, opt => opt.MapFrom(src => src.FieldId))
        .ForMember(dest => dest.CreateDate, opt => opt.Ignore())
        .ForMember(dest => dest.UpdateDate, opt => opt.Ignore())
        .ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
        .ForMember(dest => dest.DeleteDate, opt => opt.Ignore())
        .ForMember(dest => dest.Values, opt => opt.Ignore())
        .ForMember(dest => dest.Field, opt => opt.Ignore());
    }
}