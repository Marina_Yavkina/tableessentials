namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record CreateUnitFieldResponse
{
    public Guid id {get;set;} 
    public Guid EntityId {get;set;}
    public Guid FieldId {get;set;}
}