using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class CreateEntityFieldHandler : IRequestHandler<CreateEntityFieldRequest, CreateUnitFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public CreateEntityFieldHandler(IUnitOfWork unitOfWork, IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<CreateUnitFieldResponse> Handle(CreateEntityFieldRequest request, CancellationToken cancellationToken)
    {
        var entity = _mapper.Map<EntityField>(request);
        await _unitFieldRepository.AddAsync(entity);
        await _unitOfWork.Save(cancellationToken);

        return _mapper.Map<CreateUnitFieldResponse>(entity);
    }
}