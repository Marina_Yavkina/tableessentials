using MediatR;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record  BatchCreateUnitFieldRequest(List<CreateEntityFieldRequest> batchCreateEntity) : IRequest<BatchCreateUnitFieldResponse>;

public sealed record CreateEntityFieldRequest( Guid EntityId , Guid FieldId) : IRequest<CreateUnitFieldResponse>;