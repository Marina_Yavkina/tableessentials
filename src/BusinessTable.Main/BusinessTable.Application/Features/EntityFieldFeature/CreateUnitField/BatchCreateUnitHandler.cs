using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class BatchCreateEntityFieldHandler : IRequestHandler<BatchCreateUnitFieldRequest, BatchCreateUnitFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public BatchCreateEntityFieldHandler(IUnitOfWork unitOfWork, IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<BatchCreateUnitFieldResponse> Handle(BatchCreateUnitFieldRequest request, CancellationToken cancellationToken)
    {
        try {
        var entityField = _mapper.Map<List<EntityField>>(request.batchCreateEntity);
        await _unitFieldRepository.AddRange(entityField);
        await _unitOfWork.Save(cancellationToken);
        }
        catch (Exception ex)
        {
         return new BatchCreateUnitFieldResponse{ isSuccessful =false} ;
        }

        return new BatchCreateUnitFieldResponse{ isSuccessful = true} ;
    }
}