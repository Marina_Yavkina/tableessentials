using AutoMapper;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class UpdateUnitFieldMapper : Profile
{
    public UpdateUnitFieldMapper()
    {
        CreateMap<UpdateUnitFieldRequest,EntityField>()
        .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.id))
        .ForMember(dest => dest.UnitId, opt => opt.MapFrom(src => src.EntityId))
        .ForMember(dest => dest.FieldId, opt => opt.MapFrom(src => src.FieldId));; 
    }
}