using MediatR;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record UpdateUnitFieldRequest( Guid id,  Guid EntityId , Guid FieldId) : IRequest<UpdateUnitFieldResponse>;