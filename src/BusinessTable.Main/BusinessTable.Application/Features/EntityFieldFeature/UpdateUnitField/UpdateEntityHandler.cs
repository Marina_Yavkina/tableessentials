using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class UpdateEntityFieldHandler : IRequestHandler<UpdateUnitFieldRequest, UpdateUnitFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public UpdateEntityFieldHandler(IUnitOfWork unitOfWork, IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<UpdateUnitFieldResponse> Handle(UpdateUnitFieldRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entity = _mapper.Map<EntityField>(request);
            _unitFieldRepository.Update(entity);
            await _unitOfWork.Save(cancellationToken);
            return new UpdateUnitFieldResponse{isSuccessful = true};
        }
        catch
        {
            return new UpdateUnitFieldResponse{isSuccessful = false};
        }
    }
}