namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record UpdateUnitFieldResponse
{
    public bool isSuccessful {get;set;}
}