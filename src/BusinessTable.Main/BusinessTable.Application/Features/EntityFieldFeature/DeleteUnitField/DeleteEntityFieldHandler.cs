using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class DeleteEntityFieldHandler : IRequestHandler<DeleteUnitFieldRequest, DeleteUnitFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public DeleteEntityFieldHandler(IUnitOfWork unitOfWork, IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteUnitFieldResponse> Handle(DeleteUnitFieldRequest request, CancellationToken cancellationToken)
    {
        try
        {
            _unitFieldRepository.Delete(request.id);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteUnitFieldResponse{isSuccessful = true};
        }
         catch
        {
            return new DeleteUnitFieldResponse{isSuccessful = false};
        }

    }
}