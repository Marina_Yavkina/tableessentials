using MediatR;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record  BatchDeleteUnitFieldRequest(List<Guid> ids) : IRequest<DeleteUnitFieldResponse>;

public sealed record DeleteUnitFieldRequest( Guid id) : IRequest<DeleteUnitFieldResponse>;