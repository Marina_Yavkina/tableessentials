using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;
using BusinessTable.Domain.Entities;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class BatchDeleteEntityFieldHandler : IRequestHandler<BatchDeleteUnitFieldRequest, DeleteUnitFieldResponse>
{
    private readonly IUnitOfWork _unitOfWork;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IMapper _mapper;

    public BatchDeleteEntityFieldHandler(IUnitOfWork unitOfWork, IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitOfWork = unitOfWork;
        _unitFieldRepository = unitFieldRepository;
        _mapper = mapper;
    }
    
    public async Task<DeleteUnitFieldResponse> Handle(BatchDeleteUnitFieldRequest request, CancellationToken cancellationToken)
    {
        try 
        {
            var entities = await _unitFieldRepository.GetListWithInclude(p => request.ids.Contains(p.Id));
            await _unitFieldRepository.RemoveRange(entities);
            await _unitOfWork.Save(cancellationToken);
            return new DeleteUnitFieldResponse{ isSuccessful = true} ;
        }
        catch (Exception ex)
        {
            return new DeleteUnitFieldResponse{ isSuccessful =false} ;
        }

        
    }
}