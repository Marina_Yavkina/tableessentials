using MediatR;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record GetUnitFieldRequest(Guid entity_id) : IRequest<List<GetUnitFieldResponse>>;