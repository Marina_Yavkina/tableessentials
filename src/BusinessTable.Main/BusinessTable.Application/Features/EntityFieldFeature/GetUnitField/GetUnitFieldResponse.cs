namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed record GetUnitFieldResponse
{
    public Guid id {get;set;} 
    public string entityName { get; set; }
    public Guid entityId {get;set;}
    public string fieldName { get; set; }
    public Guid fieldId {get;set;}
    public bool isTaskconnected {get;set;}
     public int fieldTypeId { get; set; }
    public Guid? lookupEntityId { get; set; }
    public Guid? lookupEntityFieldId { get; set; }
    public bool? lookupMultipleSelection {get;set;}


}