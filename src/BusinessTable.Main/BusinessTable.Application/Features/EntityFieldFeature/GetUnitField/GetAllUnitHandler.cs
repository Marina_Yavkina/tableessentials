using AutoMapper;
using BusinessTable.Application.Repositories;
using MediatR;

namespace BusinessTable.Application.Features.UnitFieldFeature;

public sealed class GetEntityFieldHandler : IRequestHandler<GetUnitFieldRequest, List<GetUnitFieldResponse>>
{
    private readonly IUnitRepository _unitRepository;
    private readonly IUnitFieldRepository _unitFieldRepository;
    private readonly IFieldRepository _fieldRepository;
    private readonly IMapper _mapper;

    public GetEntityFieldHandler(IUnitRepository unitRepository,IFieldRepository fieldRepository, IUnitFieldRepository unitFieldRepository, IMapper mapper)
    {
        _unitRepository = unitRepository;
        _unitFieldRepository = unitFieldRepository;
        _fieldRepository = fieldRepository;
        _mapper = mapper;
    }
    
    public async Task<List<GetUnitFieldResponse>> Handle(GetUnitFieldRequest request, CancellationToken cancellationToken)
    {
        var entityFields = await _unitFieldRepository.GetFieldsByEntityIdAsync(request.entity_id);
        var query1 =
            from field in  _fieldRepository.GetAllAsync().AsEnumerable()
            join entityField in entityFields
            on field.Id equals entityField.FieldId
            select new {
                id = entityField.Id,
                unitId = entityField.UnitId,
                fieldName = field.DisplayName,
                fieldId = field.Id,
                fieldTypeId = field.FieldTypeId,
                lookupEntityId = field.LookupEntityId,
                lookupEntityFieldId = field.LookupEntityFieldId,
                lookupMultipleSelection = field.LookupMultipleSelection
             };


   var eflist = query1.ToList();

            var query = 
                from ef in eflist
                join entity in  _unitRepository.GetAllAsync() on ef.unitId equals entity.Id 
                select new GetUnitFieldResponse
                { 
                    id = ef.id,
                    entityName = entity.DisplayName,
                    entityId = entity.Id,
                    fieldName = ef.fieldName,
                    fieldId = ef.fieldId,
                    isTaskconnected = entity.isTaskConnnected,
                    fieldTypeId = ef.fieldTypeId,
                    lookupEntityId = ef.lookupEntityId,
                    lookupEntityFieldId = ef.lookupEntityFieldId,
                    lookupMultipleSelection = ef.lookupMultipleSelection
                    
                };
        
            // join entity in  _unitRepository.GetAllAsync() on entityField.UnitId equals entity.Id            
            // select new GetUnitFieldResponse
            // { 
            //     id = entityField.Id,
            //     entityName = entity.DisplayName,
            //     entityId = entity.Id,
            //     fieldName = field.DisplayName,
            //     fieldId = field.Id,
            //     isTaskconnected = entity.isTaskConnnected,
            //     fieldTypeId = field.FieldTypeId,
            //     lookupEntityId = field.LookupEntityId,
            //     lookupEntityFieldId = field.LookupEntityFieldId,
            //     lookupMultipleSelection = field.LookupMultipleSelection
                
            // };
        return query.ToList();
    }
}