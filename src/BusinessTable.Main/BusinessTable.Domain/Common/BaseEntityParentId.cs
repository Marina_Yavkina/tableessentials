namespace BusinessTable.Domain.Common;

/// <summary>
/// Интерфейс сущности с идентификатором
/// </summary>
/// <typeparam name="TId">Тип идентификатора</typeparam>
public interface IParentEntity<TId,K> : IEntity<TId> 
{
    /// <summary>
    /// Идентификатор
    /// </summary>
    K? ParentId { get; set; }
}