namespace BusinessTable.Domain.Common.Constants;
public enum DataFieldType : int
{
    Simple = 4,
    DictionaryId = 5,
    PivotColumn = 6
}

public enum DataType : int
{
    Cost = 0,
    Date = 1,
    Float = 2,
    Bool = 3,
    Text = 4,
    Integer = 5
}
public enum DisplayToSource
{
    RESOURCE ,
    PROJECT ,
    TASK 
}