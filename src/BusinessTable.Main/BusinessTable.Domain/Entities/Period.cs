using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class Period: BaseEntity
    {
        public string Name {get;set;}
        public virtual List<PeriodValue> PeriodValues {get;set;}
    }
}