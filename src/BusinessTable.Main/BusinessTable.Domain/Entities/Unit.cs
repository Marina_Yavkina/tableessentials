using System;
using BusinessTable.Domain.Base;

namespace BusinessTable.Domain.Entities
{
    public class Unit: BaseEntity
    {
       
        public string DisplayName { get; set; }
        public string Definiton {get;set;}
        public bool isTaskConnnected {get;set;}
        public virtual List<EntityField>? EntityFields { get; set; }
        public virtual List<Item>? Items { get; set; }
        public virtual List<View>? Views { get; set; }
        //public virtual List<Period>? Periods {get; set; }
    }
}