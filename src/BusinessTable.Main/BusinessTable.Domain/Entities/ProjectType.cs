using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class ProjectType : BaseEntity
    {
        public string Name { get; set; }
        public virtual List<Task> Tasks { get; set; }
    }
}