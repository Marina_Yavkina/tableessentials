using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class Item: ParentBaseEntity
    {
        //public Guid EntityId {get;set;}
        public Guid UnitId {get;set;}
        ///public virtual EntityUnit Entity {get;set;}
        public virtual Unit Unit {get;set;}
        public virtual Task? Task {get;set;}
         public Guid TaskId {get;set;}
        public virtual Item? ParentItem {get;set;}     
        public virtual List<Item>? SubItems {get;set;}   
        public virtual List<Value>? Values {get;set;}  

    }
}