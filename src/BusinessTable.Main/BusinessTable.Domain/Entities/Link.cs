using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class Link: BaseEntity
    {
        public string Type { get; set; }
        public Guid SourceTaskId { get; set; }
        public Guid TargetTaskId { get; set; }
        public virtual Task SourceTask {get;set;}
        public virtual Task TargetTask {get;set;}
        
    }
}