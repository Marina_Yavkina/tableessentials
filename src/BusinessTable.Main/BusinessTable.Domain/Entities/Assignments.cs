using System;
using BusinessTable.Domain.Base;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class Assignment: BaseEntity
    {
        public string UserId { get; set; }
        public string Value { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Delay {get; set;}
        public int? Duration {get; set;}
        public string? Mode {get;set;}
        public string? Unit {get;set;}
        public Guid TaskId {get;set;}
        public virtual Task? Task { get; set; }
    }
}
