namespace BusinessTable.Domain.Entities
{
    public class LookupField{
        public Guid? LookupEntityId { get; set; }
        public Guid? LookupEntityFieldId { get; set; }
        public bool? LookupMultipleSelection {get;set;}
    }
}