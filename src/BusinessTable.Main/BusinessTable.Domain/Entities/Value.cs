using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class Value: BaseEntity
    {
        public Guid ItemId {get;set;}
        public virtual Item Item {get;set;} 
        public DateTime? DateData{get;set;}
        public bool? BoolData {get;set;}
        public string? Text {get;set;}
        public float? NumberData{ get;set;}
        public int? IntegerData{get;set;}
        public virtual PeriodValue? PeriodValue {get;set;}
        public virtual List<SelectedDictionaryItem>? SelectedDictionaryItems {get;set;}
        public virtual EntityField EntityField {get;set;}
        public Guid EntityFieldId {get;set;}

    }
}