using System;
using BusinessTable.Domain.Base;

namespace BusinessTable.Domain.Entities
{
    public class ViewField: BaseEntity
    {       
        public string Name { get; set; }
        public int SortOrder { get; set; }
        public Guid FieldId {get; set;}
        public int? DistributionOrder {get; set;}        
        public Guid ViewId {get; set;} 
        public virtual View View {get;set;}
        public int? FieldWidth {get; set;}  
        public int? FieldIndex {get; set;} 
      
    }
}