﻿using BusinessTable.Domain.Common;
using System;

namespace BusinessTable.Domain.Base
{
    public class BaseEntity : IEntity<Guid>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Удаленная запись доступна только для чтения
        /// </summary>
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        /// <summary>
        /// Дата изменения
        /// </summary>
        public DateTime? UpdateDate { get; set; }
        /// <summary>
        /// Дата удаления
        /// </summary>
        public DateTime? DeleteDate { get; set; }
    }
}
