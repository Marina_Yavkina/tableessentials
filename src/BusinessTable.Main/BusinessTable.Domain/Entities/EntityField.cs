using System;
using BusinessTable.Domain.Base;

namespace BusinessTable.Domain.Entities
{
    public class EntityField: BaseEntity
    {
        public Guid UnitId {get;set;}
        public virtual  Unit  Unit { get; set; }
        public Guid FieldId {get;set;}
        public virtual  Field  Field { get; set; }
        public virtual List<ViewField> ViewFields {get; set;}
        public virtual List<Value> Values {get; set;}
        
    }
}