using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;
using BusinessTable.Domain.Common;

namespace BusinessTable.Domain.Entities
{
    public class ProjectTypeField : BaseEntity
    {
        public Guid ProjectTypeId {get;set;}
        public virtual  ProjectType  ProjectType { get; set; }
        
        public Guid FieldId {get;set;}
        public virtual  Field  Field { get; set; }
        public DisplayTo DisplayTo {get;set;}

    }
}