using System;
using BusinessTable.Domain.Base;

namespace BusinessTable.Domain.Entities
{
    public class Field: BaseEntity
    {
       
        public string DisplayName { get; set; }
        public int FieldTypeId { get; set; }
        public int DataTypeId { get; set; }
        public int FieldOrder { get; set; }
        public Guid? LookupEntityId { get; set; }
        public Guid? LookupEntityFieldId { get; set; }
        public bool? LookupMultipleSelection {get;set;}
        public virtual List<EntityField> EntityFields { get; set; }
        
    }
}