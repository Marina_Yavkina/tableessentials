using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace  BusinessTable.Domain.Entities
{
    public class Task: ParentBaseEntity
    {
        public string Text { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? BaseStart { get; set; }
        public DateTime? BaseEnd { get; set; }
        public int Duration { get; set; }
        public decimal Progress { get; set; }
        public string Type { get; set; }
         public bool? Open {
            get {return true;}
            set { }
        }
        public string? Priority{ get; set;}
        public virtual Task? ParentTask {get;set;}     
        public virtual List<Task>? SubTasks {get;set;}    
        public virtual  List<Assignment>  Assignments { get; set; }
        public Guid? ProjectTypeId {get;set;}
        public virtual ProjectType? ProjectType {get; set;}
         public virtual List<Item>? Items { get; set; }
    }
}