using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class View: BaseEntity
    {
       
        public string DisplayName { get; set; }
       // public Guid EntityId { get; set; }
       //public virtual  EntityUnit  Entity { get; set; }
        public Guid UnitId { get; set; }
        public virtual  Unit  Unit { get; set; }
        public virtual List<ViewField> ViewFields { get; set; }
    }
}
