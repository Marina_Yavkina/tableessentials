using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class PeriodValue: BaseEntity
    {
        public Guid PeriodId {get;set;}
        public virtual Period Period {get;set;} 
        public Guid ValueId {get;set;}
        public virtual Value Value {get;set;}
        public DateTime Value_dt {get;set;}
    }
}