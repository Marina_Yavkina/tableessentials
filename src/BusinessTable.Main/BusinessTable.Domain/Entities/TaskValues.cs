using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class TaskValue: BaseEntity
    {
        public Guid TaskId {get;set;}
        public virtual Task Task { get; set; }
        public Guid? ItemId {get;set;}
        public virtual Item? Item { get; set; }
        public Guid? ValueId {get;set;}
        public virtual Value? Value { get; set; }
            
        public string? TextData {get;set;}
        public double? NumericData {get;set;}
        public bool? BoolData {get;set;}
       
    }
}