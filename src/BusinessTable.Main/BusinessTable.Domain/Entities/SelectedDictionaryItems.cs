using System;
using BusinessTable.Domain.Base;
using System.Collections;
using System.Collections.Generic;

namespace BusinessTable.Domain.Entities
{
    public class SelectedDictionaryItem: BaseEntity
    {
        public Guid ValueId {get;set;}
        public virtual Value Value {get;set;}  
        public Guid SelectedItemId {get;set;}
    }
}