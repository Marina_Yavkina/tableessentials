using Microsoft.AspNetCore.Builder;
 
namespace BusinessTable.WebAPI.MiddleWareSettings
{
    public static class CustomMiddlewareExtensions
    {
        public static IApplicationBuilder UseJwtTimeValidator(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JwtTimeValidator>();
        }
    }
}