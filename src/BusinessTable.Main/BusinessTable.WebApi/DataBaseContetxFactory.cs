using BusinessTable.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

public class DataBaseContextFactory : IDesignTimeDbContextFactory<DataContext>
{      

    DataContext IDesignTimeDbContextFactory<DataContext>.CreateDbContext(string[] args)
    {
        IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .Build();

        var builder = new DbContextOptionsBuilder<DataContext>();
        var connectionString = configuration.GetConnectionString("BusinessTable");
        builder.UseNpgsql(connectionString,b => b.MigrationsAssembly("BusinessTable.WebApi")).EnableSensitiveDataLogging();

        return new DataContext(builder.Options);
    }
}