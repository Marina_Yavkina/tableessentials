using BusinessTable.Persistence.Constants;
namespace BusinessTable.WebAPI.Extensions;

public static class CorsPolicyExtensions
{
    
    public static void ConfigureCorsPolicy(this IServiceCollection services)
    {
        services.AddCors(opt =>
        {
            opt.AddPolicy(AppConstants.CorsAllowedOrigins, builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
        });
    }
}

