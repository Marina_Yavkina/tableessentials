using BusinessTable.Persistence.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Serilog;
using Microsoft.OpenApi.Models;
using BusinessTable.WebAPI.Extensions;
using BusinessTable.Application;
using BusinessTable.Persistence.Context;
using BusinessTable.WebHost.Extensions;
using BastionGantt.Common.Extensions;
using BusinessTable.WebAPI.MiddleWareSettings;
using BusinessTable.Persistence.Constants;
using MassTransit;
using BusinessTable.WebAPI.Settings;
using SharedModel;
using BusinessTable.Consumers;

try
{
    var builder = WebApplication.CreateBuilder(args);
    builder.Environment.ApplicationName = typeof(Program).Assembly.FullName;
// Add services to the container.
 var connectionString = builder.Configuration.GetConnectionString("BusinessTable");
// builder.Services.AddDbContext<DataContext>(options => options.UseLazyLoadingProxies()
//                                                              .UseNpgsql(connectionString,b => b.MigrationsAssembly("BusinessTable.WebApi")),
//                                                               ServiceLifetime.Scoped);
builder.Services.ConfigurePersistence(builder.Configuration);
builder.Services.ConfigureApplication();
builder.Services.ConfigureCorsPolicy();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddCustomJWTAuthentification();
builder.Services.AddControllers();
builder.Services.AddMassTransit(x =>
{
    x.AddConsumer<DictionaryConsumer>();

    x.AddBus(context => Bus.Factory.CreateUsingRabbitMq(c =>
    {
        var rabbitMQSettings = builder.Configuration.GetSection(nameof(RabbitMQSettings)).Get<RabbitMQSettings>();
        c.Host(rabbitMQSettings.Host);
        c.ReceiveEndpoint(rabbitMQSettings.Queue!, e =>
        {
            e.PrefetchCount = 16;
            e.UseMessageRetry(r => r.Interval(2, 3000));
            e.ConfigureConsumer<DictionaryConsumer>(context);
        });
    }));
});
//builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(option =>
{
    option.SwaggerDoc("v1", new OpenApiInfo { Title = "Demo API", Version = "v1" });
    option.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please enter a valid token",
        Name = "Authorization",
        Type = SecuritySchemeType.Http,
        BearerFormat = "JWT",
        Scheme = "Bearer"
    });
    option.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type=ReferenceType.SecurityScheme,
                    Id="Bearer"
                }
            },
            new string[]{}
        }
    });
});


builder.Services.AddHttpContextAccessor();  
var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json","Gantt Application"));
    app.UseDeveloperExceptionPage();
    
}
    app.UseMiddleware<ExceptionHandlerMiddleware>();
    app.UseCors(AppConstants.CorsAllowedOrigins);
    //var app = WebApplicationConfiguration.Configure(builder);
    app.UseAuthentication();
    app.UseJwtTimeValidator();
    app.UseHttpsRedirection();
    app.UseAuthorization();
    app.MapControllers();

    Log.Logger.Information($"The {app.Environment.ApplicationName} started...");
    app.Run();

    return 0;
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly!");
    return 1;
}
finally
{
    Log.CloseAndFlush();
}