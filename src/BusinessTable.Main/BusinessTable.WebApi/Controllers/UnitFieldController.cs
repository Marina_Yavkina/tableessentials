using BusinessTable.Application.Features.UnitFieldFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BusinessTable.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UnitFieldController : ControllerBase
{
    private readonly IMediator _mediator;

    public UnitFieldController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Return list of Fields 
    /// <param name="id">идендификатор юнита</param>
    /// </summary>
    /// <returns></returns>
     [HttpGet("[action]/{id}")]
    public async Task<ActionResult<List<GetUnitFieldResponse>>> GetAll(Guid id, CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(new GetUnitFieldRequest(id), cancellationToken);
        return Ok(response);
    }
    
    /// <summary>
    /// Return added Field
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<CreateUnitFieldResponse>> Create(BatchCreateUnitFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// Return success or not
    /// </summary>
    /// <returns></returns>
    [HttpPut]
    public async Task<ActionResult<CreateUnitFieldResponse>> Update(BatchCreateUnitFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// удалить Field
    /// </summary>
    /// <returns></returns>
    [HttpDelete] 
    public async Task<ActionResult<DeleteUnitFieldResponse>> Delete(DeleteUnitFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }

     [HttpPost("batch")]
      public async Task<ActionResult<BatchCreateUnitFieldResponse>> BatchCreate(BatchCreateUnitFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    
}