using BusinessTable.Application.Features.UnitFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BusinessTable.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class UnitController : ControllerBase
{
    private readonly IMediator _mediator;

    public UnitController(IMediator mediator)
    {
        _mediator = mediator;
    }
      /// <summary>
        /// Return list of Units 
        /// </summary>
        /// <returns></returns>

    [HttpGet]
    public async Task<ActionResult<List<GetAllUnitResponse>>> GetAll(CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(new GetAllUnitRequest(), cancellationToken);
        return Ok(response);
    }
    
    /// <summary>
    /// Return added Unit
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<CreateUnitResponse>> Create(CreateUnitRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// Return success or not
    /// </summary>
    /// <returns></returns>
    [HttpPut]
    public async Task<ActionResult<UpdateUnitResponse>> Update(UpdateUnitRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// удалить сущность
    /// </summary>
    /// <returns></returns>
    [HttpDelete] 
    public async Task<ActionResult<DeleteUnitResponse>> Delete(DeleteUnitRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    
}