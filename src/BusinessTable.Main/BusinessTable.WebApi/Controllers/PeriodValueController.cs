using BusinessTable.Application.Features.PeriodValueFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BusinessTable.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PeriodValueController : ControllerBase
{
    private readonly IMediator _mediator;

    public PeriodValueController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Return added Value
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<CreatePeriodValueResponse>> Create(PeriodValueCreateRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// Return success or not
    /// </summary>
    /// <returns></returns>
    [HttpPut]
    public async Task<ActionResult<PeriodValueUpdateResponse>> Update(PeriodValueUpdateRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// удалить Item
    /// </summary>
    /// <returns></returns>
    [HttpDelete] 
    public async Task<ActionResult<DeletePeriodValueResponse>> Delete(DeletePeriodValueRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }

     [HttpPost("batch")]
      public async Task<ActionResult<BatchCreatePeriodValueResponse>> BatchCreate(BatchCreatePeriodValueRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    
}