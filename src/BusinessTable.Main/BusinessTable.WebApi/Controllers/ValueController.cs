using BusinessTable.Application.Features.ValueFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BusinessTable.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ValueController : ControllerBase
{
    private readonly IMediator _mediator;

    public ValueController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Return added Value
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<CreateItemValueResponse>> Create(CreateItemValueRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// Return success or not
    /// </summary>
    /// <returns></returns>
    [HttpPut]
    public async Task<ActionResult<UpdateItemValueResponse>> Update(UpdateItemValueRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// удалить Item
    /// </summary>
    /// <returns></returns>
    [HttpDelete] 
    public async Task<ActionResult<DeleteValueResponse>> Delete(DeleteValueRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }

     [HttpPost("batch")]
      public async Task<ActionResult<BatchCreateValueResponse>> BatchCreate(BatchCreateValueRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    
}