using BusinessTable.Application.Features.ItemFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using BusinessTable.Persistence.Constants;
using Microsoft.AspNetCore.Cors;

namespace BusinessTable.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
[EnableCors(AppConstants.CorsAllowedOrigins)]
public class ItemController : ControllerBase
{
    private readonly IMediator _mediator;

    public ItemController(IMediator mediator)
    {
        _mediator = mediator;
    }
    /// <summary>
    /// Return list of Fields 
    /// <param name="id">идендификатор юнита</param>
    /// </summary>
    /// <returns>экземпляры и значния по идентификатору юнита</returns>
    [HttpGet("[action]/{id}")]
    public async Task<ActionResult<List<GetItemValueRequest>>> GetAll(Guid id, CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(new GetItemValueRequest(id), cancellationToken);
        return Ok(response);
    }
    
    /// <summary>
    /// Return added Field
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<CreateItemResponse>> Create(CreateItemRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// Return success or not
    /// </summary>
    /// <returns></returns>
    [HttpPut]
    public async Task<ActionResult<CreateItemResponse>> Update(UpdateItemRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// удалить Item
    /// </summary>
    /// <returns></returns>
    [HttpDelete] 
    public async Task<ActionResult<DeleteItemResponse>> Delete(DeleteItemRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }

     [HttpPost("batch")]
      public async Task<ActionResult<BatchCreateItemResponse>> BatchCreate(BatchCreateItemRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    
}