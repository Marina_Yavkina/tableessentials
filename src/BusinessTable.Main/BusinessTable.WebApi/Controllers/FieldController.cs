using BusinessTable.Application.Features.FieldFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace BusinessTable.WebAPI.Controllers;

[Route("api/[controller]")]
[ApiController]
public class FieldController : ControllerBase
{
    private readonly IMediator _mediator;

    public FieldController(IMediator mediator)
    {
        _mediator = mediator;
    }
      /// <summary>
        /// Return list of Fields 
        /// </summary>
        /// <returns></returns>

    [HttpGet]
    public async Task<ActionResult<List<GetAllFieldResponse>>> GetAll(CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(new GetAllFieldRequest(), cancellationToken);
        return Ok(response);
    }
    
    /// <summary>
    /// Return added Field
    /// </summary>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<CreateFieldResponse>> Create(CreateFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// Return success or not
    /// </summary>
    /// <returns></returns>
    [HttpPut]
    public async Task<ActionResult<UpdateFieldResponse>> Update(UpdateFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    /// <summary>
    /// удалить Field
    /// </summary>
    /// <returns></returns>
    [HttpDelete] 
    public async Task<ActionResult<DeleteFieldResponse>> Delete(DeleteFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }

     [HttpPost("batch")]
      public async Task<ActionResult<BatchCreateFieldResponse>> BatchCreate([FromBody] BatchCreateFieldRequest request,
        CancellationToken cancellationToken)
    {
        var response = await _mediator.Send(request, cancellationToken);
        return Ok(response);
    }
    
}