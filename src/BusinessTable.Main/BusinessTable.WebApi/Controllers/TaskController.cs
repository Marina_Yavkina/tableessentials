﻿using BusinessTable.Application.Features.TaskFeature;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using BusinessTable.Persistence.Constants;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;
using BusinessTable.WebAPI.Models;

namespace BusinessTable.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors(AppConstants.CorsAllowedOrigins)]
    public class TaskController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TaskController(IMediator mediator)
        {
            _mediator = mediator;
        }

        /// <summary>
        /// Получение списка задач
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>
        // [AllowAnonymous]
        // [HttpGet("[action]/{pageNumber}:{pageSize}")]
        // public async Task<IActionResult> GetPage(int pageNumber = 1, int pageSize = 6)
        // {
        //     var pagedList = await _service.GetPage(pageNumber, pageSize);

        //     return Ok(new ResponseWrapper<List<TaskViewModel>>(pagedList.Entities, pagedList.Pagination));
        // }

         /// <summary>
        /// Получение списка проектов
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>
        [HttpGet("[action]/projects/{pageNumber}:{pageSize}")]

        public async Task<ActionResult<ResponseWrapper<List<GetTaskViewProjects>>>> GetProjects(CancellationToken cancellationToken,int pageNumber = 1, int pageSize = 6)
        {
            var pagedList = await _mediator.Send(new GetProjectsRequest(pageNumber,pageSize) ,cancellationToken);
             return Ok(new ResponseWrapper<List<GetTaskViewProjects>>(pagedList.Entities, pagedList.Pagination));
        }
         /// <summary>
        /// Получение списка задач по проекту
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>List<TaskViewModel></returns>

        [AllowAnonymous]
        [HttpGet("[action]/projects/{pageNumber}:{pageSize}:{id}")]
        public async  Task<ActionResult<ResponseWrapper<List<GetTaskViewResponse>>>> GetTasksByProject(CancellationToken cancellationToken,string id,int pageNumber = 1, int pageSize = 6)
        {
            var pagedList = await _mediator.Send(new GetTaskRequest(new Guid(id),  pageNumber,pageSize) ,cancellationToken);

            return Ok(new ResponseWrapper<List<GetTaskViewResponse>>(pagedList.Entities, pagedList.Pagination));
        }

        /// <summary>
        /// Получение информации о задаче
        /// </summary>
        /// <param name="pageNumber">номер страницы</param>
        /// <param name="pageSize">количество</param>
        /// <returns>TaskDto</returns>
        // [AllowAnonymous]
        // [HttpGet("[action]/{id}")]
        // public async Task<IActionResult> Get(Guid id)
        // {
        //     var taskDto = await _service.Get(id);
        //     return Ok(taskDto);
        // }

        /// <summary>
        /// Добавить фитнес клуб
        /// </summary>
        /// <param name="request">информация о клубе</param>
        /// <returns>идентификатор клуба</returns>
       [HttpPost("[action]")]
        public async Task<ActionResult<CreateTaskResponse>> Create(CreateTaskRequest request,
        CancellationToken cancellationToken)
        {
            var response = await _mediator.Send(request, cancellationToken);
            return Ok(response);
        }
        /// <summary>
        /// Отредактировать информацию фитнес клуба
        /// </summary>
        /// <param name="id">идендификатор клуба</param>
        /// <param name="request">информация о клубе</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<UpdateTaskResponse>> UpdateTask(UpdateTaskRequest request, CancellationToken cancellationToken)
        {
            var response = await _mediator.Send(request, cancellationToken);
            return Ok(response);           
        }

        /// <summary>
        /// Поместить клуб в архив
        /// </summary>
        /// <param name="id">идентификатор клуба</param>
        /// <returns></returns>
        [HttpDelete("[action]/{id}")] 
        public async  Task<ActionResult<RemoveTaskResponse>> DeleteTask(RemoveTaskRequest request,
        CancellationToken cancellationToken)
        {
            var response = await _mediator.Send(request, cancellationToken);
            return Ok(response);
        }
       
    }
}
