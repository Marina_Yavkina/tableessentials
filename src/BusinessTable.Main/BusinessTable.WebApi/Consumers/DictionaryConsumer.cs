using MassTransit;
using SharedModel;
using BusinessTable.Application.Repositories;
using BusinessTable.Domain.Entities;
using System.Threading;
using A=System.Threading.Tasks;
using BusinessTable.Persistence.Context;

namespace BusinessTable.Consumers {
    public class DictionaryConsumer : IConsumer<DictListContract>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUnitRepository _unitRepository;
        private readonly IFieldRepository _fieldRepository;
        private readonly IUnitFieldRepository _unitFieldRepository;
        private  AutoResetEvent autoreseteventDataWait;
        public DictionaryConsumer( IUnitRepository unitRepository,IUnitOfWork unitOfWork, IFieldRepository fieldRepository, IUnitFieldRepository unitFieldRepository)
        {
            _unitRepository = unitRepository;
            _fieldRepository = fieldRepository;
            _unitFieldRepository = unitFieldRepository;
            _unitOfWork = unitOfWork;
           autoreseteventDataWait = new AutoResetEvent(false);
            
          
        }
        public async A.Task Consume(ConsumeContext<DictListContract> context)
        {
            var tokenSource = new CancellationTokenSource();
            // foreach (DictContract item in context.Message.dictionary)
            // {
            //     var unit = _unitRepository.GetAllAsync().Where(g => g.DisplayName == item.entity).FirstOrDefault();
            //     if (unit == null)
            //     {
            //         unit = await _unitRepository.AddAsync(new Unit{DisplayName = item.entity,Definiton = item.entity, isTaskConnnected = false }); 
            //     }
            //     var field = _fieldRepository.GetAllAsync().Where(g => g.DisplayName == item.field).FirstOrDefault();
            //     if (field == null)
            //     {
            //         Field f = new Field();
            //         f.DisplayName = item.field;
            //         if (item.type.simple_type == "integer") 
            //         {
            //             f.FieldTypeId = 1;
            //             var new_field = await _fieldRepository.AddAsync(f); 
            //         }
            //         else if( item.type.simple_type == "string" )
            //         {
            //             f.FieldTypeId = 4;
            //             var new_field = await _fieldRepository.AddAsync(f); 
            //         }
                    
            //     }
            //     await _unitOfWork.Save(tokenSource.Token);
            // }
          
            Console.Write(context.Message);           
            // Task.Run(() => 
            //     {
            //         using (var context = new DataContext())
            //         {
            //             InsertDictionaryToDb(context.Message,autoreseteventDataWait, tokenSource.Token);
            //         }
            //     });
            A.Task.Factory.StartNew(() => { InsertDictionaryToDb(context.Message,autoreseteventDataWait, tokenSource.Token);});
            autoreseteventDataWait.WaitOne();



        }
        private  async void InsertDictionaryToDb( DictListContract source,object stateInfo, CancellationToken cancellationToken)
        {
           // using (var context = new DataContext())
                    //{
            foreach (DictContract item in source.dictionary)
            {
               // var unit1 =  context.Units.AsQueryable().Where(g => g.DisplayName == item.entity).FirstOrDefault();
                
                var unit =  _unitRepository.GetAllAsync().Where(g => g.DisplayName == item.entity).FirstOrDefault();
                if (unit == null)
                {
                    unit = await _unitRepository.AddAsync(new Unit{DisplayName = item.entity,Definiton = item.entity, isTaskConnnected = false }); 
                }
                var field = _fieldRepository.GetAllAsync().Where(g => g.DisplayName == item.field).FirstOrDefault();
                if (field == null)
                {
                    Field f = new Field();
                    f.DisplayName = item.field;
                    if (item.type.simple_type == "integer") 
                    {
                        f.FieldTypeId = 1;
                        var new_field = await _fieldRepository.AddAsync(f); 
                    }
                    else if( item.type.simple_type == "string" )
                    {
                        f.FieldTypeId = 4;
                        var new_field = await _fieldRepository.AddAsync(f); 
                    }
                    
                }
                await _unitOfWork.Save(cancellationToken);
            }

            ((AutoResetEvent)stateInfo).Set();

            //}
        }

    }
}