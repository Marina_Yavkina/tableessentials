﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace BusinessTable.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class InitialCreate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "Fields",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    DisplayName = table.Column<string>(type: "character varying(256)", unicode: false, nullable: false),
                    FieldTypeId = table.Column<int>(type: "integer", nullable: false),
                    DataTypeId = table.Column<int>(type: "integer", nullable: false),
                    FieldOrder = table.Column<int>(type: "integer", nullable: false),
                    LookupEntityId = table.Column<Guid>(type: "uuid", nullable: true),
                    LookupEntityFieldId = table.Column<Guid>(type: "uuid", nullable: true),
                    LookupMultipleSelection = table.Column<bool>(type: "boolean", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Fields", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Periods",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    Name = table.Column<string>(type: "character varying(256)", unicode: false, nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Periods", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    DisplayName = table.Column<string>(type: "character varying(256)", unicode: false, nullable: false),
                    Definiton = table.Column<string>(type: "character varying(256)", unicode: false, nullable: true),
                    isTaskConnnected = table.Column<bool>(type: "boolean", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "EntityFields",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    UnitId = table.Column<Guid>(type: "uuid", nullable: false),
                    FieldId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EntityFields", x => x.Id);
                    table.ForeignKey(
                        name: "FK_EntityFields_Fields_FieldId",
                        column: x => x.FieldId,
                        principalTable: "Fields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_EntityFields_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    UnitId = table.Column<Guid>(type: "uuid", nullable: false),
                    ParentId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Items_Items_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Items",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Items_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "View",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    DisplayName = table.Column<string>(type: "text", nullable: false),
                    UnitId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_View", x => x.Id);
                    table.ForeignKey(
                        name: "FK_View_Units_UnitId",
                        column: x => x.UnitId,
                        principalTable: "Units",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Values",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    DateData = table.Column<DateTime>(type: "timestamp", nullable: true),
                    BoolData = table.Column<bool>(type: "boolean", nullable: true),
                    Text = table.Column<string>(type: "character varying(1024)", unicode: false, nullable: true),
                    NumberData = table.Column<float>(type: "numeric(30,6)", nullable: true),
                    IntegerData = table.Column<int>(type: "integer", nullable: true),
                    EntityFieldId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Values", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Values_EntityFields_EntityFieldId",
                        column: x => x.EntityFieldId,
                        principalTable: "EntityFields",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Values_Items_ItemId",
                        column: x => x.ItemId,
                        principalTable: "Items",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ViewField",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    SortOrder = table.Column<int>(type: "integer", nullable: false),
                    FieldId = table.Column<Guid>(type: "uuid", nullable: false),
                    DistributionOrder = table.Column<int>(type: "integer", nullable: true),
                    ViewId = table.Column<Guid>(type: "uuid", nullable: false),
                    FieldWidth = table.Column<int>(type: "integer", nullable: true),
                    FieldIndex = table.Column<int>(type: "integer", nullable: true),
                    EntityFieldId = table.Column<Guid>(type: "uuid", nullable: true),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    UpdateDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ViewField", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ViewField_EntityFields_EntityFieldId",
                        column: x => x.EntityFieldId,
                        principalTable: "EntityFields",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_ViewField_View_ViewId",
                        column: x => x.ViewId,
                        principalTable: "View",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PeriodValues",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    PeriodId = table.Column<Guid>(type: "uuid", nullable: false),
                    ValueId = table.Column<Guid>(type: "uuid", nullable: false),
                    Value_dt = table.Column<DateTime>(type: "timestamp", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PeriodValues", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PeriodValues_Periods_PeriodId",
                        column: x => x.PeriodId,
                        principalTable: "Periods",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PeriodValues_Values_ValueId",
                        column: x => x.ValueId,
                        principalTable: "Values",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SelectedDictionaryItems",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false, defaultValueSql: "uuid_generate_v4()"),
                    ValueId = table.Column<Guid>(type: "uuid", nullable: false),
                    SelectedItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    IsDeleted = table.Column<bool>(type: "boolean", nullable: false, defaultValue: false),
                    CreateDate = table.Column<DateTime>(type: "timestamp", nullable: false, defaultValueSql: "now()"),
                    UpdateDate = table.Column<DateTime>(type: "timestamp", nullable: true),
                    DeleteDate = table.Column<DateTime>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SelectedDictionaryItems", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SelectedDictionaryItems_Values_ValueId",
                        column: x => x.ValueId,
                        principalTable: "Values",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Fields",
                columns: new[] { "Id", "DataTypeId", "DeleteDate", "DisplayName", "FieldOrder", "FieldTypeId", "LookupEntityFieldId", "LookupEntityId", "LookupMultipleSelection", "UpdateDate" },
                values: new object[,]
                {
                    { new Guid("10000000-0000-0000-0005-000000000001"), 4, null, "Наименование", 0, 4, null, null, null, null },
                    { new Guid("10000000-0000-0000-0005-000000000003"), 0, null, "Стоимость", 0, 4, null, null, null, null },
                    { new Guid("10000000-0000-0000-0005-000000000004"), 0, null, "Внесенная сумма", 0, 6, null, null, null, null },
                    { new Guid("10000000-0000-0000-0005-000000000005"), 5, null, "Номер телефона", 0, 4, null, null, null, null },
                    { new Guid("10000000-0000-0000-0005-000000000011"), 4, null, "Описание", 0, 4, null, null, null, null },
                    { new Guid("10000000-0000-0000-0005-000000000012"), 4, null, "Адрес", 0, 4, null, null, null, null },
                    { new Guid("10000000-0000-0000-0005-000000000015"), 5, null, "Номер договора", 0, 4, null, null, null, null },
                    { new Guid("10000000-0000-0000-0005-000000000016"), 4, null, "Выбранный склад", 0, 5, new Guid("10000000-0000-0000-1136-000000000001"), new Guid("f0000000-0000-0000-0000-000000000002"), true, null },
                    { new Guid("10000000-0000-0000-0006-000000000001"), 1, null, "Дата договора", 0, 4, null, null, null, null }
                });

            migrationBuilder.InsertData(
                table: "Periods",
                columns: new[] { "Id", "DeleteDate", "Name", "UpdateDate" },
                values: new object[,]
                {
                    { new Guid("b0000000-0000-0000-0000-000000000001"), null, "Дни", null },
                    { new Guid("b0000000-0000-0000-0000-000000000002"), null, "Недели", null },
                    { new Guid("b0000000-0000-0000-0000-000000000003"), null, "Месяцы", null },
                    { new Guid("b0000000-0000-0000-0000-000000000004"), null, "Годы", null }
                });

            migrationBuilder.InsertData(
                table: "Units",
                columns: new[] { "Id", "Definiton", "DeleteDate", "DisplayName", "UpdateDate", "isTaskConnnected" },
                values: new object[,]
                {
                    { new Guid("10000000-0000-0000-0065-000000000001"), "Сущность договоры", null, "Договор", null, true },
                    { new Guid("f0000000-0000-0000-0000-000000000002"), "Сущность склад", null, "Склад", null, true },
                    { new Guid("f0000000-0000-0000-0000-000000000003"), "Сущность контррагент", null, "Контрагент", null, true }
                });

            migrationBuilder.InsertData(
                table: "EntityFields",
                columns: new[] { "Id", "DeleteDate", "FieldId", "UnitId", "UpdateDate" },
                values: new object[,]
                {
                    { new Guid("10000000-0000-0000-0006-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000001"), new Guid("10000000-0000-0000-0065-000000000001"), null },
                    { new Guid("10000000-0000-0000-0026-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000004"), new Guid("10000000-0000-0000-0065-000000000001"), null },
                    { new Guid("10000000-0000-0000-0036-000000000001"), null, new Guid("10000000-0000-0000-0006-000000000001"), new Guid("10000000-0000-0000-0065-000000000001"), null },
                    { new Guid("10000000-0000-0000-0056-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000003"), new Guid("10000000-0000-0000-0065-000000000001"), null },
                    { new Guid("10000000-0000-0000-0136-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000001"), new Guid("f0000000-0000-0000-0000-000000000003"), null },
                    { new Guid("10000000-0000-0000-0236-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000005"), new Guid("f0000000-0000-0000-0000-000000000003"), null },
                    { new Guid("10000000-0000-0000-0736-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000012"), new Guid("f0000000-0000-0000-0000-000000000003"), null },
                    { new Guid("10000000-0000-0000-1136-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000001"), new Guid("f0000000-0000-0000-0000-000000000002"), null },
                    { new Guid("10000000-0000-0000-1736-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000016"), new Guid("f0000000-0000-0000-0000-000000000003"), null },
                    { new Guid("10000000-0000-0000-2236-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000011"), new Guid("f0000000-0000-0000-0000-000000000002"), null },
                    { new Guid("10000000-0000-0000-3736-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000012"), new Guid("f0000000-0000-0000-0000-000000000002"), null }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "DeleteDate", "ParentId", "UnitId", "UpdateDate" },
                values: new object[,]
                {
                    { new Guid("10000000-0000-0000-0001-000000000001"), null, null, new Guid("10000000-0000-0000-0065-000000000001"), null },
                    { new Guid("10000000-0000-0000-0001-000000000002"), null, null, new Guid("10000000-0000-0000-0065-000000000001"), null },
                    { new Guid("10000000-0000-0000-0001-000000000003"), null, null, new Guid("10000000-0000-0000-0065-000000000001"), null },
                    { new Guid("10000000-0000-0000-0001-000000000004"), null, null, new Guid("f0000000-0000-0000-0000-000000000003"), null },
                    { new Guid("10000000-0000-0000-0001-000000000005"), null, null, new Guid("f0000000-0000-0000-0000-000000000003"), null },
                    { new Guid("10000000-0000-0000-0001-000000000006"), null, null, new Guid("f0000000-0000-0000-0000-000000000002"), null },
                    { new Guid("10000000-0000-0000-0001-000000000007"), null, new Guid("10000000-0000-0000-0001-000000000006"), new Guid("f0000000-0000-0000-0000-000000000002"), null },
                    { new Guid("10000000-0000-0000-0001-000000000009"), null, new Guid("10000000-0000-0000-0001-000000000006"), new Guid("f0000000-0000-0000-0000-000000000002"), null }
                });

            migrationBuilder.InsertData(
                table: "Values",
                columns: new[] { "Id", "BoolData", "DateData", "DeleteDate", "EntityFieldId", "IntegerData", "ItemId", "NumberData", "Text", "UpdateDate" },
                values: new object[,]
                {
                    { new Guid("a0000000-0000-0000-0000-000000000001"), null, null, null, new Guid("10000000-0000-0000-0006-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000001"), null, "Договор на разработку программного продукта", null },
                    { new Guid("a0000000-0000-0000-0000-000000000002"), null, new DateTime(2023, 12, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("10000000-0000-0000-0036-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000001"), null, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000003"), null, null, null, new Guid("10000000-0000-0000-0026-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000001"), 100000000f, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000004"), null, null, null, new Guid("10000000-0000-0000-0056-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000001"), 50000f, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000007"), null, null, null, new Guid("10000000-0000-0000-0006-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000002"), null, "Договор на техническую поддержку", null },
                    { new Guid("a0000000-0000-0000-0000-000000000008"), null, new DateTime(2023, 11, 23, 0, 0, 0, 0, DateTimeKind.Unspecified), null, new Guid("10000000-0000-0000-0036-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000002"), null, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000009"), null, null, null, new Guid("10000000-0000-0000-0026-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000002"), 4.5E+09f, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000010"), null, null, null, new Guid("10000000-0000-0000-0056-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000002"), 40000f, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000011"), null, null, null, new Guid("10000000-0000-0000-0056-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000002"), 350000f, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000012"), null, null, null, new Guid("10000000-0000-0000-0056-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000002"), 680000f, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000013"), null, null, null, new Guid("10000000-0000-0000-0136-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000004"), null, "OOO ТехСтрой", null },
                    { new Guid("a0000000-0000-0000-0000-000000000014"), null, null, null, new Guid("10000000-0000-0000-0236-000000000001"), 1224565767, new Guid("10000000-0000-0000-0001-000000000004"), null, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000015"), null, null, null, new Guid("10000000-0000-0000-0736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000004"), null, "г. Москва , улица Урицкого 25", null },
                    { new Guid("a0000000-0000-0000-0000-000000000016"), null, null, null, new Guid("10000000-0000-0000-0136-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000004"), null, "OOO СпецТехника", null },
                    { new Guid("a0000000-0000-0000-0000-000000000017"), null, null, null, new Guid("10000000-0000-0000-0236-000000000001"), 98763454, new Guid("10000000-0000-0000-0001-000000000005"), null, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000018"), null, null, null, new Guid("10000000-0000-0000-0736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000005"), null, "г. Москва , улица О. Кошевого 68-Б", null },
                    { new Guid("a0000000-0000-0000-0000-000000000019"), null, null, null, new Guid("10000000-0000-0000-1136-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000006"), null, "Склад микросхем", null },
                    { new Guid("a0000000-0000-0000-0000-000000000020"), null, null, null, new Guid("10000000-0000-0000-2236-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000006"), null, "Склад микросхем №1", null },
                    { new Guid("a0000000-0000-0000-0000-000000000021"), null, null, null, new Guid("10000000-0000-0000-3736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000006"), null, "г. Ставрополь, ул. Ленина 67", null },
                    { new Guid("a0000000-0000-0000-0000-000000000031"), null, null, null, new Guid("10000000-0000-0000-1736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000004"), null, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000032"), null, null, null, new Guid("10000000-0000-0000-1736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000005"), null, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000213"), null, null, null, new Guid("10000000-0000-0000-0026-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000001"), 14500000f, null, null },
                    { new Guid("a0000000-0000-0000-0000-000000000214"), null, null, null, new Guid("10000000-0000-0000-0026-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000001"), 7500000f, null, null }
                });

            migrationBuilder.InsertData(
                table: "Items",
                columns: new[] { "Id", "DeleteDate", "ParentId", "UnitId", "UpdateDate" },
                values: new object[] { new Guid("10000000-0000-0000-0001-000000000008"), null, new Guid("10000000-0000-0000-0001-000000000007"), new Guid("f0000000-0000-0000-0000-000000000002"), null });

            migrationBuilder.InsertData(
                table: "PeriodValues",
                columns: new[] { "Id", "DeleteDate", "PeriodId", "UpdateDate", "ValueId", "Value_dt" },
                values: new object[,]
                {
                    { new Guid("c0000000-0000-0000-0000-000000000004"), null, new Guid("b0000000-0000-0000-0000-000000000001"), null, new Guid("a0000000-0000-0000-0000-000000000003"), new DateTime(2023, 11, 3, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("c0000000-0000-0000-0000-000000000005"), null, new Guid("b0000000-0000-0000-0000-000000000001"), null, new Guid("a0000000-0000-0000-0000-000000000009"), new DateTime(2023, 11, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("c0000000-0000-0000-0000-000000000015"), null, new Guid("b0000000-0000-0000-0000-000000000001"), null, new Guid("a0000000-0000-0000-0000-000000000213"), new DateTime(2023, 11, 7, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { new Guid("c0000000-0000-0000-0000-000000000016"), null, new Guid("b0000000-0000-0000-0000-000000000001"), null, new Guid("a0000000-0000-0000-0000-000000000214"), new DateTime(2023, 11, 8, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });

            migrationBuilder.InsertData(
                table: "SelectedDictionaryItems",
                columns: new[] { "Id", "DeleteDate", "SelectedItemId", "UpdateDate", "ValueId" },
                values: new object[,]
                {
                    { new Guid("a0000000-0000-0000-0000-000000000032"), null, new Guid("10000000-0000-0000-0001-000000000009"), null, new Guid("a0000000-0000-0000-0000-000000000031") },
                    { new Guid("c0000002-0000-0000-0000-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000007"), null, new Guid("a0000000-0000-0000-0000-000000000031") },
                    { new Guid("c0000004-0000-0000-0000-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000008"), null, new Guid("a0000000-0000-0000-0000-000000000031") },
                    { new Guid("c0000052-0000-0000-0000-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000006"), null, new Guid("a0000000-0000-0000-0000-000000000032") }
                });

            migrationBuilder.InsertData(
                table: "Values",
                columns: new[] { "Id", "BoolData", "DateData", "DeleteDate", "EntityFieldId", "IntegerData", "ItemId", "NumberData", "Text", "UpdateDate" },
                values: new object[,]
                {
                    { new Guid("a0000000-0000-0000-0000-000000000022"), null, null, null, new Guid("10000000-0000-0000-1136-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000007"), null, "Склад готовой продукции", null },
                    { new Guid("a0000000-0000-0000-0000-000000000023"), null, null, null, new Guid("10000000-0000-0000-2236-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000007"), null, "Склад готовой продукции №1", null },
                    { new Guid("a0000000-0000-0000-0000-000000000024"), null, null, null, new Guid("10000000-0000-0000-3736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000007"), null, "г.Таганрог, ул. Петровкская 124", null },
                    { new Guid("a0000000-0000-0000-0000-000000000028"), null, null, null, new Guid("10000000-0000-0000-1136-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000009"), null, "Склад бытовой химии", null },
                    { new Guid("a0000000-0000-0000-0000-000000000029"), null, null, null, new Guid("10000000-0000-0000-2236-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000009"), null, "Склад бытовой химии №1", null },
                    { new Guid("a0000000-0000-0000-0000-000000000030"), null, null, null, new Guid("10000000-0000-0000-3736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000009"), null, "г.Краснодар, ул. Галицкого 178", null },
                    { new Guid("a0000000-0000-0000-0000-000000000025"), null, null, null, new Guid("10000000-0000-0000-1136-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000008"), null, "Канцелярский склад", null },
                    { new Guid("a0000000-0000-0000-0000-000000000026"), null, null, null, new Guid("10000000-0000-0000-2236-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000008"), null, "Канцелярский склад №1", null },
                    { new Guid("a0000000-0000-0000-0000-000000000027"), null, null, null, new Guid("10000000-0000-0000-3736-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000008"), null, "г.Краснодар, ул. Галицкого 34", null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_EntityFields_FieldId",
                table: "EntityFields",
                column: "FieldId");

            migrationBuilder.CreateIndex(
                name: "IX_EntityFields_UnitId",
                table: "EntityFields",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_ParentId",
                table: "Items",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Items_UnitId",
                table: "Items",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_PeriodValues_PeriodId",
                table: "PeriodValues",
                column: "PeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_PeriodValues_ValueId",
                table: "PeriodValues",
                column: "ValueId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_SelectedDictionaryItems_ValueId",
                table: "SelectedDictionaryItems",
                column: "ValueId");

            migrationBuilder.CreateIndex(
                name: "IX_Values_EntityFieldId",
                table: "Values",
                column: "EntityFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_Values_ItemId",
                table: "Values",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_View_UnitId",
                table: "View",
                column: "UnitId");

            migrationBuilder.CreateIndex(
                name: "IX_ViewField_EntityFieldId",
                table: "ViewField",
                column: "EntityFieldId");

            migrationBuilder.CreateIndex(
                name: "IX_ViewField_ViewId",
                table: "ViewField",
                column: "ViewId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PeriodValues");

            migrationBuilder.DropTable(
                name: "SelectedDictionaryItems");

            migrationBuilder.DropTable(
                name: "ViewField");

            migrationBuilder.DropTable(
                name: "Periods");

            migrationBuilder.DropTable(
                name: "Values");

            migrationBuilder.DropTable(
                name: "View");

            migrationBuilder.DropTable(
                name: "EntityFields");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "Fields");

            migrationBuilder.DropTable(
                name: "Units");
        }
    }
}
