﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BusinessTable.WebApi.Migrations
{
    /// <inheritdoc />
    public partial class periodValue : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Fields",
                columns: new[] { "Id", "DataTypeId", "DeleteDate", "DisplayName", "FieldOrder", "FieldTypeId", "LookupEntityFieldId", "LookupEntityId", "LookupMultipleSelection", "UpdateDate" },
                values: new object[] { new Guid("10000000-0000-0000-0005-000000000018"), 0, null, "Оплата по дате", 0, 6, null, null, null, null });

            migrationBuilder.InsertData(
                table: "EntityFields",
                columns: new[] { "Id", "DeleteDate", "FieldId", "UnitId", "UpdateDate" },
                values: new object[] { new Guid("10000000-0000-0000-0027-000000000001"), null, new Guid("10000000-0000-0000-0005-000000000018"), new Guid("10000000-0000-0000-0065-000000000001"), null });

            migrationBuilder.InsertData(
                table: "Values",
                columns: new[] { "Id", "BoolData", "DateData", "DeleteDate", "EntityFieldId", "IntegerData", "ItemId", "NumberData", "Text", "UpdateDate" },
                values: new object[] { new Guid("a0000000-0000-0000-0000-000000000334"), null, null, null, new Guid("10000000-0000-0000-0027-000000000001"), null, new Guid("10000000-0000-0000-0001-000000000001"), 2700000f, null, null });

            migrationBuilder.InsertData(
                table: "PeriodValues",
                columns: new[] { "Id", "DeleteDate", "PeriodId", "UpdateDate", "ValueId", "Value_dt" },
                values: new object[] { new Guid("c0000000-0000-0000-0000-000000000116"), null, new Guid("b0000000-0000-0000-0000-000000000001"), null, new Guid("a0000000-0000-0000-0000-000000000334"), new DateTime(2023, 11, 2, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "PeriodValues",
                keyColumn: "Id",
                keyValue: new Guid("c0000000-0000-0000-0000-000000000116"));

            migrationBuilder.DeleteData(
                table: "Values",
                keyColumn: "Id",
                keyValue: new Guid("a0000000-0000-0000-0000-000000000334"));

            migrationBuilder.DeleteData(
                table: "EntityFields",
                keyColumn: "Id",
                keyValue: new Guid("10000000-0000-0000-0027-000000000001"));

            migrationBuilder.DeleteData(
                table: "Fields",
                keyColumn: "Id",
                keyValue: new Guid("10000000-0000-0000-0005-000000000018"));
        }
    }
}
