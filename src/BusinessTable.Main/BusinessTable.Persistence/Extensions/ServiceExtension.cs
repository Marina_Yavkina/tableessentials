using BusinessTable.Application.Repositories;
using BusinessTable.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Persistence.Repositories;

namespace BusinessTable.Persistence.Extensions;

public static class ServiceExtensions
{
    public static void ConfigurePersistence(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("BusinessTable");
        services.AddDbContext<DataContext>(options => options.UseLazyLoadingProxies()
                                                             .UseNpgsql(connectionString,b => b.MigrationsAssembly("BusinessTable.WebApi")),
                                                              ServiceLifetime.Scoped);

        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<IUnitRepository,UnitRepository>();
        services.AddScoped<IFieldRepository,FieldRepository>();
        services.AddScoped<IUnitFieldRepository,UnitFieldRepository>();
        services.AddScoped<IItemRepository,ItemRepository>();
        services.AddScoped<IPeriodRepository,PeriodRepository>();
        services.AddScoped<IPeriodValueRepository,PeriodValueRepository>();
        services.AddScoped<ISelectedDataDictionaryRepository,SelectedDataDictionaryRepository>();
        services.AddScoped<IValueRepository,ValueRepository>();
    }
}