using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;

namespace  BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class UnitFieldRepository : Repository<EntityField, Guid>, IUnitFieldRepository
    {
        public UnitFieldRepository(DataContext context) : base(context) { }

               
        public async Task<List<EntityField>> GetFieldsByEntityIdAsync(Guid entityId, bool noTracking = false)
        {
            try 
            {
            var ef = await GetListWithInclude(p =>  p.UnitId == entityId && p.IsDeleted == false,c => c.Field);
            return ef.ToList();
            }
            catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public IQueryable<EntityField> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);

        }
    }
}
