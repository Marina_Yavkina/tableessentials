﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;

namespace  BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class ItemRepository : Repository<Item, Guid>, IItemRepository
    {
        public ItemRepository(DataContext context) : base(context) { }
       
        public async Task<List<Item>> GetItemsByEntityIdAsync(Guid id ,bool noTracking = false)
        {
            var items = await GetListWithInclude(p => p.UnitId == id && p.IsDeleted == false);

            return items.ToList();
        }
        public async Task<List<Item>> GetItemsWithChildsByIdAsync(Guid id ,bool noTracking = false)
        {
            var item = await GetAsync(id);
            var subitems =  item.SubItems?.SelectManyRecursive(x => x.SubItems).ToList();
            subitems.AddRange(item.SubItems);
            return subitems;            
        }
        public IQueryable<Item> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);

        }
    }
}
