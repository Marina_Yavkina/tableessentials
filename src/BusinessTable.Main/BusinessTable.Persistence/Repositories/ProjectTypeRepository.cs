﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;
using BusinessTable.Domain.Models;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class ProjectTypeRepository : Repository<ProjectType, Guid>, IProjectTypeRepository
    {
        public ProjectTypeRepository(DataContext context) : base(context) { }

        public async Task<PagedList<ProjectType>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<ProjectType>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
        public async Task<ProjectType> GetById(Guid id, bool noTracking = false)
        {
            return await GetAll(noTracking)
                .Where(p => p.Id == id )
                .FirstAsync();
        }
        public async Task<ProjectType> GetByName(string name, bool noTracking = false)
        {
            return await GetAll(noTracking)
                .Where(p => p.Name == name )
                .FirstAsync();
        }
        public IQueryable<ProjectType> GetAllAsync( bool noTracking = false)
        {
            return GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
        }

        
    }
}
