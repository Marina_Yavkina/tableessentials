﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class PeriodRepository : Repository<Period, Guid>, IPeriodRepository
    {
        public PeriodRepository(DataContext context) : base(context) { }
              
        public IQueryable<Period> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);

        }

        public async Task<List<Period>> GetPeriodsAsync( bool noTracking = false)
        {
            var periods = await GetListWithInclude(p => p.IsDeleted == false);

            return periods.ToList();
        }
    }
}
