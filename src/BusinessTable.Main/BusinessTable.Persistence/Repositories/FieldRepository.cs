﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class FieldRepository : Repository<Field, Guid>, IFieldRepository
    {
        public FieldRepository(DataContext context) : base(context) { }

        public async Task<List<Field>> GetPagedByTextAsync(string text, bool noTracking = false)
        {
            
            var fields = await GetListWithInclude(p => p.DisplayName.Contains(text) && p.IsDeleted == false);
            return fields.ToList();
        }
        public IQueryable<Field> GetAllAsync( bool noTracking = false)
        {
            return GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
        }
    }
}
