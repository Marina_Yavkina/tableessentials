﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class SelectedDataDictionaryRepository : Repository<SelectedDictionaryItem, Guid>, ISelectedDataDictionaryRepository
    {
        public SelectedDataDictionaryRepository(DataContext context) : base(context) { }

        public async Task<List<SelectedDictionaryItem>> GetSelectedItemsByValueIdAsync(Guid valueId, bool noTracking = false)
        {
            var selectedItems  = await GetListWithInclude(p => p.ValueId == valueId &&  p.IsDeleted == false);

            return selectedItems.ToList();
        }
        public IQueryable<SelectedDictionaryItem> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);

        }

    }
}
