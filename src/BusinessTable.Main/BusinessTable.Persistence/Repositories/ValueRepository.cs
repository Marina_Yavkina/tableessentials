﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;
using System.Linq.Expressions;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class ValueRepository : Repository<Value, Guid>, IValueRepository
    {
        public ValueRepository(DataContext context) : base(context) { }
        public async Task<List<Value>> GetValuesByEntityfieldAndItemAsync(Guid entityFieldId, Guid itemId, bool noTracking = false,params Expression<Func<Value, object>>[] includeProperties)
        {
            var values  = await GetListWithInclude(p => p.EntityFieldId == entityFieldId && p.ItemId == itemId && p.IsDeleted == false,includeProperties);

            return values.ToList();
        }
        public IQueryable<Value> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);

        }
    }
}
