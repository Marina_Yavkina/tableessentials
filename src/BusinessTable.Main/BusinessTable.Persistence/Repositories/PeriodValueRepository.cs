﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class PeriodValueRepository : Repository<PeriodValue, Guid>, IPeriodValueRepository
    {
        public PeriodValueRepository(DataContext context) : base(context) { }
              
        public IQueryable<PeriodValue> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);

        }


        public async Task<List<PeriodValue>> GetPeriodValueByValueIdAsync(Guid valueId, bool noTracking = false)
        {
             var periodvalues = await GetListWithInclude(p => p.ValueId== valueId && p.IsDeleted == false);

            return periodvalues.ToList();
        }
    }
}
