﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;

namespace  BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class UnitRepository : Repository<Unit, Guid>, IUnitRepository
    {
        public UnitRepository(DataContext context) : base(context) { }

        
        public async Task<List<Unit>> GetPagedByTextAsync(string text, int pageNumber, int pageSize, bool noTracking = false)
        {
            var entity = await GetListWithInclude(p => p.DisplayName.Contains(text) && p.IsDeleted == false);
            return entity.ToList();
        }
        
         public IQueryable<Unit> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);
        }

        
    }
}
