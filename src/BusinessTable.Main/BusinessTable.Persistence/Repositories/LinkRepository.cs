using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;
using BusinessTable.Domain.Models;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class LinkRepository : Repository<Link, Guid>, ILinkRepository
    {
        public LinkRepository(DataContext context) : base(context) { }

        public async Task<PagedList<Link>> GetPagedAsync(Guid[] taskIds,bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => (taskIds.Contains(p.TargetTaskId) ||  taskIds.Contains(p.SourceTaskId)) &&  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Link>()
            {
                Entities = await query.ToListAsync(),
                Pagination = new Pagination(query.Count(), 0, 0)
            };
        }
        public async Task<List<Link>> GetPagedLinksAsync(Guid[] taskIds,  bool noTracking = false)
        {
             var query = GetAll(noTracking)
                .Where(p => (taskIds.Contains(p.TargetTaskId) ||  taskIds.Contains(p.SourceTaskId)) &&  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);
             return await query.ToListAsync();   

        }

        public async Task<PagedList<Link>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false)
        {
             var query = GetAll(noTracking)
                .Where(p => p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<Link>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }
    }
}
