﻿using BusinessTable.Application.Repositories.Base;
using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;
using BusinessTable.Domain.Models;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class ProjectTypeFieldRepository : Repository<ProjectTypeField, Guid>, IProjectTypeFieldRepository
    {
        public ProjectTypeFieldRepository(DataContext context) : base(context) { }

        public async Task<PagedList<ProjectTypeField>> GetPagedAsync( int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<ProjectTypeField>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }

        public async Task<PagedList<ProjectTypeField>> GetPagedByProjectTypeIdAsync(Guid projectTypeId, int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p => p.ProjectTypeId == projectTypeId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<ProjectTypeField>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };

        }
        public IQueryable<ProjectTypeField> GetAllAsync(bool noTracking = false)
        {
            return  GetAll(noTracking)
            .Where(p => p.IsDeleted == false)
            .OrderBy(p => p.CreateDate);
        }
        
    }
}
