using BusinessTable.Application.Repositories;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;

namespace BusinessTable.Persistence.Repositories.Base;

public class UnitOfWork : IUnitOfWork
{
    private readonly DataContext _context;

    public UnitOfWork(DataContext context)
    {
        _context = context;
    }
    public Task Save(CancellationToken cancellationToken)
    {
        return _context.SaveChangesAsync(cancellationToken);
    }
}