﻿using Microsoft.EntityFrameworkCore;
using BusinessTable.Application.Repositories.Base;
using System.Linq.Expressions;
using BusinessTable.Domain.Common;
using BusinessTable.Persistence.Extensions;

namespace BusinessTable.Persistence.Repositories.Base
{
    /// <summary>
    /// Репозиторий для чтения
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    /// <typeparam name="TPrimaryKey">Основной ключ</typeparam>
    public abstract class ReadRepository<T, TPrimaryKey>
        : IReadRepository<T, TPrimaryKey> where T 
        : class, IEntity<TPrimaryKey>
    {
        protected readonly DbContext Context;
        protected DbSet<T> EntitySet;

        protected ReadRepository(DbContext context)
        {
            Context = context;
            EntitySet = Context.Set<T>();
        }

        /// <summary>
        /// Запросить все сущности в базе
        /// </summary>
        /// <param name="asNoTracking">Вызвать с AsNoTracking</param>
        /// <returns>IQueryable массив сущностей</returns>
        protected virtual IQueryable<T> GetAll(bool asNoTracking = false)
        {
            return asNoTracking ? EntitySet.AsNoTracking() : EntitySet;
        }

        public virtual async Task<T> GetAsync(TPrimaryKey id, bool asNoTracking = false)
        {
            if(!asNoTracking)
                return await EntitySet.FindAsync((object)id);
            
            return await EntitySet.AsNoTracking().FirstOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<T> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await EntitySet.FirstOrDefaultAsync(predicate);
        }

        public async Task<T> GetWithIncludeAsync(Expression<Func<T, bool>> predicate, 
            params Expression<Func<T, object>>[] includeProperties)
        {
            return await EntitySet.IncludeEx(includeProperties).FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<T>> GetListWithInclude(Func<T, bool> predicate, 
            params Expression<Func<T, object>>[]? includeProperties)
        {
            if (includeProperties != null)
            {
            return  EntitySet.AsNoTracking()
                .IncludeEx(includeProperties)
                .Where(predicate)
                .ToList();
            }
            else{
                return EntitySet.Where(predicate).ToList();
            }

            // return await EntitySet.AsNoTracking()
            //     .IncludeEx(includeProperties)
            //     .Where(predicate)
            //     .ToList();
        }
        public Task<List<T>> GetAll(CancellationToken cancellationToken)
        {
            return EntitySet.ToListAsync(cancellationToken);
        }

       
    }
}