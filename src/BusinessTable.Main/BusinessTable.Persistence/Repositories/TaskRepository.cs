﻿using BusinessTable.Application.Repositories.Base;
using A=BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Persistence.Context;
using BusinessTable.Persistence.Extensions;
using BusinessTable.Persistence.Repositories.Base;
using BusinessTable.Application.Repositories;
using BusinessTable.Domain.Models;

namespace BusinessTable.Persistence.Repositories
{
    /// <summary>
    /// Репозиторий зоны
    /// </summary>
    public class TaskRepository : Repository<A.Task, Guid>, ITaskRepository
    {
        public TaskRepository(DataContext context) : base(context) { }

        public async Task<PagedList<A.Task>> GetPagedAsync(int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking)
                .Where(p =>  p.IsDeleted == false)
                .OrderBy(p => p.CreateDate).Include(t => t.Assignments);

            return new PagedList<A.Task>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }

         public async Task<PagedList<A.Task>> GetOnlyProjectsAsync(int pageNumber, int pageSize, bool noTracking = false)
         {
            var query = GetAll(noTracking).Include(t => t.Assignments)
                .Where(p => p.Type == "project" && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<A.Task>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
         }
        public async Task<PagedList<A.Task>> GetTasksByPrpjectTypePagedAsync(Guid projectTypeId, int pageNumber, int pageSize, bool noTracking = false)
        {
            var query = GetAll(noTracking).Include(t => t.Assignments)
                .Where(p => p.ProjectType.Id == projectTypeId && p.IsDeleted == false)
                .OrderBy(p => p.CreateDate);

            return new PagedList<A.Task>()
            {
                Entities = await query
                    .Skip((pageNumber - 1) * pageSize)
                    .Take(pageSize)
                    .ToListAsync(),
                Pagination = new Pagination(query.Count(), pageNumber, pageSize)
            };
        }

        public async Task<PagedList<A.Task>?> GetTaskByProjectIdPagedAsync(Guid taskId, int pageNumber, int pageSize, bool noTracking = false)
        {
            try{
            var res =  GetAll(noTracking).Include(c => c.Assignments).Where(x => ((x.ParentId == null) && (x.Id == taskId))).FirstOrDefault();
            
            //var subtasks = GetChildren(res?.SubTasks!,res.Id); //res?.SubTasks?.GetRecursive(taskId);
         
            var subtasks =  res?.SubTasks?.SelectManyRecursive(x => x.SubTasks).ToList();
            subtasks.AddRange(res.SubTasks);
            

            if (subtasks is null) 
               return null;
               //var subtasks = result.Where(x => ((x.ParentId == null) && (x.Id == id))).AsQueryable();
            return new PagedList<A.Task>()
            {
                Entities =  subtasks!,
                Pagination = new Pagination(subtasks!.Count(), pageNumber, pageSize)
            };
            }
            catch (Exception ex) {
                var e = ex.Message;
                return new PagedList<A.Task>();
            }
        }

         public async Task<List<A.Task>?> GetTaskByProjectIdAsync(Guid taskId, bool noTracking = false)
        {
            try{
            var res =  GetAll(noTracking).Include(c => c.Assignments).Where(x => ((x.ParentId == null) && (x.Id == taskId))).FirstOrDefault();
            
            //var subtasks = GetChildren(res?.SubTasks!,res.Id); //res?.SubTasks?.GetRecursive(taskId);
         
            var subtasks =  res?.SubTasks?.SelectManyRecursive(x => x.SubTasks).ToList();
            subtasks.AddRange(res.SubTasks);
            

            if (subtasks is null) 
               return null;
               //var subtasks = result.Where(x => ((x.ParentId == null) && (x.Id == id))).AsQueryable();
            return subtasks;
            }
            catch (Exception ex) {
                var e = ex.Message;
                return new List<A.Task>();
            }
        }

        public List<A.Task> GetChildren(List<A.Task> foos, Guid id)
        {
            return foos
                .Where(x => x.ParentId == id)
                .Union(foos.Where(x => x.ParentId == id)
                    .SelectMany(y => GetChildren(y.SubTasks, y.Id))
                ).ToList();
        }
        public async Task<A.Task> GetById(Guid id, bool noTracking = false)
        {
            return await GetAll(noTracking)
                .Where(p => p.Id== id )
                .FirstAsync();
        }
    }
}
