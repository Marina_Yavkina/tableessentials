﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace BusinessTable.Persistence.Context.Configurations
{
    public class ValueConfiguration : IEntityTypeConfiguration<Value>
    {
        public void Configure(EntityTypeBuilder<Value> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.HasOne(j => j.Item)
                     .WithMany(j => j.Values)
                     .HasForeignKey(j => j.ItemId);
              builder.Property(a => a.Text)
                     .HasColumnType("character varying(1024)")
                     .IsUnicode(false)
                     .IsRequired(false);
              builder.Property(a => a.NumberData)
                     .HasColumnType("numeric(30, 6)")
                     .IsRequired(false);
              builder.Property(a => a.IntegerData)
                     .HasColumnType("integer")
                     .IsRequired(false);
              builder.Property(a => a.DateData)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.BoolData)
                     .HasColumnType("boolean")
                     .IsRequired(false);
              builder.HasOne(j => j.EntityField)
                     .WithMany(j => j.Values)
                     .HasForeignKey(j => j.EntityFieldId);
              builder.HasData(SeedData.Values);

        }
    }
}
