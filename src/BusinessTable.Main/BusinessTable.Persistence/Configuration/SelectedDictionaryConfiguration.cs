﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace  BusinessTable.Persistence.Context.Configurations
{
    public class SelectedDictionaryConfiguration : IEntityTypeConfiguration<SelectedDictionaryItem>
    {
        public void Configure(EntityTypeBuilder<SelectedDictionaryItem> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.HasOne(f => f.Value)
                     .WithMany(p => p.SelectedDictionaryItems)
                     .HasForeignKey(a => a.ValueId)
                     .IsRequired();
              builder.Property(a => a.SelectedItemId)
                     .HasColumnType("uuid")
                     .IsRequired();
              builder.HasData(SeedData.SelectedDictionaryItems);
        }
    }
}
