﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;

namespace BastionGantt.DataAccess.Context.Configurations
{
    public class TaskValueConfiguration : IEntityTypeConfiguration<TaskValue>
    {
        public void Configure(EntityTypeBuilder<TaskValue> builder)
        {
                  builder.Property(a => a.Id)
                        .HasColumnType("uuid")
                        .HasDefaultValueSql("uuid_generate_v4()")
                        .IsRequired();
                  builder.Property(a => a.TextData)
                        .HasColumnType("character varying(1024)")
                        .IsUnicode(false);
                  builder.Property(a => a.NumericData)
                        .HasColumnType("numeric(30, 6)")
                        .IsUnicode(false);
                  builder.Property(a => a.IsDeleted)
                        .HasDefaultValue(false)
                        .IsRequired();
                  builder.Property(a => a.CreateDate)
                        .IsRequired()
                        .HasColumnType("timestamp")
                        .HasDefaultValueSql("now()");
                  builder.Property(a => a.UpdateDate)
                        .IsRequired(false)
                        .HasColumnType("timestamp");
                  builder.HasOne(a => a.Item)
                        .WithMany()
                        .HasForeignKey(t => t.ItemId);
                  builder.HasOne(a => a.Task)
                        .WithMany()
                        .HasForeignKey(t => t.TaskId)
                        .IsRequired();
                  builder.HasOne(a => a.Value)
                        .WithMany()
                        .HasForeignKey(t => t.ValueId);
            
        }
    }
}
