﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace  BusinessTable.Persistence.Context.Configurations
{
    public class ItemConfiguration : IEntityTypeConfiguration<Item>
    {
        public void Configure(EntityTypeBuilder<Item> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.HasMany(j => j.SubItems)
                     .WithOne(j => j.ParentItem)
                     .HasForeignKey(j => j.ParentId);
              builder.HasData(SeedData.Items);
        }
    }
}
