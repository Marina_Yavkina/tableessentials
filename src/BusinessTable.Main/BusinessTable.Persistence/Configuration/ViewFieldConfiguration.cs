﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace  BusinessTable.Persistence.Context.Configurations
{
    public class ViewFieldConfiguration : IEntityTypeConfiguration<ViewField>
    {
        public void Configure(EntityTypeBuilder<ViewField> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.HasOne(f => f.View)
                     .WithMany(p => p.ViewFields)
                     .HasForeignKey(a => a.ViewId)
                     .IsRequired();
              builder.Property(a => a.Name)
                     .HasColumnType("vcharacter varying(256)")
                     .IsRequired();
              builder.Property(a => a.FieldIndex)
                     .HasColumnType("integer")
                     .IsRequired(false);
              builder.Property(a => a.FieldWidth)
                     .HasColumnType("integer")
                     .IsRequired(false);
              builder.Property(a => a.DistributionOrder)
                     .HasColumnType("integer")
                     .IsRequired(false);
              builder.Property(a => a.SortOrder)
                     .HasColumnType("integer")
                     .IsRequired();
        }
    }
}
