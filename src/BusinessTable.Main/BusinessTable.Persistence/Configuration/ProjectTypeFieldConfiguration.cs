using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace  BusinessTable.Persistence.Context.Configurations
{
    public class ProjectTypeFieldConfiguration : IEntityTypeConfiguration<ProjectTypeField>
    {
        public void Configure(EntityTypeBuilder<ProjectTypeField> builder)
        {
                builder.Property(a => a.Id)
                        .HasColumnType("uuid")
                        .HasDefaultValueSql("uuid_generate_v4()")
                        .IsRequired();

                builder.Property(a => a.IsDeleted)
                        .HasDefaultValue(false)
                        .IsRequired();
                        
                builder.Property(a => a.UpdateDate)
                        .HasColumnType("timestamp")
                        .IsRequired(false);
                builder.Property(a => a.CreateDate)
                        .HasColumnType("timestamp")
                        .IsRequired()
                        .HasDefaultValueSql("now()");

                builder.HasOne(a => a.ProjectType)
                        .WithMany()
                        .HasForeignKey(t => t.ProjectTypeId)
                        .IsRequired();
                builder.HasOne(a => a.Field)
                        .WithMany()
                        .HasForeignKey(t => t.FieldId)
                        .IsRequired();
                builder.Property(a => a.DisplayTo)
                        .HasColumnType("jsonb");
              
        }
    }
}
