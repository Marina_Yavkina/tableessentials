﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace  BusinessTable.Persistence.Context.Configurations
{
    public class FieldConfiguration : IEntityTypeConfiguration<Field>
    {
        public void Configure(EntityTypeBuilder<Field> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.DisplayName)
                     .IsRequired()
                     .HasColumnType("character varying(256)")
                     .IsUnicode(false);
              builder.Property(a => a.FieldTypeId)
                     .IsRequired()
                     .HasColumnType("integer");  
              builder.Property(a => a.DataTypeId)
                     .IsRequired()
                     .HasColumnType("integer");   
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.Property(a => a.LookupEntityFieldId)
                     .HasColumnType("uuid")
                     .IsRequired(false);
              builder.Property(a => a.LookupEntityId)
                     .HasColumnType("uuid")
                     .IsRequired(false);
               builder.Property(a => a.LookupMultipleSelection)
                     .HasColumnType("boolean")
                     .IsRequired(false);
              builder.HasMany(f => f.EntityFields)
                   .WithOne(p => p.Field)
                   .HasForeignKey(a => a.FieldId)
                   .IsRequired();
              builder.HasData(SeedData.Fields);
        }
    }
}
