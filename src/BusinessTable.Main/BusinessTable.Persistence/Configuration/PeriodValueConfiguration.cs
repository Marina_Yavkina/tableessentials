﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace  BusinessTable.Persistence.Context.Configurations
{
    public class PeriodValueConfiguration : IEntityTypeConfiguration<PeriodValue>
    {
        public void Configure(EntityTypeBuilder<PeriodValue> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.HasOne(f => f.Value)
                     .WithOne(p => p.PeriodValue)
                     .HasForeignKey<PeriodValue>(h => h.ValueId);
              builder.HasOne(f => f.Period)
                     .WithMany(p => p.PeriodValues)
                     .HasForeignKey(f => f.PeriodId);
              builder.Property(a => a.Value_dt)
                     .HasColumnType("timestamp")
                     .IsRequired();
              builder.HasData(SeedData.PeriodValues);
        }
    }
}
