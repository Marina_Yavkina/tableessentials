﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using A=BusinessTable.Domain.Entities;

namespace BastionGantt.DataAccess.Context.Configurations
{
    public class TaskConfiguration : IEntityTypeConfiguration<A.Task>
    {
        public void Configure(EntityTypeBuilder<A.Task> builder)
        {
              builder.HasMany(a => a.Assignments)
                     .WithOne(t => t.Task)
                     .HasForeignKey(t => t.TaskId)
                     .IsRequired(false); 
               builder.HasMany(a => a.Items)
                     .WithOne(t => t.Task)
                     .HasForeignKey(t => t.TaskId)
                     .IsRequired(false); 
                         
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.Text)
                     .IsRequired()
                     .HasColumnType("character varying(500)")
                     .IsUnicode(false);
              builder.Property(a => a.Type)
                     .IsRequired()
                     .HasColumnType("character varying(50)")
                     .IsUnicode(false);
              builder.Property(a => a.StartDate)
                     .HasColumnType("timestamp")
                     .IsRequired();
              builder.Property(a => a.EndDate)
                     .HasColumnType("timestamp")
                     .IsRequired();
              builder.Property(a => a.BaseStart)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.BaseEnd)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
               builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.HasOne(a => a.ProjectType)
                     .WithMany()
                     .HasForeignKey(t => t.ProjectTypeId)
                     .IsRequired(false);
              builder.HasMany(a => a.SubTasks)
                     .WithOne(t => t.ParentTask)
                     .HasForeignKey(t => t.ParentId);
               builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              
        }
    }
}
