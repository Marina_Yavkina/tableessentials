﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using BusinessTable.Domain.Entities;
using BusinessTable.Persistence.Context.Data;

namespace BusinessTable.Persistence.Context.Configurations
{
    public class ViewConfiguration : IEntityTypeConfiguration<View>
    {
        public void Configure(EntityTypeBuilder<View> builder)
        {
              builder.Property(a => a.Id)
                     .HasColumnType("uuid")
                     .HasDefaultValueSql("uuid_generate_v4()")
                     .IsRequired();
              builder.Property(a => a.DisplayName)
                     .IsRequired()
                     .HasColumnType("character varying(256)")
                     .IsUnicode(false);
              builder.Property(a => a.IsDeleted)
                     .HasDefaultValue(false)
                     .IsRequired();
              builder.Property(a => a.UpdateDate)
                     .HasColumnType("timestamp")
                     .IsRequired(false);
              builder.Property(a => a.CreateDate)
                     .HasColumnType("timestamp")
                     .IsRequired()
                     .HasDefaultValueSql("now()");
              builder.HasOne(f => f.Unit)
                     .WithMany(p => p.Views)
                     .HasForeignKey(a => a.UnitId);
        }
    }
}
