﻿using BusinessTable.Domain.Entities;
using BusinessTable.Domain.Common.Constants;
using System.Globalization;

namespace BusinessTable.Persistence.Context.Data
{
    public static class SeedData
    {
        static CultureInfo currentCulture = CultureInfo.CurrentCulture;
        public static IEnumerable<Unit> Units => new List<Unit>()
        {
            new Unit
            {
                Id = Guid.Parse("10000000-0000-0000-0065-000000000001"),
                DisplayName = "Договор",
                Definiton = "Сущность договоры",
                isTaskConnnected = true,
                IsDeleted = false
            },
            new Unit
            {
                Id = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                 DisplayName = "Склад",
                Definiton = "Сущность склад",
                isTaskConnnected = true,
                IsDeleted = false
            },
            new Unit
            {
                Id = Guid.Parse("f0000000-0000-0000-0000-000000000003"),
                DisplayName = "Контрагент",
                Definiton = "Сущность контррагент",
                isTaskConnnected = true,
                IsDeleted = false
            },
        };
        public static IEnumerable<Field> Fields => new List<Field>()
        {
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000001"),
                IsDeleted = false,
                DisplayName = "Наименование",
                FieldTypeId = (int)DataFieldType.Simple,
                DataTypeId = (int)DataType.Text,
            },
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0006-000000000001"),
                IsDeleted = false,
                DisplayName = "Дата договора",
                FieldTypeId = (int)DataFieldType.Simple,
                DataTypeId = (int)DataType.Date,
            },


            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000003"),
                IsDeleted = false,
                DisplayName = "Стоимость",
                FieldTypeId = (int)DataFieldType.Simple,
                DataTypeId = (int)DataType.Cost,
            },
            
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000004"),
                IsDeleted = false,
                DisplayName = "Внесенная сумма",
                FieldTypeId = (int)DataFieldType.PivotColumn,
                DataTypeId = (int)DataType.Cost,
            },
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000018"),
                IsDeleted = false,
                DisplayName = "Оплата по дате",
                FieldTypeId = (int)DataFieldType.PivotColumn,
                DataTypeId = (int)DataType.Cost,
            },
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000005"),
                IsDeleted = false,
                DisplayName = "Номер телефона",
                FieldTypeId = (int)DataFieldType.Simple,
                DataTypeId = (int)DataType.Integer,
            },
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000011"),
                IsDeleted = false,
                DisplayName = "Описание",
                FieldTypeId = (int)DataFieldType.Simple,
                DataTypeId = (int)DataType.Text,
            },
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000015"),
                IsDeleted = false,
                DisplayName = "Номер договора",
                FieldTypeId = (int)DataFieldType.Simple,
                DataTypeId = (int)DataType.Integer,
            },
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000012"),
                IsDeleted = false,
                DisplayName = "Адрес",
                FieldTypeId = (int)DataFieldType.Simple,
                DataTypeId = (int)DataType.Text,
            },
            //TO DO
            new Field
            {
                Id = Guid.Parse("10000000-0000-0000-0005-000000000016"),
                IsDeleted = false,
                DisplayName = "Выбранный склад",
                FieldTypeId = (int)DataFieldType.DictionaryId,
                DataTypeId = (int)DataType.Text,
                LookupEntityId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                LookupEntityFieldId = Guid.Parse("10000000-0000-0000-1136-000000000001"),
                LookupMultipleSelection = true

            },
        };
        public static IEnumerable<EntityField> EntityFields => new List<EntityField>()
        {
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0006-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001"),
                FieldId = Guid.Parse("10000000-0000-0000-0005-000000000001")   //наименование            
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0036-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001"),
                FieldId = Guid.Parse("10000000-0000-0000-0006-000000000001") //дата договора          
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0056-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001"),
                FieldId = Guid.Parse("10000000-0000-0000-0005-000000000003"), //стоимость             
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0026-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001"),
                FieldId =  Guid.Parse("10000000-0000-0000-0005-000000000004"), //внесенная сумма           
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0027-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001"),
                FieldId =  Guid.Parse("10000000-0000-0000-0005-000000000018"), //оплата по дате           
            },
            // Контрагенты
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0136-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000003"),
                FieldId =  Guid.Parse("10000000-0000-0000-0005-000000000001"), //наименование            
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0236-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000003"),
                FieldId = Guid.Parse("10000000-0000-0000-0005-000000000005"),  //номер телефона            
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-0736-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000003"),
                FieldId = Guid.Parse("10000000-0000-0000-0005-000000000012"), // Адрес          
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-1736-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000003"),
                FieldId = Guid.Parse("10000000-0000-0000-0005-000000000016"), // Выбранный склад         
            },
             // Склады
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-1136-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                FieldId =  Guid.Parse("10000000-0000-0000-0005-000000000001"), //наименование             
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-2236-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                FieldId = Guid.Parse("10000000-0000-0000-0005-000000000011"), //описание             
            },
            new EntityField
            {
                Id = Guid.Parse("10000000-0000-0000-3736-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),
                FieldId = Guid.Parse("10000000-0000-0000-0005-000000000012"), //адрес             
            }
        };
        public static IEnumerable<Item> Items => new List<Item>()
        {
            
            new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001") //договоры
            },
            new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000002"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001") //договоры
            },
            new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000003"),
                IsDeleted = false,
                UnitId = Guid.Parse("10000000-0000-0000-0065-000000000001") //договоры
            },
            new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000004"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000003")//контрагенты
            },
            new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000005"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000003")//контрагенты
            },
            new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000006"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000002")//склады
            },
             new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000007"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),//склады
                ParentId = Guid.Parse("10000000-0000-0000-0001-000000000006")
            },
             new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000008"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),//склады
                ParentId = Guid.Parse("10000000-0000-0000-0001-000000000007")
            },
             new Item
            {
                Id = Guid.Parse("10000000-0000-0000-0001-000000000009"),
                IsDeleted = false,
                UnitId = Guid.Parse("f0000000-0000-0000-0000-000000000002"),//склады
                ParentId = Guid.Parse("10000000-0000-0000-0001-000000000006")
            }
        };
        public static IEnumerable<Value> Values => new List<Value>()
        {
            
            new Value
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000001"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-0006-000000000001"),
                Text = "Договор на разработку программного продукта",
                IsDeleted = false
            },
            new Value
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000002"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0036-000000000001"),
                DateData = Convert.ToDateTime("12/12/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat),
                IsDeleted = false
            },
            new Value//* Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000003"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0026-000000000001"),
                NumberData = (float)100000000.000, 
                IsDeleted = false
            },
             new Value//* Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000213"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0026-000000000001"),
                NumberData = (float)14500000.000, 
                IsDeleted = false
            },
             new Value//* Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000214"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0026-000000000001"),
                NumberData = (float)7500000.000, 
                IsDeleted = false
            },
            new Value//* Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000334"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0027-000000000001"),
                NumberData = (float)2700000.000, 
                IsDeleted = false
            },
            new Value // Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000004"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0056-000000000001"),
                NumberData = (float)50000.000, 
                IsDeleted = false
            },
            // new Value // Внести в PeriodValue
            // {
            //     Id = Guid.Parse("a0000000-0000-0000-0000-000000000005"),
            //     ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
            //     EntityFieldId = Guid.Parse("10000000-0000-0000-0056-000000000001"),
            //     NumberData = (float)250000.000, 
            //     IsDeleted = false
            // },
            // new Value // Внести в PeriodValue
            // {
            //     Id = Guid.Parse("a0000000-0000-0000-0000-000000000006"),
            //     ItemId = Guid.Parse("10000000-0000-0000-0001-000000000001"),
            //     EntityFieldId = Guid.Parse("10000000-0000-0000-0056-000000000001"),
            //     NumberData = (float)600000.000, 
            //     IsDeleted = false
            // },
            new Value
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000007"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000002"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-0006-000000000001"),
                Text = "Договор на техническую поддержку",
                IsDeleted = false
            },
            new Value
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000008"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000002"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0036-000000000001"),
                DateData = Convert.ToDateTime("23/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat),
                IsDeleted = false
            },
            new Value //* Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000009"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000002"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0026-000000000001"),
                NumberData = (float)4500000000.000, 
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000010"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000002"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0056-000000000001"),
                NumberData = (float)40000.000, 
                IsDeleted = false
            },
            new Value // Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000011"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000002"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0056-000000000001"),
                NumberData = (float)350000.000, 
                IsDeleted = false
            },
            new Value // Внести в PeriodValue
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000012"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000002"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0056-000000000001"),
                NumberData = (float)680000.000, 
                IsDeleted = false
            },
            
            //Склады
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000019"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000006"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-1136-000000000001"),
                Text = "Склад микросхем" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000020"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000006"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-2236-000000000001"),
                Text = "Склад микросхем №1" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000021"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000006"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-3736-000000000001"),
                Text = "г. Ставрополь, ул. Ленина 67" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000022"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000007"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-1136-000000000001"),
                Text = "Склад готовой продукции" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000023"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000007"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-2236-000000000001"),
                Text = "Склад готовой продукции №1" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000024"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000007"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-3736-000000000001"),
                Text = "г.Таганрог, ул. Петровкская 124" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000025"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000008"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-1136-000000000001"),
                Text = "Канцелярский склад" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000026"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000008"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-2236-000000000001"),
                Text = "Канцелярский склад №1" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000027"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000008"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-3736-000000000001"),
                Text = "г.Краснодар, ул. Галицкого 34" ,
                IsDeleted = false
            },
             new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000028"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000009"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-1136-000000000001"),
                Text = "Склад бытовой химии" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000029"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000009"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-2236-000000000001"),
                Text = "Склад бытовой химии №1" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000030"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000009"),
                EntityFieldId =  Guid.Parse("10000000-0000-0000-3736-000000000001"),
                Text = "г.Краснодар, ул. Галицкого 178" ,
                IsDeleted = false
            },
            //Контрагенты
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000013"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000004"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0136-000000000001"),
                Text = "OOO ТехСтрой", 
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000014"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000004"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0236-000000000001"),
                IntegerData = 1224565767,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000015"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000004"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0736-000000000001"),
                Text = "г. Москва , улица Урицкого 25" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000031"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000004"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-1736-000000000001"),
                IsDeleted = false,
            },
             new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000016"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000004"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0136-000000000001"),
                Text = "OOO СпецТехника", 
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000017"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000005"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0236-000000000001"),
                IntegerData = 98763454,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000018"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000005"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-0736-000000000001"),
                Text = "г. Москва , улица О. Кошевого 68-Б" ,
                IsDeleted = false
            },
            new Value 
            {
                Id = Guid.Parse("a0000000-0000-0000-0000-000000000032"),
                ItemId = Guid.Parse("10000000-0000-0000-0001-000000000005"),
                EntityFieldId = Guid.Parse("10000000-0000-0000-1736-000000000001"),
                IsDeleted = false
            },
        };

        public static IEnumerable<Period> Periods => new List<Period>()
        {
            new Period
            {
                Id = Guid.Parse("b0000000-0000-0000-0000-000000000001"),
                Name = "Дни",
                IsDeleted = false
            },
            new Period
            {
                Id = Guid.Parse("b0000000-0000-0000-0000-000000000002"),
                Name = "Недели",
                IsDeleted = false
            },
            new Period
            {
                Id = Guid.Parse("b0000000-0000-0000-0000-000000000003"),
                Name = "Месяцы",
                IsDeleted = false
            },
            new Period
            {
                Id = Guid.Parse("b0000000-0000-0000-0000-000000000004"),
                Name = "Годы",
                IsDeleted = false
            },
        };
        public static IEnumerable<SelectedDictionaryItem> SelectedDictionaryItems => new List<SelectedDictionaryItem>()
        {
            new SelectedDictionaryItem
            {
                Id  = Guid.Parse("c0000002-0000-0000-0000-000000000001"),
                ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000031"),
                SelectedItemId =Guid.Parse("10000000-0000-0000-0001-000000000007") 
            },
            new SelectedDictionaryItem
            {
                Id  = Guid.Parse("c0000004-0000-0000-0000-000000000001"),
                ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000031"),
                SelectedItemId = Guid.Parse("10000000-0000-0000-0001-000000000008") 
            },
             new SelectedDictionaryItem
            {
                Id  = Guid.Parse("c0000052-0000-0000-0000-000000000001"),
                ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000032"),
                SelectedItemId =Guid.Parse("10000000-0000-0000-0001-000000000006")  
            },
            new SelectedDictionaryItem
            {
                Id  = Guid.Parse("a0000000-0000-0000-0000-000000000032"),
                ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000031"),
                SelectedItemId = Guid.Parse("10000000-0000-0000-0001-000000000009") 
            },
        };
        public static IEnumerable<PeriodValue> PeriodValues => new List<PeriodValue>()
        {
            // new PeriodValue
            // {
            //     Id = Guid.Parse("c0000000-0000-0000-0000-000000000001"),
            //     IsDeleted = false,
            //     PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000001"),
            //     ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000004"),
            //     Value_dt = Convert.ToDateTime("03/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            // },
            // new PeriodValue
            // {
            //     Id = Guid.Parse("c0000000-0000-0000-0000-000000000002"),
            //     IsDeleted = false,
            //     PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000001"),
            //     ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000005"),
            //     Value_dt = Convert.ToDateTime("07/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            // },
            // new PeriodValue
            // {
            //     Id = Guid.Parse("c0000000-0000-0000-0000-000000000003"),
            //     IsDeleted = false,
            //     PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000004"),
            //     ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000006"),
            //     Value_dt = Convert.ToDateTime("01/01/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            // },
             new PeriodValue
            {
                Id = Guid.Parse("c0000000-0000-0000-0000-000000000004"),
                IsDeleted = false,
                PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000001"),
                ValueId = Guid.Parse("a0000000-0000-0000-0000-000000000003"),
                Value_dt = Convert.ToDateTime("03/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            },
            new PeriodValue
            {
                Id = Guid.Parse("c0000000-0000-0000-0000-000000000005"),
                IsDeleted = false,
                PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000001"),
                ValueId =  Guid.Parse("a0000000-0000-0000-0000-000000000009"),
                Value_dt = Convert.ToDateTime("07/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            },
            new PeriodValue
            {
                Id = Guid.Parse("c0000000-0000-0000-0000-000000000015"),
                IsDeleted = false,
                PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000001"),
                ValueId =   Guid.Parse("a0000000-0000-0000-0000-000000000213"),
                Value_dt = Convert.ToDateTime("07/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            },
            new PeriodValue
            {
                Id = Guid.Parse("c0000000-0000-0000-0000-000000000016"),
                IsDeleted = false,
                PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000001"),
                ValueId =   Guid.Parse("a0000000-0000-0000-0000-000000000214"),
                Value_dt = Convert.ToDateTime("08/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            },
            new PeriodValue
            {
                Id = Guid.Parse("c0000000-0000-0000-0000-000000000116"),
                IsDeleted = false,
                PeriodId =  Guid.Parse("b0000000-0000-0000-0000-000000000001"),
                ValueId =   Guid.Parse("a0000000-0000-0000-0000-000000000334"),
                Value_dt = Convert.ToDateTime("02/11/2023",System.Globalization.CultureInfo.GetCultureInfo("ru-RU").DateTimeFormat)
                
            }
        };
                
    }
}
