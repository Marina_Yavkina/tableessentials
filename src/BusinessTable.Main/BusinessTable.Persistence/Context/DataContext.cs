using BusinessTable.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using  BusinessTable.Persistence.Context.Configurations;

namespace BusinessTable.Persistence.Context;

public class DataContext : DbContext
{
    public DataContext() 
    {
    }
    public DataContext(DbContextOptions<DataContext> options) : base(options)
    {
    }
    public DbSet<Unit> Units { get; set; }
   
    public DbSet<Field> Fields { get; set; }
    public DbSet<EntityField> EntityFields { get; set; }
    public DbSet<Item> Items { get; set; }
    public DbSet<Period> Periods { get; set; }
    public DbSet<Value> Values { get; set; }
    public DbSet<PeriodValue> PeriodValues { get; set; }
    public DbSet<SelectedDictionaryItem> SelectedDictionaryItems { get; set; }
    

    protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.HasPostgresExtension("uuid-ossp");
            
            modelBuilder.ApplyConfiguration(new UnitConfiguration());
            //modelBuilder.ApplyConfiguration(new EntityConfiguration());
            modelBuilder.ApplyConfiguration(new FieldConfiguration());
            modelBuilder.ApplyConfiguration(new EntityFieldConfiguration());
            modelBuilder.ApplyConfiguration(new ItemConfiguration());
            modelBuilder.ApplyConfiguration(new PeriodConfiguration());
            modelBuilder.ApplyConfiguration(new SelectedDictionaryConfiguration());
            modelBuilder.ApplyConfiguration(new PeriodValueConfiguration());
            modelBuilder.ApplyConfiguration(new ValueConfiguration());
        }
}
